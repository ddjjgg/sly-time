/*!****************************************************************************
 *
 * @file
 * ST_os_time_public.h
 *
 * The publicly accessible declarations associated with "ST_os_time.c"
 *
 * Refer to the Doxygen generated documentation for information on the
 * following items.
 *
 *****************************************************************************/




#ifndef ST_OS_TIME_PUBLIC_H
#define ST_OS_TIME_PUBLIC_H




#include <time.h>
#include <inttypes.h>




/// The max number of seconds that the #ST_Ps data structure can represent

#define ST_Ps_MAX_SECS	\
			\
	((int64_t) INT64_MAX / ((int64_t) 1000 * 1000 * 1000 * 1000))




/// The max number nanoseconds that #ST_Ps data structure can represent
/// ON TOP OF representing #TIME_PS_MAX_SECS (i.e. (TIME_PS_MAX_SEC * 10^12 +
/// TIME_PS_MAX_NSECS_WHEN_MAX_SEC * 10^3) represents the max value of a
/// #ST_Ps)

#define ST_Ps_MAX_NSECS_WHEN_MAX_SECS	\
					\
	((INT64_MAX - (ST_Ps_MAX_SECS * ((int64_t) 1000000000000))) / (int64_t) 1000)




/// The max value *.tv_nsec should have in a #timespec data structure

#define ST_TIMESPEC_MAX_TV_NSEC		(999999999)




/// The number of nanoseconds in a second

#define ST_NS_PER_SEC			(1000 * 1000 * 1000)




/// The number of picoseconds in a second

#define ST_PS_PER_SEC			((int64_t) 1000 * 1000 * 1000 * 1000)




/// The number of picoseconds in a nanosecond

#define ST_PS_PER_NS			(1000)




/// Used in printf() to print out a #ST_PsNum

#define ST_PRI_ST_PsNum			PRIi64




/// Used internally in #ST_Ps to hold a number of picoseconds

typedef int64_t ST_PsNum;




/// The datatype used to hold an integer number of picoseconds

typedef struct ST_Ps
{
	/// A time value measure in picoseconds

	ST_PsNum	ps;
}

ST_Ps;




#endif //ST_OS_TIME_PUBLIC_H

