#######################
# Compilation Options #
#######################


#Main project options

SHELL=			/bin/bash

CC?=			gcc

PROJ_NAME=		SlyTime

SYS_HEADER_DIR=		/usr/include

SYS_LIBRARY_DIR=	/usr/lib

DEST_HEADER_DIR=	$(SYS_HEADER_DIR)

DEST_LIBRARY_DIR=	$(SYS_LIBRARY_DIR)

SRC_TOP_HEADER=		SlyTime.h

PUBLIC_HEADERS=		$(SRC_TOP_HEADER) $(wildcard ./*_public.h) 


#This extracts the version number from the main project header

MAJOR_VERSION_GREP=	grep SLY_TIME_MAJOR_VERSION $(SRC_TOP_HEADER) | cut -d \( -f2 | cut -d \) -f1
MINOR_VERSION_GREP=	grep SLY_TIME_MINOR_VERSION $(SRC_TOP_HEADER) | cut -d \( -f2 | cut -d \) -f1
PATCH_VERSION_GREP=	grep SLY_TIME_PATCH_VERSION $(SRC_TOP_HEADER) | cut -d \( -f2 | cut -d \) -f1

MAJOR_VERSION=		$(shell $(MAJOR_VERSION_GREP))
MINOR_VERSION=		$(shell $(MINOR_VERSION_GREP))
PATCH_VERSION=		$(shell $(PATCH_VERSION_GREP))


#Determmine the semantic version when the minor version and patch version are
#masked out

SEM_1_VERSION=		$(MAJOR_VERSION)-x-x
SEM_2_VERSION=		$(MAJOR_VERSION)-$(MINOR_VERSION)-x
SEM_3_VERSION=		$(MAJOR_VERSION)-$(MINOR_VERSION)-$(PATCH_VERSION)

MIN_SEM_1_VERSION=	$(MAJOR_VERSION)
MIN_SEM_2_VERSION=	$(MAJOR_VERSION)-$(MINOR_VERSION)
MIN_SEM_3_VERSION=	$(MAJOR_VERSION)-$(MINOR_VERSION)-$(PATCH_VERSION)


#Determines all of the possible locations that the header/library files and
#links can be installed, based on semantic versions with parts masked out.
#
#During installation, symbolic links to $HEADER_DIR_LN_3_DEST will be placed at
#$HEADER_DIR_LN_1_DEST and $HEADER_DIR_LN_2_DEST, and $HEADER_DIR_LN_3_DEST itself will
#point to the actual header in $PUBLIC_HEADERS_DIR. A similar operation will
#be done for $LIBRARY_LN_3_DEST with LIBRARY_LN_2_DEST and LIBRARY_LN_1_DEST.

HEADER_DIR_LN_1=	$(PROJ_NAME)-$(SEM_1_VERSION)
HEADER_DIR_LN_2=	$(PROJ_NAME)-$(SEM_2_VERSION)
HEADER_DIR_LN_3=	$(PROJ_NAME)-$(SEM_3_VERSION)

LIBRARY_LN_1=		lib$(PROJ_NAME)-$(SEM_1_VERSION).so
LIBRARY_LN_2=		lib$(PROJ_NAME)-$(SEM_2_VERSION).so
LIBRARY_LN_3=		lib$(PROJ_NAME)-$(SEM_3_VERSION).so

HEADER_DIR_LN_1_DEST=	$(DEST_HEADER_DIR)/$(HEADER_DIR_LN_1)
HEADER_DIR_LN_2_DEST=	$(DEST_HEADER_DIR)/$(HEADER_DIR_LN_2)
HEADER_DIR_LN_3_DEST=	$(DEST_HEADER_DIR)/$(HEADER_DIR_LN_3)

LIBRARY_LN_1_DEST=	$(DEST_LIBRARY_DIR)/$(LIBRARY_LN_1)
LIBRARY_LN_2_DEST=	$(DEST_LIBRARY_DIR)/$(LIBRARY_LN_2)
LIBRARY_LN_3_DEST=	$(DEST_LIBRARY_DIR)/$(LIBRARY_LN_3)


#Commands that can be used to find the most recent version of the header/library
#that are installed on the system that match the version at some level of 
#specificity

FIND_HEADER_DIR_LN_1_MATCH=	ls -rxv1 $(DEST_HEADER_DIR) | grep $(PROJ_NAME)-$(MIN_SEM_1_VERSION) | cut -d$$'\n' -f1
FIND_HEADER_DIR_LN_2_MATCH=	ls -rxv1 $(DEST_HEADER_DIR) | grep $(PROJ_NAME)-$(MIN_SEM_2_VERSION) | cut -d$$'\n' -f1

FIND_LIBRARY_LN_1_MATCH=	ls -rxv1 $(DEST_LIBRARY_DIR) | grep lib$(PROJ_NAME)-$(MIN_SEM_1_VERSION) | cut -d$$'\n' -f1
FIND_LIBRARY_LN_2_MATCH=	ls -rxv1 $(DEST_LIBRARY_DIR) | grep lib$(PROJ_NAME)-$(MIN_SEM_2_VERSION) | cut -d$$'\n' -f1 


#Determines the options that will be used to compile the actual library

LIB_CFLAGS=		-Wall -std=gnu11 -g -fPIC -rdynamic -O2
LIB_LDFLAGS=		-lSlyDebug-0-x-x

LIB_SOURCES=		$(shell ls -1 | grep -E '.*\.(c|h)$$' | grep -E -v '(test|main)')

LIB_OBJECTS=		$(patsubst %.c,%.o,$(shell ls -1 | grep -E '.*\.(c)$$' | grep -E -v '(test|main)'))

LIB_DEPENDS=		$(patsubst %,.%.lib_dep,$(LIB_SOURCES))


#Determines the options that will be used to compile the test benches 

TEST_CFLAGS=		-Wall -std=gnu11 -g -fPIC -rdynamic -O2
TEST_LDFLAGS=		-L./ -lpthread -l$(PROJ_NAME)-$(SEM_3_VERSION)

TEST_SOURCES=		$(wildcard test*.c)\
			$(wildcard test*.h)

TEST_OBJECTS=		$(patsubst %.c,%.o,$(wildcard test*.c))

TEST_DEPENDS=		$(patsubst %,.%.test_dep,$(TEST_SOURCES))


#Determines the options that will be used to compile the library as an
#executable application with a main () function

APP_CFLAGS=		-Wall -std=gnu11 -g -fPIC -rdynamic -O2 -D COMPILE_MAIN=1
APP_LDFLAGS=		-lSlyDebug-0-x-x

APP_SOURCES=		ST_main.c

APP_OBJECTS=		ST_main.o

APP_DEPENDS=		$(patsubst %,.%.app_dep,$(APP_SOURCES))

APP_NAME=		SlyTime.a



######################
# Debugging Messages #
######################

#$(info $$CC					= [${CC}])
#$(info $$PROJ_NAME				= [${PROJ_NAME}])
#$(info $$SYS_HEADER_DIR			= [${SYS_HEADER_DIR}])
#$(info $$SYS_LIBRARY_DIR			= [${SYS_LIBRARY_DIR}])
#$(info $$DEST_HEADER_DIR			= [${DEST_HEADER_DIR}])
#$(info $$DEST_LIBRARY_DIR			= [${DEST_LIBRARY_DIR}])
#$(info $$SRC_TOP_HEADER			= [${SRC_TOP_HEADER}])
#$(info $$MAJOR_VERSION_GREP			= [${MAJOR_VERSION_GREP}])
#$(info $$MINOR_VERSION_GREP			= [${MINOR_VERSION_GREP}])
#$(info $$PATCH_VERSION_GREP			= [${PATCH_VERSION_GREP}])
#$(info $$MAJOR_VERSION				= [${MAJOR_VERSION}])
#$(info $$MINOR_VERSION				= [${MINOR_VERSION}])
#$(info $$PATCH_VERSION				= [${PATCH_VERSION}])
#$(info $$SEM_1_VERSION				= [${SEM_1_VERSION}])
#$(info $$SEM_2_VERSION				= [${SEM_2_VERSION}])
#$(info $$SEM_3_VERSION				= [${SEM_3_VERSION}])
#$(info $$HEADER_DIR_LN_1			= [${HEADER_DIR_LN_1}])
#$(info $$HEADER_DIR_LN_2			= [${HEADER_DIR_LN_2}])
#$(info $$HEADER_DIR_LN_3			= [${HEADER_DIR_LN_3}])
#$(info $$LIBRARY_LN_1				= [${LIBRARY_LN_1}])
#$(info $$LIBRARY_LN_2				= [${LIBRARY_LN_2}])
#$(info $$LIBRARY_LN_3				= [${LIBRARY_LN_3}])
#$(info $$HEADER_DIR_LN_1_DEST			= [${HEADER_DIR_LN_1_DEST}])
#$(info $$HEADER_DIR_LN_2_DEST			= [${HEADER_DIR_LN_2_DEST}])
#$(info $$HEADER_DIR_LN_3_DEST			= [${HEADER_DIR_LN_3_DEST}])
#$(info $$LIBRARY_LN_1_DEST			= [${LIBRARY_LN_1_DEST}])
#$(info $$LIBRARY_LN_2_DEST			= [${LIBRARY_LN_2_DEST}])
#$(info $$LIBRARY_LN_3_DEST			= [${LIBRARY_LN_3_DEST}])
#$(info $$LIB_CFLAGS				= [${LIB_CFLAGS}])
#$(info $$LIB_LDFLAGS				= [${LIB_LDFLAGS}])
#$(info $$LIB_SOURCES				= [${LIB_SOURCES}])
#$(info $$LIB_OBJECTS				= [${LIB_OBJECTS}])
#$(info $$LIB_DEPENDS				= [${LIB_DEPENDS}])
#$(info $$TEST_CFLAGS				= [${TEST_CFLAGS}])
#$(info $$TEST_LDFLAGS				= [${TEST_LDFLAGS}])
#$(info $$TEST_SOURCES				= [${TEST_SOURCES}])
#$(info $$TEST_OBJECTS				= [${TEST_OBJECTS}])
#$(info $$TEST_DEPENDS				= [${TEST_DEPENDS}])
#$(info $$APP_CFLAGS				= [${APP_CFLAGS}])
#$(info $$APP_LDFLAGS				= [${APP_LDFLAGS}])
#$(info $$APP_SOURCES				= [${APP_SOURCES}])
#$(info $$APP_OBJECTS				= [${APP_OBJECTS}])
#$(info $$APP_DEPENDS				= [${APP_DEPENDS}])
#$(info $$FIND_HEADER_DIR_LN_1_MATCH		= [$(FIND_HEADER_DIR_LN_1_MATCH)])
#$(info $$FIND_HEADER_DIR_LN_2_MATCH		= [$(FIND_HEADER_DIR_LN_2_MATCH)])
#$(info $$FIND_LIBRARY_LN_1_MATCH		= [$(FIND_LIBRARY_LN_1_MATCH)])
#$(info $$FIND_LIBRARY_LN_2_MATCH		= [$(FIND_LIBRARY_LN_2_MATCH)])
#$(info shell $$FIND_HEADER_DIR_LN_1_MATCH	= [$(shell $(FIND_HEADER_DIR_LN_1_MATCH))])
#$(info shell $$FIND_HEADER_DIR_LN_2_MATCH	= [$(shell $(FIND_HEADER_DIR_LN_2_MATCH))])
#$(info shell $$FIND_LIBRARY_LN_1_MATCH		= [$(shell $(FIND_LIBRARY_LN_1_MATCH))])
#$(info shell $$FIND_LIBRARY_LN_2_MATCH		= [$(shell $(FIND_LIBRARY_LN_2_MATCH))])




#######################
# Compilation Targets #
#######################

#Used to make the default action the compilation of the library

.PHONY: all
all: $(LIBRARY_LN_3) $(APP_NAME) $(LIB_OBJECTS) $(TEST_OBJECTS) $(APP_OBJECTS)




#Used to remove everything from current directory except source files

.PHONY: clean
clean:
	rm -f --preserve-root \
		$(LIBRARY_LN_3) $(LIB_OBJECTS) $(LIB_DEPENDS) \
		$(TEST_OBJECTS) $(TEST_DEPENDS) \
		$(APP_NAME) $(APP_OBJECTS) $(APP_DEPENDS)




#Used to install the library to the system's library directory

.PHONY: install
install: $(LIBRARY_LN_3)

	#Remove symbolic links that may already exist that we will
	#immediately re-build

	rm -vf --preserve-root $(HEADER_DIR_LN_2_DEST)
	rm -vf --preserve-root $(HEADER_DIR_LN_1_DEST)

	rm -vf --preserve-root $(LIBRARY_LN_2_DEST)
	rm -vf --preserve-root $(LIBRARY_LN_1_DEST)


	#Make the directory to hold all of the header files
	
	mkdir -p $(HEADER_DIR_LN_3_DEST)


	#Copy all the public headers into the new header directory, and make
	#some symbolic links to that directory using different semantic version
	#levels

	cp -vf  $(PUBLIC_HEADERS) $(HEADER_DIR_LN_3_DEST)
	ln -vsf $(HEADER_DIR_LN_3_DEST) $(HEADER_DIR_LN_2_DEST)
	ln -vsf $(HEADER_DIR_LN_3_DEST) $(HEADER_DIR_LN_1_DEST)


	#Install the library file, and make some symbolic links to it using
	#different semantic version levels

	cp -vf ./$(LIBRARY_LN_3) $(LIBRARY_LN_3_DEST)
	ln -vsf $(LIBRARY_LN_3_DEST) $(LIBRARY_LN_2_DEST)
	ln -vsf $(LIBRARY_LN_3_DEST) $(LIBRARY_LN_1_DEST)




#Used to remove the library from the system

.PHONY: uninstall
uninstall:
	
	#Remove the current version of SlyTime
	#
	#This is wrapped in $eval() blocks, because this MUST occur before
	#the next lines, which are also wrapped in $eval() blocks.

	$(eval $(info $(shell \
		\
		if [[ ! -z "$(HEADER_DIR_LN_3)" ]]; \
		then \
			rm -vrf --preserve-root $(DEST_HEADER_DIR)/$(HEADER_DIR_LN_3); \
		fi \
	)))

	$(eval $(info $(shell rm -vf --preserve-root $(HEADER_DIR_LN_2_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(HEADER_DIR_LN_1_DEST))))
	 
	$(eval $(info $(shell rm -vf --preserve-root $(LIBRARY_LN_3_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(LIBRARY_LN_2_DEST))))
	$(eval $(info $(shell rm -vf --preserve-root $(LIBRARY_LN_1_DEST))))
	
	
	#See if old versions of SlyTime are available
	#
	#This is in eval block so that these variables get updated before the
	#rest of the recipe is executed
	
	$(eval HEADER_DIR_LN_1_MATCH=	$(shell $(FIND_HEADER_DIR_LN_1_MATCH)))	
	$(eval HEADER_DIR_LN_2_MATCH=	$(shell $(FIND_HEADER_DIR_LN_2_MATCH)))
	
	$(eval LIBRARY_LN_1_MATCH=	$(shell $(FIND_LIBRARY_LN_1_MATCH)))
	$(eval LIBRARY_LN_2_MATCH=	$(shell $(FIND_LIBRARY_LN_2_MATCH)))
	
	
	#If older versions of SlyTime are available, regenerate
	#the symbolic links to point to them, to keep as many existing programs
	#working as possible.
	#
	#Note how `ln` is _not_ passed the '-f' option
	
	if [[ ! -z "$(HEADER_DIR_LN_1_MATCH)" ]]; \
	then \
		ln -vs $(DEST_HEADER_DIR)/$(HEADER_DIR_LN_1_MATCH) $(HEADER_DIR_LN_1_DEST); \
	fi
	
	if [[ ! -z "$(HEADER_DIR_LN_2_MATCH)" ]]; \
	then \
		ln -vs $(DEST_HEADER_DIR)/$(HEADER_DIR_LN_2_MATCH) $(HEADER_DIR_LN_2_DEST); \
	fi
	
	if [[ ! -z "$(LIBRARY_LN_1_MATCH)" ]]; \
	then \
		ln -vs $(DEST_LIBRARY_DIR)/$(LIBRARY_LN_1_MATCH) $(LIBRARY_LN_1_DEST); \
	fi
	
	if [[ ! -z "$(LIBRARY_LN_2_MATCH)" ]]; \
	then \
		ln -vs $(DEST_LIBRARY_DIR)/$(LIBRARY_LN_2_MATCH) $(LIBRARY_LN_2_DEST); \
	fi




#Used to remove every version of SlyTime from the system

.PHONY: uninstall-all
uninstall-all:
	
	if [[ ! -z "$(PROJ_NAME)" ]]; \
	then \
		rm -vfr --preserve-root $(DEST_HEADER_DIR)/$(PROJ_NAME)*/; \
		rm -vf --preserve-root $(DEST_HEADER_DIR)/$(PROJ_NAME)*; \
		rm -vf --preserve-root $(DEST_HEADER_DIR)/$(PROJ_NAME)*.h; \
		rm -vf --preserve-root $(DEST_LIBRARY_DIR)/lib$(PROJ_NAME)*.so; \
	fi




#Build a list of dependencies between each source file at run-time. To do this
#requires parsing each source file with the compiler, but without actually
#compiling the files.
#
#Each source file gets its own dependency file, so that not every source file
#has to be reparsed when only a single source file changes.

$(LIB_DEPENDS): .%.lib_dep : %
	rm -f --preserve-root ./$@
	$(CC) $(LIB_LDFLAGS) $(LIB_CFLAGS) -MM $^ > $@

include $(LIB_DEPENDS)


$(TEST_DEPENDS): .%.test_dep : %
	rm -f --preserve-root ./$@
	$(CC) $(TEST_LDFLAGS) $(TEST_CFLAGS) -MM $^ > $@

include $(TEST_DEPENDS)


$(APP_DEPENDS): .%.app_dep : %
	rm -f --preserve-root ./$@
	$(CC) $(APP_LDFLAGS) $(APP_CFLAGS) -MM $^ > $@

include $(APP_DEPENDS)




#Makes the main library file depend on all the other object files
#
#Note, the prequisites for $(LIB_OBJECTS) come from "include $(LIB_DEPENDS)"

$(LIB_OBJECTS):
	$(CC) $(LIB_LDFLAGS) $(LIB_CFLAGS) -c $(patsubst %.o, %.c, $@) -o $@


$(LIBRARY_LN_3): $(LIB_OBJECTS)
	$(CC) $(LIB_LDFLAGS) $(LIB_OBJECTS) -shared -o $@




#Makes all of the test bench files compile as separate programs that link to
#SlyTime

$(TEST_OBJECTS): $(LIBRARY_LN_3)
	$(CC) $(TEST_LDFLAGS) $(TEST_CFLAGS) $(patsubst %.o, %.c, $@) -o $@




#This produces the executable application version of the library

$(APP_OBJECTS):
	$(CC) $(APP_LDFLAGS) $(APP_CFLAGS) -c $(patsubst %.o, %.c, $@) -o $@


$(APP_NAME): $(APP_OBJECTS) $(LIB_OBJECTS)
	$(CC) $(APP_LDFLAGS) $(APP_OBJECTS) $(LIB_OBJECTS) -o $@

