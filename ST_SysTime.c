/*!****************************************************************************
 *
 * @file
 * ST_SysTime.c
 *
 * Contains an implementation of the #ST_SysTime data structure, which is used
 * to characterize the running system's timing characteristics. This
 * information is particularly useful for performing system sleeps in a highly
 * accurate manner.
 *
 *****************************************************************************/




#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>


#include "ST_debug.h"
#include "ST_misc.h"
#include "ST_numeric_types.h"
#include "ST_os_time.h"


#include "ST_SysTime.h"




/// Stores a #ST_Ps record, which is a collection of time samples where the
/// most recently updated sample is kept track of.

typedef struct ST_PsRecord
{

	/// The number of samples to store in #samples

	u32 num_samples;


	/// Indicates where the most recently updated sample in the #samples
	/// array is

	u32 index;


	/// The current calculation of the sample average, which should be
	/// updated whenever #samples is updated

	ST_Ps sample_avg;


	/// An array holding time samples in the #ST_Ps format
	///
	/// @warning
	/// It is up to external data structures and functions to perform the
	/// memory management of this array.
	/// Why? It obviates the need for an extra de-referencing operation in
	/// #ST_SysTime, which would be required if this had normal
	/// *_create() and *_delete() functions.

	ST_Ps * samples;

}

ST_PsRecord;




/// Stores parameters regarding the capabilities of the operating system's
/// clocks and timers

typedef struct ST_SysTime
{

	/// The smallest amount of time the OS can measure (in units of
	/// pico-seconds)
	///
	/// This uses CLOCK_MONOTONIC on Linux.

	ST_Ps monotonic_res_ps;


	/// The smallest amount of time the operating system can make a program
	/// perform a non-busy sleep by

	ST_Ps min_nbs_ps;


	/// The amount of acceptable error in determining the smallest non-busy
	/// program sleep. i.e., if this is 0.10, and the program is asked to
	/// sleep for 100 usec, it it should not sleep for less than 90 usec
	/// and no more than 110 usec.
	///
	/// To be clear, this is NOT the average error of any given non-busy
	/// sleep, it is relevant only when the non-busy sleep interval is
	/// equal to #min_nbs_ps.

	f64 min_nbs_max_error;


	/// The smallest timed-busy sleep that can be performed with the
	/// operating system's given timer functions. A "timed" busy sleep has
	/// a minimum useful sleep interval because the OS timer functions can
	/// take 1000's of clock cycles to execute.

	ST_Ps min_tbs_ps;


	/// How many times larger than #min_tbs_ps a sleep interval has to be
	/// for the preferred sleep strategy to switch from an "instruction
	/// busy sleep" to a "timed busy sleep"

	f64 min_tbs_ibs_factor;


	/// The average amount of time that the "arbitrary operation" for this
	/// #ST_SysTime data structure takes. See #ST_arb_op () for details
	/// about this value. It plays a critical role in performing
	/// "instruction busy sleeps"

	ST_Ps arb_op_ps;


	/// A record holding previous samples of the system's sleep jitter
	/// determined during attempts to make the program perform a non-busy
	/// sleep. This array is updated every time a nonbusy sleep is
	/// performed using the #ST_SysTime structure.
	///
	/// Note, part of the reason this array is necessary is because most
	/// systems have a variable sleep jitter depending on current system
	/// load.

	ST_PsRecord nbs_jitter;


	/// A record holding previous samples of the system's busy sleep
	/// jitter.
	///
	/// How can a busy sleep possibly have jitter? Ultimately,
	/// when the system time is requested in a busy sleep, that is a
	/// request to kernel-space, and the kernel _may_ take a variable
	/// amount of time to service that request depending on its
	/// configuration.

	ST_PsRecord tbs_jitter;

}

ST_SysTime;




/// Updates all of the fields available within a #ST_PsRecord. Note that
/// some of the updated values are automatically calculated.
///
/// @warning
/// For the #samples array, it is up to external logic to perform its memory
/// management. External logic must malloc() the space for it, and it MUST
/// free() it.
/*!
 *  @param tpr			The #ST_PsRecord to update
 *
 *  @param num_samples		The number of samples that should be inside of
 *  				the #ST_PsRecord
 *
 *  @param samples		Pointer to the array where the samples should
 *  				be stored. This array MUST be #num_samples in
 *  				length
 *
 *  @param init_val		The initial value to set the samples to in the
 *  				#samples array
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_PsRecord_set_all
(
 	ST_PsRecord *	tpr,
	u32		num_samples,
	ST_Ps *		samples,
	ST_Ps		init_val
)
{

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, tpr, ST_BAD_ARG);
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, samples, ST_BAD_ARG);


	// Set all of the immediately known parameters of the #ST_PsRecord

	tpr->num_samples	= num_samples;

	tpr->index		= 0;

	tpr->samples		= samples;

	tpr->sample_avg		= init_val;


	// Initialize all of the samples at the #samples array

	ST_PsRecord_init_samples (tpr, init_val);


	return ST_SUCCESS;

}




/// Initializes all the samples in a #ST_PsRecord to the same value
/*!
 *  @param tpr			The #ST_PsRecord to update
 *
 *  @param init_val		The initial value of the samples within the
 *  				#ST_PsRecord
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_PsRecord_init_samples (ST_PsRecord * tpr, ST_Ps init_val)
{
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, tpr, ST_BAD_ARG);


	// Enter loop where all the samples of the #ST_PsRecord are
	// initialized one-by-one

	for (u32 sample_num = 0; sample_num < tpr->num_samples; sample_num += 1)
	{
		tpr->samples [sample_num] = init_val;
	}


	return ST_SUCCESS;
}




/// Updates the stored value of the sample average in a #ST_PsRecord, which is
/// based off of the samples currently available in the record
/*!
 *  @param tpr			The #ST_PsRecord to update
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_PsRecord_refresh_avg (ST_PsRecord * tpr)
{
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, tpr, ST_BAD_ARG);


	// Compute the average of all the samples in the ST_PsRecord

	double avg = 0;

	for (u32 sample_num = 0; sample_num < tpr->num_samples; sample_num += 1)
	{
		avg += (f64) tpr->samples [sample_num].ps;
	}

	avg = (avg / (f64) tpr->num_samples);


	//Save the average back into the #ST_PsRecord

	tpr->sample_avg.ps = (ST_PsNum) avg;


	return ST_SUCCESS;
}




/// Add a time sample to a #ST_PsRecord, overwriting old values if necessary
/*!
 *  @param tpr			The #ST_PsRecord to update
 *
 *  @param new_tp_val		The sample to insert into #tpr
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_PsRecord_add_sample (ST_PsRecord * tpr, ST_Ps new_tp_val)
{
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, tpr, ST_BAD_ARG);


	// Update the position of the most recent sleep jitter (and take into
	// account wrap-around at the same time)
	//
	// Note that this must come before actually saving the jitter, so that
	// the index always points to the most recent value

	tpr->index = (tpr->index + 1) % (tpr->num_samples);


	// Insert the new jitter value into the sleep jitter array at the
	// most recent location
	//
	// Note how we check that there is actually space for samples at
	// tpr->samples

	if (0 < tpr->num_samples)
	{
		tpr->samples [tpr->index] = new_tp_val;
	}


	// Refresh the sample average, now the newest sample has been put into
	// the record

	ST_PsRecord_refresh_avg (tpr);

	return ST_SUCCESS;
}




/// Prints out a #ST_PsRecord
/*!
 *  @param f_ptr		The FILE * to print to
 *
 *  @param tpr			The #ST_PsRecord to print
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_PsRecord_print (FILE * f_ptr, ST_PsRecord * tpr)
{
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, f_ptr, ST_BAD_ARG);

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, tpr, ST_BAD_ARG);


	// Print out the general parameters of the #ST_PsRecord

	fprintf (f_ptr, "ST_PsRecord @ %p\n\n", tpr);

	fprintf (f_ptr, "num_samples = %" PRIu32 "\n", tpr->num_samples);

	fprintf (f_ptr, "index       = %" PRIu32 "\n", tpr->index);

	fprintf (f_ptr, "sample_avg  = ");

	ST_Ps_print (f_ptr, tpr->sample_avg);

	fprintf (f_ptr, "\n");

	fprintf (f_ptr, "samples     @ %p\n\n", tpr->samples);

	// Enter a loop to print out every sample within the #ST_PsRecord

	fprintf (f_ptr, "Recent #ST_Ps Samples (arrow points to most recent)\n\n");

	for (u32 sample_num = 0; sample_num < tpr->num_samples; sample_num += 1)
	{
		if (sample_num == tpr->index)
		{
			fprintf (f_ptr, "---> ");
		}

		else
		{
			fprintf (f_ptr, "     ");
		}

		fprintf (f_ptr, "%+" ST_PRI_ST_PsNum " ps\n", tpr->samples [sample_num].ps);
	}


	return ST_SUCCESS;
}




/// Prints out a ST_SysTime data structure
/*!
 *  @param f_ptr		The FILE * to print to
 *
 *  @param stp			The #ST_SysTime data structure to print out
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_print (FILE * f_ptr, ST_SysTime * stp)
{
	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, f_ptr, ST_BAD_ARG);

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);


	fprintf
	(
	 	f_ptr,
		"ST_SysTime         @ %p\n\n",
		stp
	);


	fprintf
	(
		f_ptr,
		"monotonic_res_ps   = %" ST_PRI_ST_PsNum " ps\n",
		stp->monotonic_res_ps.ps
	);


	fprintf
	(
		f_ptr,
		"min_nbs_ps         = %" ST_PRI_ST_PsNum " ps\n",
		stp->min_nbs_ps.ps
	);


	fprintf
	(
		f_ptr,
		"min_nbs_max_error  = %lf\n",
		stp->min_nbs_max_error
	);


	fprintf
	(
		f_ptr,
		"min_tbs_ps         = %" ST_PRI_ST_PsNum " ps\n",
		stp->min_tbs_ps.ps
	);


	fprintf
	(
		f_ptr,
		"min_tbs_ibs_factor = %lf\n",
		stp->min_tbs_ibs_factor
	);


	fprintf
	(
		f_ptr,
		"arb_op_ps          = %" ST_PRI_ST_PsNum " ps\n\n",
		stp->arb_op_ps.ps
	);


	fprintf (f_ptr, "Printing #nbs_jitter\n\n");
	ST_PsRecord_print (f_ptr, &(stp->nbs_jitter));


	fprintf (f_ptr, "\nPrinting #tbs_jitter\n\n");
	ST_PsRecord_print (f_ptr, &(stp->tbs_jitter));


	return ST_SUCCESS;
}




/// Determines the smallest useful system sleep and saves this value in a
/// #ST_SysTime data structure.
/*!
 *  @param stp			The #ST_SysTime to modify
 *
 *  @param monotonic_time_res	The system monotonic timer resolution. This
 *  				can be obtained with get_monotonic_res_ps().
 *
 *  @param min_nbs_first_ps	Same usage as in ST_get_min_nb_sleep_ps()
 *
 *  @param max_sleep_error	Same usage as in ST_get_min_nb_sleep_ps()
 *
 *  @param max_sleep_attempts	The max number of times to attempt to determine
 *				the smallest sleep interval before giving up.
 *				This argument is necessary because system
 *				sleeps can probalistically fail.
 *
 *				This value MUST be larger than
 *				#num_sleep_samples, otherwise ST_BAD_ARG will
 *				be returned.
 *
 *  @param num_sleep_retries	The number of times to retry a singular sleep
 *  				interval in an iteration of
 *  				get_smallest_sleep(). The recommended value is
 *  				8.
 *
 *  @param num_sleep_samples	The number of times the smallest sleep will be
 *  				determined. The results will be averaged
 *  				together to get a final value for the smallest
 *  				sleep. This is necessary to get a realistic
 *  				value, beause the smallest useful sleep on a
 *  				modern system is a moving target depending on
 *  				system load.
 *
 *  				The recommended value for this argument is 8.
 *
 *  @param def_small_sleep	The default value to assume for the smallest
 *  				system sleep, in the event that the actual
 *  				value can not be determined. This value will be
 *  				used in the event that the number of attempts
 *  				exceeds #max_sleep_attempts.
 *
 *  @return			Standard status code. In the event that ST_SUCCESS
 *  				is not returned, then #def_small_sleep will be
 *  				used as the smallest sleep value.
 */

ST_STD_RES ST_SysTime_update_min_nbs_ps
(
	ST_SysTime *	stp,
	ST_Ps		monotonic_time_res,
	ST_Ps		min_nbs_first_ps,
	f64		max_sleep_error,
	u32		max_sleep_attempts,
	u32		num_sleep_retries,
	u32		num_sleep_samples,
	ST_Ps		def_small_sleep
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);


	//Save the maximum error that will be allowed when calculating the
	//smallest sleep

	stp->min_nbs_max_error = max_sleep_error;


	//Check if the number of sleep attempts is higher than the number of
	//needed samples

	if (max_sleep_attempts < num_sleep_samples)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"#max_sleep_attempts must be larger than "
			"#num_sleep_samples!"
		);

		stp->min_nbs_ps = def_small_sleep;

		return ST_BAD_ARG;
	}


	// Enter an attempt loop to determine the smallest useful system sleep

	u32 cur_attempt	= 0;
	u32 cur_sample	= 0;

	ST_Ps small_sleep_avg = {.ps = 0};
	ST_Ps cur_small_sleep = {.ps = 0};

	for (cur_attempt = 0; cur_attempt < max_sleep_attempts; cur_attempt += 1)
	{
		ST_STD_RES res = ST_get_min_nb_sleep_ps
				 (
					&cur_small_sleep,
					min_nbs_first_ps,
					max_sleep_error,
					monotonic_time_res,
					num_sleep_retries
				 );


		// If this attempt to get a sample was successful, then add its
		// contribution to the average.

		if (ST_SUCCESS == res)
		{
			small_sleep_avg.ps += (ST_PsNum) (cur_small_sleep.ps /  (ST_PsNum) num_sleep_samples);

			cur_sample += 1;
		}

		// Check if enough samples have been taken

		if (cur_sample >= num_sleep_samples)
		{
			break;
		}
	}


	// Check if the correct number of samples have been taken

	if (cur_sample < num_sleep_samples)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Ran out of attempts to determine minimum useful "
			"system sleep, so using default value."
		);

		stp->min_nbs_ps = def_small_sleep;

		return ST_UNKNOWN_ERROR;
	}


	// Save the calculated minimum sleep

	stp->min_nbs_ps = small_sleep_avg;


	return ST_SUCCESS;
}




/// Updates the estimated minimum amount of time that a "timed busy sleep" can
/// be performed.
/*!
 *  @param stp			Pointer to the #ST_SysTime to update
 *
 *  @param max_attempts		The max number of times a single busy sleep can
 *				be retried before giving up. The recommended
 *				value here is 1024.
 *
 *  @param num_samples		The number of times to try attempting the
 *				smallest possible busy sleep. The larger this
 *				value is, the more resistant this function is
 *				to outliers, but it will take longer to execute
 *				as well. The recommended value is (16).
 *
 *  @param def_min_tbs_ps	The default value to assume for the smallest
 *				possible "timed busy sleep". This is highly
 *				system dependent, but generally, this should
 *				represent about 200 clock cycles, so
 *				approximately 66000 ps to 200000 ps.
 *
 *  @return			Standard status code. In the event that neither
 *  				#ST_SUCCESS or #ST_BAD_ARG is returned, then
 *  				#def_min_tbs_ps was used.
 */

ST_STD_RES ST_SysTime_update_min_tbs_ps
(
	ST_SysTime *		stp,
	u32			max_attempts,
	u32			num_samples,
	ST_Ps			def_min_tbs_ps
)
{
	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);


	if (0 >= num_samples)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#num_samples floored to 1"
		);

		num_samples = 1;
	}


	if (num_samples >= max_attempts)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#max_attempts floored to #num_samples"
		);

		max_attempts = num_samples;
	}


	if ((32 * 1024) < num_samples)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"num_samples = %" PRIu32 " , meaning that many "
			"samples will be stored before their median value is "
			"found. This value is large, so this function will "
			"probably be unnecessarily slow.",
			num_samples
		)
	}


	//Allocate an array to hold all of the samples smallest timed busy
	//sleep

	ST_Ps * sample_ps = malloc (sizeof (ST_Ps) * num_samples);

	if (NULL == sample_ps)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not allocate space for #sample_ps. Using given "
			"default for #min_tbs_ps."
		);

		stp->min_tbs_ps = def_min_tbs_ps;

		return ST_BAD_ALLOC;
	}


	//Get the samples of the minimum amount of time a timed busy sleep can
	//be performed.

	for (u32 sample_num = 0; sample_num < num_samples; sample_num += 1)
	{
		//Perform the busy sleep

		struct timespec start_ts = {.tv_sec = 0, .tv_nsec = 0};
		struct timespec stop_ts  = {.tv_sec = 0, .tv_nsec = 0};

		struct timespec sleep_ts = {.tv_sec = 0, .tv_nsec = 1};


		ST_STD_RES res =

		ST_busy_sleep_marked_monotonic
		(
			sleep_ts,
			max_attempts,
			&start_ts,
			&stop_ts
		);


		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Failed to perform busy sleep."
			);

			stp->min_tbs_ps = def_min_tbs_ps;

			return ST_UNKNOWN_ERROR;
		}


		//Determine the amount of time the busy sleep took, and convert
		//it to an #ST_Ps value

		struct timespec used_ts = ST_timespec_abs_diff (stop_ts, start_ts);


		ST_Ps used_ps = {.ps = 0};

		res = ST_timespec_to_ST_Ps (used_ts, &used_ps);


		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Failed to convert #timespec value to #ST_Ps."
			);

			stp->min_tbs_ps = def_min_tbs_ps;

			return ST_UNKNOWN_ERROR;
		}


		//Save this sample in the sample array

		sample_ps [sample_num] = used_ps;
	}


	//Sort the array of samples

	qsort (sample_ps, num_samples, sizeof (ST_Ps), ST_Ps_compare_qsort);


	if (ST_SYS_TIME_H_DEBUG)
	{
		printf ("%s, sample_ps : \n", __func__);

		for (u32 i = 0; i < num_samples; i += 1)
		{
			printf ("%" ST_PRI_ST_PsNum "\n", sample_ps [i].ps);
		}
	}


	//Save the median as the value of #min_tbs_ps

	stp->min_tbs_ps = sample_ps [(num_samples / 2)];


	//Free any memory allocated in this function

	free (sample_ps);


	return ST_SUCCESS;

}




/// Updates the estimated amount of time it takes to complete the SlyTime
/// "arbitrary operation". This value is relevant in performing so-called
/// "instruction busy sleeps", which are used to perform extremely small timed
/// sleeps.
/*!
 *  @param stp			Pointer to the #ST_SysTime to update
 *
 *  @param max_attempts		The max number of times to attempt getting the
 *				average time it takes to execute ST_arb_op ().
 *				The recommended value here is
 *				(2 * #num_averages), and it must at least be
 *				larger than #num_averages
 *
 *  @param max_attempts_per_avg The max number of times to retry the OS timer
 *				functions in the event they fail when trying to
 *				get the average execution time of ST_arb_op ().
 *				The recommended value here is (1024).
 *
 *  @param num_averages		The number of times to measure the average
 *				execution time of ST_arb_op_ps () before
 *				accepting the median of those values as _the
 *				average_. The recommended value here is (64).
 *				The larger this value is, the more accurate the
 *				result will be, but it makes this function take
 *				longer as well.
 *				This value must be larger than 0.
 *
 *  @param num_samples_per_avg	The number of times to execute ST_arb_op ()
 *				during each calculation of its average
 *				execution time. The recommended value here is
 *				(100 * 1000).
 *				The larger this value is, the more accurate the
 *				result will be, but it makes this function take
 *				longer as well.
 *				This value must be larger than 0.
 *
 *  @param def_arb_op_ps	The default execution time to assume for
 *  				the execution time for ST_arb_op (). This value
 *  				is only used in the case that the actual value
 *  				could not be measured.
 *  				On most systems, it should take about 10 clock
 *  				cycles, so this value can be anywhere from 3000
 *  				ps to 12000 ps.
 *  				It is considered better to overestimate on this
 *  				value than underestimate, because assuming
 *  				ST_arb_op () takes longer to execute than it
 *  				really does will make "instruction busy sleeps"
 *  				tend to end early rather than late.
 *
 *  @return			Standard status code. In the event that neither
 *  				#ST_SUCCESS or #ST_BAD_ARG is returned, then
 *  				#def_arb_op_ps was used.
 */

ST_STD_RES ST_SysTime_update_arb_op_ps
(
	ST_SysTime *	stp,
	u32		max_attempts,
	u32		max_attempts_per_avg,
	u32		num_averages,
	u32		num_samples_per_avg,
	ST_Ps		def_arb_op_ps
)
{
	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);


	if (0 >= max_attempts_per_avg)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"#max_attempts_per_avg floored to 1"
		);

		max_attempts_per_avg = 1;
	}


	if (0 >= num_averages)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"#num_averages floored to 1"
		);

		num_averages = 1;
	}


	if (0 >= num_samples_per_avg)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"#num_samples_per_avg floored to 1"
		);

		num_samples_per_avg = 1;
	}


	if (num_averages > max_attempts)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"#max_attempts floored to #num_averages"
		);

		max_attempts = num_averages;
	}


	if ((32 * 1024) < num_averages)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"num_averages = %" PRIu32 " , meaning that many "
			"averages will be stored before their median value is "
			"found. This value is large, so this function will "
			"probably be unnecessarily slow.",
			num_averages
		)
	}


	//Allocate an array to hold the averages for the amount of time
	//#ST_arb_op () consumes

	ST_Ps * avg_ps = malloc (sizeof (ST_Ps) * num_averages);

	if (NULL == avg_ps)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Could not allocate space for #avg_ps. Using given "
			"default value for #arb_op_ps."
		);


		stp->arb_op_ps = def_arb_op_ps;


		return ST_BAD_ALLOC;
	}


	//Attempt to get all #num_averages of samples of the average execution
	//time for #ST_arb_op_ps ()

	u32 avg_index = 0;

	for (u32 attempt_num = 0; attempt_num < max_attempts; attempt_num += 1)
	{
		ST_STD_RES res =

		ST_get_arb_op_ps_avg
		(
			&(avg_ps [avg_index]),
			max_attempts_per_avg,
			num_samples_per_avg
		);


		if (ST_SUCCESS == res)
		{
			avg_index += 1;
		}


		if (num_averages <= avg_index)
		{
			break;
		}
	}


	//Check to make sure that enough averages were successfully recorded

	if (num_averages > avg_index)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Not enough averages gathered to determine #arb_op_ps "
			"as directed. Using given default value of #arb_op_ps "
			"in #ST_SysTime at %p",
			stp
		);


		//Use default value here

		stp->arb_op_ps = def_arb_op_ps;


		//Free any memory allocated in this function

		free (avg_ps);


		return ST_UNKNOWN_ERROR;

	}


	//Sort the array of samples of the average

	qsort (avg_ps, num_averages, sizeof (ST_Ps), ST_Ps_compare_qsort);


	if (ST_SYS_TIME_H_DEBUG)
	{
		printf ("%s, avg_ps : \n", __func__);

		for (u32 i = 0; i < num_averages; i += 1)
		{
			printf ("%" ST_PRI_ST_PsNum "\n", avg_ps [i].ps);
		}
	}


	//Save the median as the value of #arb_op_ps

	stp->arb_op_ps = avg_ps [(num_averages / 2)];


	//Free any memory allocated in this function

	free (avg_ps);


	return ST_SUCCESS;
}




/// Updates the measured system monotonic time resolution in a #ST_SysTime
/// data structure.
/*!
 *  @param stp			The #ST_SysTime to update
 *
 *  @param max_attempts		It is possible for the system to interrupt
 *  				the clock_getres() function, so getting the
 *  				system monotonic timer resolution is
 *  				necessarily an operation that can fail. This
 *  				indicates the number of times to retry finding
 *  				the resolution if the first attempt fails. The
 *  				recommended value here is 256.
 *
 *  				If this argument is 0, it will be treated as if
 *  				it were 1.
 *
 *  @param def_time_res		The value to assume for the monotonic timer
 *				resolution in the event that all attempts to
 *				determine its actual value have failed.
 *
 *  @return			Standard status code. ST_UNKNOWN_ERROR is returned
 *  				if the timer resolution could not be
 *  				determined, ST_SUCCESS is returned otherwise.
 *
 *  				In the event the timer resolution could not be
 *  				determined, then the monotonic timer resolution
 *  				in #stp will be made equal to #def_time_res.
 */

ST_STD_RES ST_SysTime_update_monotonic_time_res
(
	ST_SysTime *	stp,
	u32		max_attempts,
	ST_Ps		def_time_res
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);

	u32 internal_max_attempts = max_attempts;

	if (internal_max_attempts <= 0)
	{
		internal_max_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// Attempt to determine system monotonic timer resolution in eager
	// fashion

	ST_Ps res_ps;

	ST_STD_RES res = ST_get_monotonic_res_ps_eager (&res_ps, internal_max_attempts);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Failed to determine resolution of system timers! "
			"Assuming default monotonic timer resolution."
		);

		stp->monotonic_res_ps = def_time_res;

		return ST_UNKNOWN_ERROR;
	}


	// Save the determined #monotonic_res_ps

	stp->monotonic_res_ps = res_ps;


	return ST_SUCCESS;
}




/// Inserts a new entry for a nonbusy sleep jitter into a #ST_SysTime
/// structure. This will overwrite old sleep jitter entries as necessary.
/*!
 *  @param stp			The #ST_SysTime structure to update
 *
 *  @param sleep_jitter_ps	The sleep jitter value to add to #ST_SysTime
 *  				data structure. A positive value indicates how
 *  				many pico-seconds were overslept, and a
 *  				negative value indicates how many pico-seconds
 *  				were underslept.
 *
 *  @return			Standard status code.
 */

ST_STD_RES ST_SysTime_record_nbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps		sleep_jitter_ps
)
{
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);

	return ST_PsRecord_add_sample (&(stp->nbs_jitter), sleep_jitter_ps);
}




/// The same as ST_SysTime_record_nbs_jitter(), EXCEPT that a
/// _busy_ sleep jitter is recorded instead.
/*!
 *  @param stp			The #ST_SysTime structure to update
 *
 *  @param sleep_jitter_ps	The sleep jitter value to add to #ST_SysTime
 *  				data structure. A positive value indicates how
 *  				many pico-seconds were overslept, and a
 *  				negative value indicates how many pico-seconds
 *  				were underslept.
 *
 *  @return			Standard status code.
 */

ST_STD_RES ST_SysTime_record_tbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps		sleep_jitter_ps
)
{
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);

	return ST_PsRecord_add_sample (&(stp->tbs_jitter), sleep_jitter_ps);
}




/// Averages all of the nonbusy sleep jitters recorded in a #ST_SysTime
/// structure
/*!
 *  @param stp			The #ST_SysTime data structure read the
 *  				sleep jitters from
 *
 *  @param avg_jitter_ps	Where to place the calculated average sleep
 *  				jitter
 *
 *  @return			Standard status code.
 */

ST_STD_RES ST_SysTime_avg_nbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps *		avg_jitter_ps
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, avg_jitter_ps, ST_BAD_ARG);


	// Make sure the average of the nonbusy sleep jitters is up-to-date

	ST_PsRecord_refresh_avg (&(stp->nbs_jitter));


	// Save the newly refreshed jitter average

	*avg_jitter_ps = stp->nbs_jitter.sample_avg;


	return ST_SUCCESS;
}




/// Averages all of the busy sleep jitters recorded in a #ST_SysTime
/// structure
/*!
 *  @param stp			The #ST_SysTime data structure read the
 *  				sleep jitters from
 *
 *  @param avg_jitter_ps	Where to place the calculated average sleep
 *  				jitter
 *
 *  @return			Standard status code.
 */

ST_STD_RES ST_SysTime_avg_tbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps *		avg_jitter_ps
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ARG);
	ST_DEBUG_NULL_RETURN (ST_SYS_TIME_H_DEBUG, avg_jitter_ps, ST_BAD_ARG);


	// Make sure the average of the busy sleep jitters is up-to-date

	ST_PsRecord_refresh_avg (&(stp->tbs_jitter));


	// Save the newly refreshed jitter average

	*avg_jitter_ps = stp->tbs_jitter.sample_avg;


	return ST_SUCCESS;
}




/// Performs a smart "instruction busy-sleep", which is a busy sleep that
/// executes some arbitrary CPU instructions to create a timing delay. This
/// method of a busy sleep should be used when the OS timer functions are too
/// slow for the given sleep interval.
///
/// @warning
/// The value of #used_ps produced by this function is an approximation based
/// on the number of instructions executed; it is NOT derived from the OS
/// timers. As a result, it can potentially be very inaccurate (say, in the
/// event the time slice the OS gave to this program completed and will switch
/// to another, so 2 consecutive instructions from this program end up being
/// far apart). As a result, #used_ps should be treated as a best effort value,
/// it may or may not suffer from an inaccuracy spike.
/*!
 *  @param stp			A #ST_SysTime data structure that describes
 *				the running system's timing parameters
 *
 *  @param sleep_ps		The amount of time to sleep, measured in
 *				pico-seconds
 *
 *  @param used_ps		Where to save the amount of time the sleep
 *				actually took, measured in pico-seconds. This
 *				can be NULL if this value is not desired.
 *				See the above warning regarding this argument.
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_ibs_opt
(
	ST_SysTime *		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
)
{
	// Sanity check on arguments

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);


	//Calculate how many times to execute ST_arb_op (), and then execute
	//it that many times

	u64 num_arb_op = (sleep_ps.ps / stp->arb_op_ps.ps);


	if (0 >= num_arb_op)
	{
		num_arb_op = 1;
	}


	for (u64 exec_num = 0; exec_num < num_arb_op; exec_num += 1)
	{
		ST_arb_op (exec_num);
	}


	//Save the estimate of the amount of time used

	if (NULL != used_ps)
	{
		used_ps->ps = (num_arb_op * stp->arb_op_ps.ps);
	}

	return ST_SUCCESS;
}




/// Performs a smart _timed_ busy-sleep, which is a busy sleep that attempts
/// to anticipate its own oversleep / undersleep. Keep in mind, this kind of
/// sleep does NOT save any CPU resources.
///
/// This busy sleep is "timed" because it operates on the principle of
/// constantly checking the current time reported by the operating system.
///
/*!
 *  @param stp			An #ST_SysTime data structure that describes
 *				the running system's timing parameters
 *
 *  @param sleep_ps		The amount of time to sleep, measured in
 *				pico-seconds
 *
 *  @param num_retries		The max number of retries to attempt the busy
 *				sleep, in the event the system interrupts the
 *				sleep. (For a busy sleep, this means the system
 *				timer functions become unavailable for some
 *				time for whatever reason).
 *
 *  @param used_ps		Where to save the amount of time the sleep
 *				actually took, measured in pico-seconds. This
 *				can be NULL if this value is not desired.
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_tbs_opt
(
	ST_SysTime * 		stp,
	ST_Ps			sleep_ps,
	u32			num_retries,
	ST_Ps *			used_ps
)
{
	// Sanity check on #stp argument

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);


	//Check if sleep method should be switched to an "instruction busy
	//sleep" due to a very small sleep interval

	if
	(
		(stp->arb_op_ps.ps < stp->min_tbs_ps.ps)

		&&

		(sleep_ps.ps < (ST_PsNum) (stp->min_tbs_ps.ps * stp->min_tbs_ibs_factor))
	)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Switching to instruction busy sleep...."
		);

		return ST_SysTime_ibs_opt (stp, sleep_ps, used_ps);
	}


	//Sanity check on #num_retries argument

	if (num_retries <= 0)
	{
		num_retries = 1;

		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#num_retries argument was floored to 1."
		);
	}


	// Determine the amount of time to sleep, based on the system's busy
	// sleep jitter

	ST_Ps busy_ps = {.ps = 0};

	if (sleep_ps.ps < stp->tbs_jitter.sample_avg.ps)
	{
		busy_ps.ps = 0;
	}

	else
	{
		busy_ps.ps = (sleep_ps.ps - (stp->tbs_jitter.sample_avg.ps));
	}


	// Convert the sleep interval into a #timespec value

	struct timespec busy_ts = {.tv_sec = 0, .tv_nsec = 0};

	ST_STD_RES res = ST_Ps_to_timespec (busy_ps, &busy_ts);


	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not convert #busy_ps to #busy_ts."
		);

		return res;
	}


	// Perform the actual busy sleep itself

	struct timespec busy_start =	{.tv_sec = 0, .tv_nsec = 0};
	struct timespec busy_end =	{.tv_sec = 0, .tv_nsec = 0};

	res =	ST_busy_sleep_marked_monotonic
		(
			busy_ts,
			num_retries,
			&busy_start,
			&busy_end
		);


	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not perform a busy sleep."
		);

		return res;
	}


	// Determine the actual amount of time used by the busy sleep, and
	// convert it into an #ST_Ps value

	struct timespec busy_used_ts = ST_timespec_abs_diff (busy_end, busy_start);


	ST_Ps busy_used_ps = {.ps = 0};

	res = ST_timespec_to_ST_Ps (busy_used_ts, &busy_used_ps);


	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not convert #busy_used_ts to a #ST_Ps value"
		);

		return res;
	}


	// Determine the amount of busy sleep jitter

	ST_Ps busy_jitter_ps = ST_Ps_diff (busy_used_ps, busy_ps);


	// Record the busy sleep jitter

	ST_SysTime_record_tbs_jitter (stp, busy_jitter_ps);


	// Save the amount of time the sleep actually consumed

	if (NULL != used_ps)
	{
		*used_ps = busy_used_ps;
	}


	return ST_SUCCESS;
}




/// Like ST_SysTime_tbs_opt(), but with fewer arguments.
/// (These parameters of the sleep are made to use default values.)
/*!
 *  @param stp			A #ST_SysTime data structure that describes
 *				the running system's timing parameters
 *
 *  @param sleep_ps		The amount of time to sleep, measured in
 *				pico-seconds
 *
 *  @param used_ps		Where to save the amount of time the sleep
 *				actually took, measured in pico-seconds. This
 *				can be NULL if this value is not desired.
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_tbs
(
	ST_SysTime *		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
)
{
	return	ST_SysTime_tbs_opt
		(
			stp,
			sleep_ps,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			used_ps
		);
}




/// Performs a smart non-busy sleep that uses automatically determined
/// busy/non-busy waits, which simulataneously frees the CPU as long as
/// possible whilst keeping the accuracy of the sleep very high.
/*!
 *  @param stp			An #ST_SysTime data structure that describes
 *  				the running system's timing parameters
 *
 *  @param sleep_ps		The amount of time to sleep, measured in
 *				pico-seconds
 *
 *  @param jitter_ratio		After calculating the average nonbusy sleep
 *  				jitter, this value will be multiplied with it
 *  				to determine the predicted sleep jitter. The
 *  				higher this value is, the larger of a portion
 *  				of the sleep that will be busy.
 *  				The recomended value for this argument is
 *				(1.20)
 *
 *  @param nonbusy_retries	The number of times to retry a non-busy sleep,
 *  				in the event that it is interrupted. Note, the
 *  				remaining sleep time is automatically adjusted,
 *  				so this argument does NOT increase the total
 *  				sleep time. The recommended value for this
 *  				argument is 1024.
 *
 *  @param busy_retries		The number of times to retry a busy sleep, in
 *  				the event it is interrupted. How can a busy
 *  				sleep possibly fail? The time measuring system
 *  				calls on a hypothetical platform may not be
 *  				guaranteed to succeed, so the system may get
 *  				into a state where the timers are unavailable
 *  				to user programs for a short interval.  Like
 *  				#nonbusy_retries, this does NOT increase the
 *  				total sleep time.  The recommended value for
 *  				this argument is 1024.
 *
 *  @param used_ps		Where to save the amount of time the sleep
 *				actually took, measured in pico-seconds. This
 *				can be NULL if this value is not desired.
 *
 *  @return			#ST_SUCCESS if the sleep was performed
 *  				correctly and the value at #used_ps was
 *  				updated. Otherwise, a standard status code will
 *  				be returned describing the error.
 */

ST_STD_RES ST_SysTime_nbs_opt
(
	ST_SysTime *		stp,
	ST_Ps			sleep_ps,
	f64			jitter_ratio,
	u32			nonbusy_retries,
	u32			busy_retries,
	ST_Ps *			used_ps
)
{
	//Sanity check on the #stp argument

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);


	//Immediately go to an instruction-busy-sleep if the necessary. This
	//needs to be done so quickly that this must occur near the start of
	//this function.

	if
	(
		(stp->arb_op_ps.ps < stp->min_tbs_ps.ps)

		&&

		(sleep_ps.ps < (ST_PsNum) (stp->min_tbs_ps.ps * stp->min_tbs_ibs_factor))
	)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Switching to instruction busy sleep...."
		);

		return ST_SysTime_ibs_opt (stp, sleep_ps, used_ps);
	}


	//Sanity check on the retry arguments

	if (0 >= nonbusy_retries)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#nonbusy_retries argument was floored to 1."
		);

		nonbusy_retries = 1;
	}


	if (0 >= busy_retries)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#busy_retries argument was floored to 1."
		);

		busy_retries = 1;
	}


	// Fetch the average nonbusy sleep jitter

	ST_Ps avg_jitter_ps = {.ps = 0};

	ST_STD_RES res = ST_SysTime_avg_nbs_jitter (stp, &avg_jitter_ps);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not determine average nonbusy sleep jitter. "
			"Sleep failed!"
		);

		return res;
	}


	// Determine the predicted jitter

	ST_Ps pred_jitter_ps = {.ps = (ST_PsNum) (avg_jitter_ps.ps * jitter_ratio)};


	// Determine if this sleep should have a nonbusy portion at all

	int sleep_is_busy_only =

	(sleep_ps.ps < stp->min_nbs_ps.ps) || (sleep_ps.ps < pred_jitter_ps.ps);


	// Determine the portion of the sleep that should be nonbusy, and the
	// portion that should be busy.

	ST_Ps nonbusy_ps =	{.ps = 0};
	ST_Ps busy_ps =		{.ps = 0};

	if (sleep_is_busy_only)
	{
		nonbusy_ps.ps =	0;
		busy_ps.ps =	sleep_ps.ps;
	}

	else
	{
		nonbusy_ps.ps =	(ST_PsNum) (sleep_ps.ps - pred_jitter_ps.ps);
		busy_ps.ps =	(ST_PsNum) (pred_jitter_ps.ps);
	}


	ST_DEBUG_PRINT
	(
		ST_SYS_TIME_H_DEBUG,
		"Initial nonbusy_ps.ps = %" ST_PRI_ST_PsNum " ps\n"
		"Initial busy_ps.ps =    %" ST_PRI_ST_PsNum " ps",
		nonbusy_ps.ps,
		busy_ps.ps
	);


	// Perform the nonbusy portion of the sleep, and calculate statistics
	// about it that help determine how the busy portion should be
	// performed

	struct timespec nonbusy_ts =		{.tv_sec = 0, .tv_nsec = 0};

	struct timespec nonbusy_start_ts =	{.tv_sec = 0, .tv_nsec = 0};
	struct timespec nonbusy_stop_ts =	{.tv_sec = 0, .tv_nsec = 0};

	struct timespec nonbusy_used_ts	=	{.tv_sec = 0, .tv_nsec = 0};

	ST_Ps nonbusy_used_ps =			{.ps = 0};
	ST_Ps nonbusy_jitter_ps =		{.ps = 0};


	if ( ! sleep_is_busy_only)
	{
		//Convert the nonbusy sleep interval into a #timespec value

		res = ST_Ps_to_timespec (nonbusy_ps, &nonbusy_ts);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not convert #nonbusy_ps into "
				"#nonbusy_ts, sleep failed."
			);

			return res;
		}


		//Perform the nonbusy sleep

		res =	ST_sleep_marked_monotonic
			(
				nonbusy_ts,
				nonbusy_retries,
				&nonbusy_start_ts,
				&nonbusy_stop_ts
			);


		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not perform nonbusy portion of sleep."
			);

			return res;
		}


		// Calculate the actual amount of time used by this nonbusy
		// sleep, and convert that into an #ST_Ps value

		nonbusy_used_ts =

		ST_timespec_abs_diff (nonbusy_stop_ts, nonbusy_start_ts);


		res = ST_timespec_to_ST_Ps (nonbusy_used_ts, &nonbusy_used_ps);

		if (res != ST_SUCCESS)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not convert #nonbusy_used_ts into "
				"#nonbusy_used_ps"
			);

			return res;
		}


		// Calculate the amount of jitter that is associated with this
		// nonbusy sleep, and then record that jitter in the #stp
		// object

		nonbusy_jitter_ps.ps = nonbusy_used_ps.ps - nonbusy_ps.ps;

		ST_SysTime_record_nbs_jitter (stp, nonbusy_jitter_ps);


		// Check if the nonbusy sleep overslept so much, that no busy
		// sleep is needed anymore

		if (sleep_ps.ps < nonbusy_used_ps.ps)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Overslept in nonbusy sleep, so skipping busy "
				"sleep."
			);


			//Save the amount of time the sleep actually consumed

			if (NULL != used_ps)
			{
				*used_ps = nonbusy_used_ps;
			}

			return ST_SUCCESS;
		}


		// Recalculate the busy sleep interval based on the actual
		// amount of time the nonbusy sleep consumed

		busy_ps.ps = sleep_ps.ps - nonbusy_used_ps.ps;
	}


	ST_DEBUG_PRINT
	(
		ST_SYS_TIME_H_DEBUG,
		"nonbusy_used_ps.ps =    %" ST_PRI_ST_PsNum "\n"
		"busy_ps.ps =            %" ST_PRI_ST_PsNum "\n",
		nonbusy_used_ps.ps,
		busy_ps.ps
	);


	// Perform the busy portion of the sleep

	ST_Ps busy_used_ps = {.ps = 0};

	res =	ST_SysTime_tbs_opt
		(
			stp,
			busy_ps,
			busy_retries,
			&busy_used_ps
		);


	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not properly perform busy portion of sleep."
		);

		return res;
	}


	// Save the amount of time this sleep consumed
	//
	// Note, if there was no nonbusy sleep, then nonbusy_used_ps.ps will be
	// equal to 0

	if (NULL != used_ps)
	{
		used_ps->ps = nonbusy_used_ps.ps + busy_used_ps.ps;
	}


	return ST_SUCCESS;
}




/// Performs a smart non-busy sleep that uses automatically determined
/// busy/non-busy waits, which simulataneously frees the CPU as long as
/// possible whilst keeping the accuracy of the sleep very high.
///
/// This function is similar to ST_SysTime_nbs_opt (), but with less arguments.
/// (These parameters of the sleep are placed at default values).
/*!
 *  @param stp			An #ST_SysTime data structure that describes
 *  				the running system's timing parameters
 *
 *  @param sleep_ps		The amount of time to sleep, measured in
 *				pico-seconds
 *
 *  @param used_ps		Where to save the amount of time the sleep
 *				actually took, measured in pico-seconds. This
 *				can be NULL if this value is not desired.
 *
 *  @return			#ST_SUCCESS if the sleep was performed
 *  				correctly and the value at #used_ps was
 *  				updated. Otherwise, a standard status code will
 *  				be returned describing the error.
 */

ST_STD_RES ST_SysTime_nbs
(
	ST_SysTime * 		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
)
{

	return	ST_SysTime_nbs_opt
		(
			stp,
			sleep_ps,
			ST_SYS_TIME_NBS_JITTER_RATIO,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			ST_SYS_TIME_NBS_BUSY_RETRIES,
			used_ps
		);
}




/// Calculates the error associated with a particular sleep interval when
/// performing a non-busy sleep using ST_SysTime_nbs_opt ().
///
/// #num_sleeps of separate sleep intervals of #single_nbs_ps are used.
/// This makes the function consume enough time so that a stop-watch can be
/// started and stopped without throwing off the error calculation.
///
/// It is important to note that the error is "absolute" in the sense if the
/// requested sleep interval is 100 us and the actual sleep is 95 us or 105 us,
/// then the error will be +0.05 in either case.
///
/// @warning
/// This function can consume a surprisingly large amount of time if a sleep
/// interval with a large amount of error is used. The expected amount of time
/// this function consumes can be given by
///
///	total_time = single_nbs_ps * (1 + error) * num_sleeps
///
/// @warning
/// The calculated error value associated with a sleep interval is a moving
/// target dependent on system load. So it may be desirable to run this
/// function multiple times and then take the median as a result. In practice,
/// this should be unnecessary in the wide majority of use-cases.
/*!
 *  @param stp			A #ST_SysTime data structure that describes
 *  				the running system's timing parameters
 *
 *  @param single_nbs_ps	The "nbs" sleep interval that the calculated
 *				error will be in reference to.
 *
 *  @param jitter_ratio		See ST_SysTime_nbs_opt ()
 *
 *  @param nonbusy_retries	See ST_SysTime_nbs_opt ()
 *
 *  @param busy_retries		See ST_SysTime_nbs_opt ()
 *
 *  @param num_sleeps		The number of times to sleep for #single_nbs_ps
 *				in between starting and stopping the
 *				stop-watch. This should be large enough so that
 *				the time used for starting/stopping the
 *				stop-watch are very small in comparison to the
 *				total sleep time. It is recommended that the
 *				total sleep time is at least 1 millisecond,
 *				so the recommended value here is
 *				(1 ms / #single_nbs_ps)
 *
 *  @param error		Where to place the calculated error associated
 *  				with the sleep interval
 *
 *  @return			Standard status code; if this is not
 *				#ST_SUCCESS, then the error was not calculated
 *				correctly.
 */

ST_STD_RES ST_SysTime_nbs_opt_calc_error
(
	ST_SysTime *		stp,

	ST_Ps			single_nbs_ps,
	f64			jitter_ratio,
	u32			nonbusy_retries,
	u32			busy_retries,

	u64			num_sleeps,
	f64 *			error
)
{
	//Sanity check on arguments

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);
	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, error, ST_BAD_ARG);


	if (0 >= single_nbs_ps.ps)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#single_nbs_ps.ps was %" ST_PRI_ST_PsNum " "
			"so it was floored to 1.",
			single_nbs_ps.ps
		);

		single_nbs_ps.ps = 1;
	}


	if (0 >= num_sleeps)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#num_sleeps was %" PRIu32 " "
			"so it was floored to 1.",
			num_sleeps
		);

		num_sleeps = 1;
	}


	//Start a "stop-watch"

	const u32 STOP_WATCH_RETRIES = 1024;

	struct timespec start_ts = {.tv_sec = 0, .tv_nsec = 0};

	ST_STD_RES res = ST_get_monotonic_time_eager (&start_ts, STOP_WATCH_RETRIES);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not start stop-watch!"
		);

		return ST_BAD_LIB_CALL;
	}


	//Perform #num_sleeps number of sleeps, each with an interval of
	//#single_nbs_ps

	for (u32 sleep_num = 0; sleep_num < num_sleeps; sleep_num += 1)
	{
		ST_SysTime_nbs_opt
		(
			stp,
			single_nbs_ps,
			jitter_ratio,
			nonbusy_retries,
			busy_retries,
			NULL
		);
	}


	//Stop the "stop-watch"

	struct timespec stop_ts = {.tv_sec = 0, .tv_nsec = 0};

	res = ST_get_monotonic_time_eager (&stop_ts, STOP_WATCH_RETRIES);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not stop stop-watch!"
		);

		return ST_BAD_LIB_CALL;
	}


	//Calculate the amount of time consumed by all the sleeps in total
	//
	//FIXME: If a function to convert a #timespec directly to an #f64
	//existed here, the problem of overflow could be totally obviated
	//at this part

	struct timespec actual_total_ts = ST_timespec_abs_diff (stop_ts, start_ts);

	ST_Ps actual_total_ps = {.ps = 0};

	res = ST_timespec_to_ST_Ps (actual_total_ts, &actual_total_ps);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"Could not convert #actual_total_ts to #actual_total_ps."
		);

		return ST_UNKNOWN_ERROR;
	}


	//Calculate the error associated with the sleep (the use of f64's here
	//makes it so that an overflow can not occur in this stage)

	f64 desired_total_ps = ((f64) single_nbs_ps.ps) * num_sleeps;

	f64 jitter_ps = desired_total_ps - ((f64) actual_total_ps.ps);

	if (0 > jitter_ps)
	{
		jitter_ps = -1 * jitter_ps;
	}

	*error = jitter_ps / desired_total_ps;


	ST_DEBUG_PRINT
	(
		ST_SYS_TIME_M_DEBUG,
		"single_nbs_ps    = %" ST_PRI_ST_PsNum " ps\n"
		"desired_total_ps = %lf ps\n"
		"actual_total_ps  = %" ST_PRI_ST_PsNum " ps\n"
		"jitter_ps        = %lf ps\n"
		"error            = %lf%%\n",
		single_nbs_ps.ps,
		desired_total_ps,
		actual_total_ps.ps,
		jitter_ps,
		100 * (*error)
	);


	return ST_SUCCESS;
}




/// Calculates the error associated with a particular sleep interval when
/// performing a non-busy sleep using ST_SysTime_nbs ().
///
/// #num_sleeps of separate sleep intervals of #single_nbs_ps are used.
/// This makes the function consume enough time so that a stop-watch can be
/// started and stopped without throwing off the error calculation.
///
/// It is important to note that the error is "absolute" in the sense if the
/// requested sleep interval is 100 us and the actual sleep is 95 us or 105 us,
/// then the error will be +0.05 in either case.
///
/// @warning
/// This function can consume a surprisingly large amount of time if a sleep
/// interval with a large amount of error is used. The expected amount of time
/// this function consumes can be given by
///
///	total_time = single_nbs_ps * (1 + error) * num_sleeps
///
/// @warning
/// The calculated error value associated with a sleep interval is a moving
/// target dependent on system load. So it may be desirable to run this
/// function multiple times and then take the median as a result. In practice,
/// this should be unnecessary in the wide majority of use-cases.
/*!
 *  @param stp			A #ST_SysTime data structure that describes
 *  				the running system's timing parameters
 *
 *  @param single_nbs_ps	The "nbs" sleep interval that the calculated
 *				error will be in reference to.
 *
 *  @param num_sleeps		The number of times to sleep for #single_nbs_ps
 *				in between starting and stopping the
 *				stop-watch. This should be large enough so that
 *				the time used for starting/stopping the
 *				stop-watch are very small in comparison to the
 *				total sleep time. It is recommended that the
 *				total sleep time is at least 1 millisecond,
 *				so the recommended value here is
 *				(1 ms / #single_nbs_ps)
 *
 *  @param error		Where to place the calculated error associated
 *  				with the sleep interval
 *
 *  @return			Standard status code; if this is not
 *				#ST_SUCCESS, then the error was not calculated
 *				correctly.
 */

ST_STD_RES ST_SysTime_nbs_calc_error
(
	ST_SysTime *		stp,
	ST_Ps			single_nbs_ps,
	u64			num_sleeps,
	f64 *			error
)
{
	return	ST_SysTime_nbs_opt_calc_error
		(
			stp,
			single_nbs_ps,
			ST_SYS_TIME_NBS_JITTER_RATIO,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			ST_SYS_TIME_NBS_BUSY_RETRIES,
			num_sleeps,
			error
		);
}




/// Calculates the smallest useful sleep that can be performed using
/// ST_SysTime_nbs_opt ().
///
/// The minimum sleep interval is found using a binary search beginning at the
/// value of #first_sleep_ps. To avoid the possibility of enormous sleep
/// intervals to meet impossibly small #max_error rates, the binary search will
/// only consider sleep intervals smaller than #first_sleep_ps.
///
/// @warning
/// A plot of (sleep interval vs. error) would show that the sleep error is not
/// monotonic. This is a side-effect of the selected sleep technique changing
/// in response to the sleep interval.
///
/// As a result, small increases to #max_error may make this function identify
/// dramatically smaller sleep intervals.
///
/// @warning
/// This function must perform many sleeps in order to measure the error
/// associated with them. As a result, it should be expected that this function
/// may take about 10 times as long as the first sleep interval to complete (if
/// #first_sleep_ps and #error_calc_min_ps are well chosen). Typically this
/// implies the function usually completes in 10 - 20 ms; so this should not be
/// performed in a tight loop.
///
/// @warning
/// The smallest sleep interval associated with a particular error rate is a
/// moving target dependent on system load. So it may be desirable to run this
/// function multiple times and then take the median as a result. In practice,
/// this should be unnecessary in the wide majority of use-cases.
/*!
 *  @param stp			A #ST_SysTime data structure that describes
 *  				the running system's timing parameters
 *
 *  @param jitter_ratio		See ST_SysTime_nbs_opt ()
 *
 *  @param nonbusy_retries	See ST_SysTime_nbs_opt ()
 *
 *  @param busy_retries		See ST_SysTime_nbs_opt ()
 *
 *  @param first_sleep_ps	The amount of time to sleep for the first test
 *				sleep interval. If this sleep interval has an
 *				error associated with it higher than
 *				#max_error, then the calculation will fail. The
 *				recommended value here is 1 millisecond.
 *
 *  @param error_calc_min_ps	The minimum amount of time to sleep in between
 *				each error calculation, repeating sleep
 *				intervals of size #first_sleep_ps if necessary.
 *				This argument is needed because using OS
 *				functions to measure time differences take a
 *				non-negligible amount of time to complete, and
 *				so their execution time must be made irrelevant
 *				to the error calculation. The recommended value
 *				here is 1 millisecond.
 *
 *  @param min_bound_ps		When the difference between the upper/lower
 *				bounds of the binary search are lower than this
 *				value, the search will terminate early. This
 *				value will allow this function to complete
 *				faster at the expense of accuracy. The
 *				recommended value here is 1 ns, so that the
 *				calculated value of #min_nbs_ps is within 1
 *				nano-second of what it otherwise would be if
 *				the "full" binary search was performed. This
 *				argument can be 0 if the binary search should
 *				continue for as long as possible.
 *
 *  @param max_error		The maximum acceptable error rate on a sleep
 *				interval that is considered useful. The
 *				recommended value here is 0.10. This value
 *				should be high enough so that the error rate
 *				associated with #first_sleep_ps is smaller than
 *				it.
 *
 *  @param def_min_sleep_ps	The value to place at #min_nbs_ps in the event
 *				that this function could not successfully
 *				complete.
 *
 *  @param min_nbs_ps		Where to place the calculated value for the
 *				minimum useful sleep that can be performed with
 *				ST_SysTime_nbs_opt ()
 *
 *  @return			#ST_SUCCESS will be returned if the minimum
 *				useful sleep interval was successfully
 *				calculated. If any other value is returned,
 *				then the value of #def_min_nbs_ps will be
 *				placed at #min_nbs_ps, except in the event that
 *				#min_nbs_ps was NULL.
 */

ST_STD_RES ST_SysTime_nbs_opt_calc_min_sleep
(
	ST_SysTime *		stp,

	f64			jitter_ratio,
	u32			nonbusy_retries,
	u32			busy_retries,

	ST_Ps			first_sleep_ps,
	ST_Ps			error_calc_min_ps,
	ST_Ps			min_bound_ps,

	f64			max_error,

	ST_Ps			def_min_nbs_ps,
	ST_Ps *			min_nbs_ps
)
{
	//Sanity check on arguments

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, min_nbs_ps, ST_BAD_ARG);


	if (NULL == stp)
	{
		*min_nbs_ps = def_min_nbs_ps;

		ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);
	}


	if (0 >= first_sleep_ps.ps)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#first_sleep_ps.ps was %" ST_PRI_ST_PsNum " "
			"so it was floored to 1.",
			first_sleep_ps.ps
		);

		first_sleep_ps.ps = 1;
	}


	if (0 >= error_calc_min_ps.ps)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_H_DEBUG,
			"#error_calc_min_ps.ps was %" ST_PRI_ST_PsNum " "
			"so it was floored to 1.",
			error_calc_min_ps.ps
		);

		error_calc_min_ps.ps = 1;
	}


	//These variables contain the current bounds of the binary search

	ST_Ps hi_sleep_ps =	first_sleep_ps;
	ST_Ps lo_sleep_ps =	{.ps = 0};

	ST_Ps cur_sleep_ps =	first_sleep_ps;


	//Determines the maximum possible number of sleep intervals that must
	//be tested in the binary search.
	//
	//This constant is derived from the fact that a binary search is used,
	//and so the search space goes down by a factor of 2 each time.

	const u32 MAX_SEARCHES = (sizeof (cur_sleep_ps.ps) * 8);


	//Enter a loop where ST_SysTime_nbs_opt () will be used with
	//increasingly smaller sleep intervals

	for (u32 search_num = 0; search_num < MAX_SEARCHES; search_num += 1)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"hi_sleep_ps =  %" ST_PRI_ST_PsNum " ps\n"
			"cur_sleep_ps = %" ST_PRI_ST_PsNum " ps\n"
			"lo_sleep_ps =  %" ST_PRI_ST_PsNum " ps\n",
			hi_sleep_ps.ps,
			cur_sleep_ps.ps,
			lo_sleep_ps.ps
		);


		//Fetch the error associated with the current sleep interval

		f64 cur_error = 0;

		u64 num_sleeps = (error_calc_min_ps.ps / cur_sleep_ps.ps) + 1;


		ST_STD_RES res =

		ST_SysTime_nbs_opt_calc_error
		(
			stp,

			cur_sleep_ps,
			jitter_ratio,
			nonbusy_retries,
			busy_retries,

			num_sleeps,
			&cur_error
		);


		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_L_DEBUG,
				"Could not fetch error associated with %"
				ST_PRI_ST_PsNum " ps sleep interval!",
				cur_sleep_ps.ps
			)

			*min_nbs_ps = def_min_nbs_ps;

			return res;
		}


		//Check if the first sleep interval was above the max error, in
		//which case the binary search can not be performed

		if ((0 == search_num) && (cur_error > max_error))
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_L_DEBUG,
				"#first_sleep_ps was too small, "
				"calculating #min_nbs_ps failed."
			)

			*min_nbs_ps = def_min_nbs_ps;

			return ST_BAD_ARG;
		}


		//Update the high/low bounds of the binary search space
		//depending on the current error

		if (cur_error < max_error)
		{
			//The sleep can become smaller, because the error is
			//allowed to become larger

			hi_sleep_ps = cur_sleep_ps;


			cur_sleep_ps.ps =

			(ST_PsNum) ((lo_sleep_ps.ps + cur_sleep_ps.ps) / (ST_PsNum) 2);
		}

		else
		{
			//The sleep must become larger, because the error is
			//too high

			lo_sleep_ps = cur_sleep_ps;


			cur_sleep_ps.ps =

			(ST_PsNum) ((hi_sleep_ps.ps + cur_sleep_ps.ps) / (ST_PsNum) 2);
		}


		//Check if the sleep interval has become so low that it is not
		//representable by an #ST_Ps any more, in which case, the sleep
		//can not progress any farther

		if (0 >= cur_sleep_ps.ps)
		{
			cur_sleep_ps.ps = 1;

			break;
		}


		//Check if the search space has become exhausted, and if so,
		//terminate the binary search

		if (lo_sleep_ps.ps >= hi_sleep_ps.ps)
		{
			break;
		}


		//Check if the distance between the upper and lower bounds of
		//the binary search are close enough as to be negligible

		if (ST_Ps_abs_diff (hi_sleep_ps, lo_sleep_ps).ps <= min_bound_ps.ps)
		{
			break;
		}
	}


	//Return the calculated value of the smallest useful sleep interval

	*min_nbs_ps = cur_sleep_ps;


	return ST_SUCCESS;
}




/// Calculates the smallest useful sleep that can be performed using
/// ST_SysTime_nbs ().
///
/// The minimum sleep interval is found using a binary search beginning at the
/// value of #first_sleep_ps. To avoid the possibility of enormous sleep
/// intervals to meet impossibly small #max_error rates, the binary search will
/// only consider sleep intervals smaller than #first_sleep_ps.
///
/// @warning
/// A plot of (sleep interval vs. error) would show that the sleep error is not
/// monotonic. This is a side-effect of the selected sleep technique changing
/// in response to the sleep interval.
///
/// As a result, small increases to #max_error may make this function identify
/// dramatically smaller sleep intervals.
///
/// @warning
/// This function must perform many sleeps in order to measure the error
/// associated with them. As a result, it should be expected that this function
/// may take about 10 times as long as the first sleep interval to complete (if
/// #first_sleep_ps and #error_calc_min_ps are well chosen). Typically this
/// implies the function usually completes in 10 - 20 ms; so this should not be
/// performed in a tight loop.
///
/// @warning
/// The smallest sleep interval associated with a particular error rate is a
/// moving target dependent on system load. So it may be desirable to run this
/// function multiple times and then take the median as a result. In practice,
/// this should be unnecessary in the wide majority of use-cases.
/*!
 *  @param stp			A #ST_SysTime data structure that describes
 *  				the running system's timing parameters
 *
 *  @param first_sleep_ps	The amount of time to sleep for the first test
 *				sleep interval. If this sleep interval has an
 *				error associated with it higher than
 *				#max_error, then the calculation will fail. The
 *				recommended value here is 1 millisecond.
 *
 *  @param error_calc_min_ps	The minimum amount of time to sleep in between
 *				each error calculation, repeating sleep
 *				intervals of size #first_sleep_ps if necessary.
 *				This argument is needed because using OS
 *				functions to measure time differences take a
 *				non-negligible amount of time to complete, and
 *				so their execution time must be made irrelevant
 *				to the error calculation. The recommended value
 *				here is 1 millisecond.
 *
 *  @param min_bound_ps		When the difference between the upper/lower
 *				bounds of the binary search are lower than this
 *				value, the search will terminate early. This
 *				value will allow this function to complete
 *				faster at the expense of accuracy. The
 *				recommended value here is 1 ns, so that the
 *				calculated value of #min_nbs_ps is within 1
 *				nano-second of what it otherwise would be if
 *				the "full" binary search was performed. This
 *				argument can be 0 if the binary search should
 *				continue for as long as possible.
 *
 *  @param max_error		The maximum acceptable error rate on a sleep
 *				interval that is considered useful. The
 *				recommended value here is 0.10. This value
 *				should be high enough so that the error rate
 *				associated with #first_sleep_ps is smaller than
 *				it.
 *
 *  @param def_min_sleep_ps	The value to place at #min_nbs_ps in the event
 *				that this function could not successfully
 *				complete.
 *
 *  @param min_nbs_ps		Where to place the calculated value for the
 *				minimum useful sleep that can be performed with
 *				ST_SysTime_nbs_opt ()
 *
 *  @return			#ST_SUCCESS will be returned if the minimum
 *				useful sleep interval was successfully
 *				calculated. If any other value is returned,
 *				then the value of #def_min_nbs_ps will be
 *				placed at #min_nbs_ps, except in the event that
 *				#min_nbs_ps was NULL.
 */

ST_STD_RES ST_SysTime_nbs_calc_min_sleep
(
	ST_SysTime *		stp,

	ST_Ps			first_sleep_ps,
	ST_Ps			error_calc_min_ps,
	ST_Ps			min_bound_ps,

	f64			max_error,

	ST_Ps			def_min_nbs_ps,
	ST_Ps *			min_nbs_ps
)
{
	return	ST_SysTime_nbs_opt_calc_min_sleep
		(
			stp,

			ST_SYS_TIME_NBS_JITTER_RATIO,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			ST_SYS_TIME_NBS_BUSY_RETRIES,

			first_sleep_ps,
			error_calc_min_ps,
			min_bound_ps,

			max_error,

			def_min_nbs_ps,
			min_nbs_ps
		);
}




/// Creates an ST_SysTime data-structure, which stores the timing
/// characteristics of the current system. This information is useful for
/// performing highly accurate timed sleeps in a program.
///
/// @warning
/// This function must perform several program sleeps of various sizes to
/// characterize the running system. It may take several milliseconds to let
/// these sleeps complete (usually about 80 ms in total). As a result, this
/// function should NOT be run in a tight loop. It is much more efficient to
/// create this structure once, and then re-use it wherever needed.
///
/// @warning
/// After using this function, ST_SysTime_destroy () MUST be called on the
/// returned pointer at some point in the future. Otherwise, there will be a
/// memory leak.
/*!
 *
 *  @param num_nbs_jitters	The number of nonbusy sleep jitter samples to
 *  				store in an array within this data structure.
 *  				The sleep jitter samples are used to determine
 *  				the likely amount of sleep error on the next
 *  				program sleep.
 *  				If this value too large, then program sleeps
 *  				will be very slow (because calculating the
 *  				average sleep jitter will be slow).
 *  				The recommended value for this argument is 8.
 *
 *  @param init_nbs_jitter  	The average nonbusy sleep jitter value to
 *  				assume at the creation of this #ST_SysTime
 *  				structure. This value will be used to intialize
 *  				the nonbusy sleep jitters record. The
 *  				recommended value is 100 us.
 *
 *  @param num_tbs_jitters	Same as #num_nbs_jitters, but for the busy
 *				sleep jitters. The recommended value is 8.
 *
 *  @param init_tbs_jitter	Same as #init_nbs_jitter, but for the busy
 *  				sleep jitters. The recommended value is 50 ns.
 *
 *  @param time_res_attempts	The number of times the monotonic time
 *  				resolution of the system should be attempted to
 *  				be determined before giving up. The recommended
 *  				value is 4096.
 *
 *  @param time_res_def		The default monotonic time resolution to assume
 *				in the event that the actual value could not be
 *				determined. The recommended value is 1 ns.
 *
 *  @param time_res_force_def	A flag indicating whether or not to force using
 *				#time_res_def for the time resolution.
 *
 *  @param time_res_success	Where to place a #ST_STD_RES flag indicating
 *				whether or not the time resolution was
 *				successfully determined. If #ST_SUCCESS is not
 *				placed here, then that means #time_res_def
 *				will be used. This argument can be NULL if this
 *				flag is not needed.
 *
 *  @param min_nbs_first_ps	The first sleep interval to use when
 *				determining the smallest system sleep.
 *				See ST_get_min_nb_sleep_ps () for details.
 *				The recommended value for this argument is
 *				1 ms.
 *
 *  @param min_nbs_max_error	This sets the acceptable sleep jitter ratio
 *				to use in the determination of the smallest
 *				nonbusy sleep interval. This does NOT represent
 *				the average sleep error in general. The
 *				recommended value for this argument is 0.25.
 *				Making this value too small will cause the
 *				smallest nonbusy sleep interval to be very
 *				large. However, making this value small will
 *				help prevent oversleeping for small nonbusy
 *				sleeps.
 *
 *  @param min_nbs_attempts	The upper limit on the number of attempts to
 *  				use when determining the smallest useful system
 *  				sleep. This argument is necessary because
 *  				ST_get_min_nb_sleep_ps () can fail if too many
 *  				sleeps get interrupted. This argument MUST be
 *  				greater than or equal to #min_nbs_samples. The
 *  				recommended value for this argument is (16 *
 *  				min_nbs_samples).
 *
 *  @param min_nbs_retries	The number of times to retry a singular sleep
 *				sleep interval (if necessary) in the
 *				determination of the smallest sleep interval.
 *				The recommended value for this argument is 16.
 *
 *  @param min_nbs_samples	The number of samples to take when determining
 *  				the smallest nonbusy system sleep. The higher
 *  				this value, the more time that process will
 *  				take. However, with more samples, the more
 *  				accurate the resulting value will be. The
 *  				recommended value for this argument is 8.
 *
 *  @param min_nbs_def		If determining the smallest sleep has failed,
 *  				then this value will be assumed to be the
 *  				smallest nonbusy system sleep. This value is
 *  				highly platform dependent, but on most Linux
 *  				systems I've tested, this value should be
 *  				400 us.
 *
 *  @param min_nbs_force_def	A flag indicating whether or not to force using
 *  				#min_nbs_def as the smallest nonbusy sleep.
 *
 *  @param min_nbs_success	Where to place a #ST_STD_RES flag indicating
 *				whether or not the smallest system sleep was
 *				successfully determined. If #ST_SUCCESS is not
 *				placed here, then that means #min_nbs_def will
 *				be used. This argument can be NULL if this flag
 *				is not needed.
 *
 *  @param min_tbs_ibs_factor	Timed busy sleeps (tbs) have minimum amount of
 *				time they can consume, which is usually larger
 *				than the minimum amount of time an instruction
 *				busy sleep (ibs) consumes. This factor chooses
 *				the point at which the sleep technique switches
 *				from tbs to ibs. For example, if this value is
 *				4, then ibs's will be used whenever the desired
 *				sleep interval is less than 4 times the minimum
 *				tbs interval. The recommended value for this
 *				argument is 32. ("ibs" sleeps aren't used all
 *				all the time because they can potentially give
 *				less accurate estimates for the elapsed time).
 *
 *  @param min_tbs_attempts	The upper limit on the number of attempts to
 *				allow in performing a singular timed busy sleep
 *				when determining the smallest useful tbs
 *				interval. The recommended value is 4096.
 *
 *  @param min_tbs_samples	The number of samples to take when determining
 *				the smallest tbs interval. The higher this
 *				value, the more accurate the result will be,
 *				but it will also consume more time. The
 *				recommended value for this argument is 256.
 *
 *  @param min_tbs_def		If determining the smallest tbs interval has
 *				failed, then this value will used as the
 *				smallest tbs interval. The recommended value
 *				for this argument is 70 ns.
 *
 *  @param min_nbs_force_def	A flag indicating whether or not to force using
 *  				#min_tbs_def as the smallest tbs interval.
 *
 *  @param min_nbs_success	Where to place a #ST_STD_RES flag indicating
 *				whether or not the smallest tbs interval was
 *				successfully determined. If #ST_SUCCESS is not
 *				placed here, then that means #min_nbs_def will
 *				be used. This argument can be NULL if this flag
 *				is not needed.
 *
 *  @param arb_op_attempts	The upper limit on the number of attempts to
 *				use when determining the "arbitrary operation
 *				interval". This value MUST be greater than or
 *				equal to #arb_op_num_avg. The recommended
 *				value is (16 * #arb_op_num_avg).
 *
 *  @param arb_op_retries	The max number of times to retry the OS timer
 *				functions inside of each calculation of the
 *				average "arbitrary operation interval". The
 *				recommended value for this argument is 4096.
 *
 *  @param arb_op_num_avg	The number of averages to collect regarding the
 *				average execution time of the SlyTime
 *				"arbitrary operation". The recommended value
 *				for this argument is 256.
 *
 *  @param arb_op_smp_per_avg	The number of times to execute the SlyTime
 *  				arbitrary operation (arb_op) before calculating
 *  				each average execution time. This argument must
 *  				large enough so that the time in between
 *  				getting the start/stop times with OS system
 *  				calls is small compared to the amount of time
 *  				spent executing the arb_op. The recommended
 *  				value for this argument is 25000. It is
 *  				critical to note that (#arb_op_num_avg *
 *  				#arb_op_smp_per_avg) determines the total
 *  				number of times the arb_op will be executed in
 *  				this function. Ergo, the time spent executing
 *  				arb_ops is given by (arb_op_interval *
 *  				#arb_op_num_avg * #arb_op_smp_per_avg)
 *
 *  @param arb_op_def		If determining the arb_op interval has failed,
 *  				then this value will used as the arb op
 *  				interval. The recommended value for this
 *  				argument is 10.5 ns.
 *
 *  @param arb_op_force_def	A flag indicating whether or not to force using
 *  				#arb_op_def as the smallest tbs interval.
 *
 *  @param arb_op_success	Where to place a #ST_STD_RES flag indicating
 *				whether or not the arb_op interval was
 *				successfully determined. If #ST_SUCCESS is not
 *				placed here, then that means #arb_op_def will
 *				be used. This argument can be NULL if this flag
 *				is not needed.
 *
 *  @return			A pointer to the newly created ST_SysTime
 *				data structure, or NULL if the creation of the
 *				structure failed
 */

ST_SysTime * ST_SysTime_create_opt
(
	u32		num_nbs_jitters,
	ST_Ps		init_nbs_jitter,
	u32		num_tbs_jitters,
	ST_Ps		init_tbs_jitter,

	u32		time_res_attempts,
	ST_Ps 		time_res_def,
	u32		time_res_force_def,
	ST_STD_RES *	time_res_success,

	ST_Ps		min_nbs_first_ps,
	f64		min_nbs_max_error,
	u32		min_nbs_attempts,
	u32		min_nbs_retries,
	u32		min_nbs_samples,
	ST_Ps 		min_nbs_def,
	u32		min_nbs_force_def,
	ST_STD_RES *	min_nbs_success,

	f64		min_tbs_ibs_factor,
	u32		min_tbs_attempts,
	u32		min_tbs_samples,
	ST_Ps		min_tbs_def,
	u32		min_tbs_force_def,
	ST_STD_RES *	min_tbs_success,

	u32		arb_op_attempts,
	u32		arb_op_retries,
	u32		arb_op_num_avg,
	u32		arb_op_smp_per_avg,
	ST_Ps		arb_op_def,
	u32		arb_op_force_def,
	ST_STD_RES *	arb_op_success
)
{
	// Sanity check on arguments

	if (0 > min_nbs_max_error)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"min_nbs_max_error is %lf, which is negative "
			"and likely incorrect.",
			min_nbs_max_error
		);
	}

	if (1024 < num_nbs_jitters)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"num_nbs_jitters is %" PRIu32 ", which "
			"will likely cause program sleeps to be "
			"inefficient because it is a large value.",
			num_nbs_jitters
		);
	}

	if (1024 < num_tbs_jitters)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"num_tbs_jitters is %" PRIu32 ", which "
			"will likely cause program sleeps to be "
			"inefficient because it is a large value.",
			num_tbs_jitters
		);
	}


	// Allocate memory for the data-structure, and then allocate memory for
	// the #ST_PsRecord's that store jitter values

	ST_SysTime * stp = malloc (sizeof (ST_SysTime));

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, NULL);


	stp->nbs_jitter.samples = malloc (sizeof (ST_Ps) * num_nbs_jitters);

	if (NULL == stp->nbs_jitter.samples)
	{
		free (stp);

		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Could not allocate #stp->nbs_jitter.samples"
		);

		return NULL;
	}


	stp->tbs_jitter.samples = malloc (sizeof (ST_Ps) * num_tbs_jitters);

	if (NULL == stp->tbs_jitter.samples)
	{
		free (stp);
		free (stp->nbs_jitter.samples);

		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Could not allocate #stp->tbs_jitter.samples"
		);

		return NULL;
	}


	// Initialize the #ST_Ps structures that keep track of the sleep
	// jitters

	ST_PsRecord_set_all
	(
	 	&(stp->nbs_jitter),
		num_nbs_jitters,
		stp->nbs_jitter.samples,
		init_nbs_jitter
	);

	ST_PsRecord_set_all
	(
		&(stp->tbs_jitter),
		num_tbs_jitters,
		stp->tbs_jitter.samples,
		init_tbs_jitter
	);


	// Determine the smallest interval of time that can be measured by the
	// running system

	ST_STD_RES std_res = ST_SUCCESS;

	if (time_res_force_def)
	{
		stp->monotonic_res_ps = time_res_def;


		if (NULL != time_res_success)
		{
			*time_res_success = ST_NO_WORK;
		}
	}

	else
	{
		std_res =

		ST_SysTime_update_monotonic_time_res
		(
			stp,
			time_res_attempts,
			time_res_def
		);


		if (NULL != time_res_success)
		{
			*time_res_success = std_res;
		}


		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_L_DEBUG,
				"Using default value for #monotonic_res_ps "
				"for #ST_SysTime at %p",
				stp
			);
		}
	}


	// Determine the smallest non-busy sleep interval possible.


	stp->min_nbs_max_error = min_nbs_max_error;


	if (min_nbs_force_def)
	{
		stp->min_nbs_ps = min_nbs_def;


		if (NULL != min_nbs_success)
		{
			*min_nbs_success = ST_NO_WORK;
		}
	}

	else
	{
		std_res =

		ST_SysTime_update_min_nbs_ps
		(
			stp,
			stp->monotonic_res_ps,
			min_nbs_first_ps,
			min_nbs_max_error,
			min_nbs_attempts,
			min_nbs_retries,
			min_nbs_samples,
			min_nbs_def
		);


		if (NULL != min_nbs_success)
		{
			*min_nbs_success = std_res;
		}


		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_L_DEBUG,
				"Using default value for #min_nbs_ps "
				"for #ST_SysTime at %p",
				stp
			);
		}
	}




	// Determine the smallest timed busy sleep interval

	stp->min_tbs_ibs_factor = min_tbs_ibs_factor;

	if (min_tbs_force_def)
	{
		stp->min_tbs_ps = min_tbs_def;


		if (NULL != min_tbs_success)
		{
			*min_tbs_success = ST_NO_WORK;
		}
	}

	{
		std_res =

		ST_SysTime_update_min_tbs_ps
		(
					stp,
					min_tbs_attempts,
					min_tbs_samples,
					min_tbs_def
		);


		if (NULL != min_tbs_success)
		{
			*min_tbs_success = std_res;
		}


		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_L_DEBUG,
				"Using default value for #min_tbs_ps "
				"for #ST_SysTime at %p",
				stp
			);
		}
	}



	// Determine the amount of time it takes to execute ST_arb_op ()

	if (arb_op_force_def)
	{
		stp->arb_op_ps = arb_op_def;


		if (NULL != arb_op_success)
		{
			*arb_op_success = ST_NO_WORK;
		}
	}

	else
	{
		std_res =

		ST_SysTime_update_arb_op_ps
		(
			stp,
			arb_op_attempts,
			arb_op_retries,
			arb_op_num_avg,
			arb_op_smp_per_avg,
			arb_op_def
		);


		if (NULL != arb_op_success)
		{
			*arb_op_success = std_res;
		}


		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_L_DEBUG,
				"Using default value for #arb_op_ps "
				"for #ST_SysTime at %p",
				stp
			);
		}
	}


	return stp;
}




/// Creates a copy of an ST_SysTime data structure, which is significantly
/// faster than creating one from scratch.
///
/// To characterize the running system, ST_SysTime_create_opt () necessarily
/// must observe several sleeps, which can take several milliseconds to
/// complete. ST_SysTime_create_copy () will just take all of its values from
/// an existing ST_SysTime object, and so it does not have to repeat any of
/// these sleeps.
///
/// @warning
/// After using this function, ST_SysTime_destroy () MUST be called on the
/// returned pointer at some point in the future. Otherwise, there will be a
/// memory leak.
/*!
 *  @param src_stp		Pointer to the #ST_SysTime data structure to
 * 				copy.
 *
 *  @param res_ptr		Pointer to where to place an #ST_STD_RES value
 * 				which either indicates #ST_SUCCESS or the
 * 				reason why this function failed. This can be
 * 				NULL if this value is not desired.
 *
 *  @return			A pointer to the newly created copy of
 * 				#src_stp, or NULL if the creation of the object
 * 				failed.
 */

ST_SysTime * ST_SysTime_create_copy
(
	ST_SysTime *	src_stp,
	ST_STD_RES *	res_ptr
)
{
	//Sanity check on arguments

	if (NULL == src_stp)
	{
		if (NULL != res_ptr)
		{
			*res_ptr = ST_BAD_ARG;
		}

		ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, src_stp, NULL);
	}


	//Allocate memory for the #ST_SysTime data-structure, but not for the
	//internal #ST_PsRecord arrays yet.

	ST_SysTime * stp = malloc (sizeof (ST_SysTime));

	if (NULL == stp)
	{
		if (NULL != res_ptr)
		{
			*res_ptr = ST_BAD_ALLOC;
		}

		ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, NULL);
	}


	//Copy the members of #src_stp over to #stp one-by-one

	stp->monotonic_res_ps =		src_stp->monotonic_res_ps;
	stp->min_nbs_ps =		src_stp->min_nbs_ps;
	stp->min_nbs_max_error =	src_stp->min_nbs_max_error;
	stp->min_tbs_ps =		src_stp->min_tbs_ps;
	stp->min_tbs_ibs_factor =	src_stp->min_tbs_ibs_factor;
	stp->arb_op_ps =		src_stp->arb_op_ps;


	//Copy the internal #ST_PsRecord members from #src_stp

	stp->nbs_jitter = src_stp->nbs_jitter;
	stp->tbs_jitter = src_stp->tbs_jitter;


	//Make sure that the pointers inside the internal #ST_PsRecord's refer
	//to different arrays than the ones in #src_stp.
	//
	//(#ST_PsRecord's do not perform their own memory management, which is
	//why #ST_SysTime can contain them as a raw structure. Otherwise, this
	//logic would be replaced with something like ST_PsRecord_copy ())

	stp->nbs_jitter.samples = NULL;
	stp->tbs_jitter.samples = NULL;


	stp->nbs_jitter.samples = malloc (sizeof (ST_Ps) * src_stp->nbs_jitter.num_samples);

	if (NULL == stp->nbs_jitter.samples)
	{
		free (stp);

		if (NULL != res_ptr)
		{
			*res_ptr = ST_BAD_ALLOC;
		}

		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Could not allocate #stp->nbs_jitter.samples"
		);

		return NULL;
	}


	stp->tbs_jitter.samples = malloc (sizeof (ST_Ps) * src_stp->tbs_jitter.num_samples);

	if (NULL == stp->tbs_jitter.samples)
	{
		free (stp);
		free (stp->nbs_jitter.samples);

		if (NULL != res_ptr)
		{
			*res_ptr = ST_BAD_ALLOC;
		}

		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Could not allocate #stp->tbs_jitter.samples"
		);

		return NULL;

	}


	//Now that the #ST_PsRecord arrays in #stp are in distinct memory
	//locations from the ones in #src_stp, we have to copy the array
	//elements from #src_stp to #stp.

	for (u32 sample_sel = 0; sample_sel < stp->nbs_jitter.num_samples; sample_sel += 1)
	{
		stp->nbs_jitter.samples [sample_sel] =

		src_stp->nbs_jitter.samples [sample_sel];
	}


	for (u32 sample_sel = 0; sample_sel < stp->tbs_jitter.num_samples; sample_sel += 1)
	{
		stp->tbs_jitter.samples [sample_sel] =

		src_stp->tbs_jitter.samples [sample_sel];
	}


	//Indicate success, and return the newly created #ST_SysTime object

	if (NULL != res_ptr)
	{
		*res_ptr = ST_SUCCESS;
	}


	return stp;
}




/// Creates an ST_SysTime data-structure, which stores the timing
/// characteristics of the current system. This information is useful for
/// performing highly accurate timed sleeps in a program.
///
/// This can be thought of as a version of ST_SysTime_create_opt () with all of
/// its arguments sets to default values.
///
/// @warning
/// This function must perform several program sleeps of various sizes to
/// characterize the running system. It may take several milliseconds to let
/// these sleeps complete (usually about 80 ms in total). As a result, this
/// function should NOT be run in a tight loop. It is much more efficient to
/// create this structure once, and then re-use it wherever needed.
///
/// @warning
/// After using this function, ST_SysTime_destroy () MUST be called on the
/// returned pointer at some point in the future. Otherwise, there will be a
/// memory leak.
/*!
 *
 *  @return			A pointer to the newly created ST_SysTime data
 *				structure, or NULL if the creation of the
 *				structure failed
 */

//TODO: Perhaps this function should have a #res_ptr argument like
//ST_SysTime_create_copy () so that more detailed errors can be returned

ST_SysTime * ST_SysTime_create ()
{
	// Select the default values for the #ST_SysTime_create_opt() call

	const u32	num_nbs_jitters		= ST_SYS_TIME_CREATE_NUM_NBS_JITTERS;
	const ST_Ps	init_nbs_jitter		= {.ps = ST_SYS_TIME_CREATE_INIT_NBS_JITTER};
	const u32	num_tbs_jitters		= ST_SYS_TIME_CREATE_NUM_TBS_JITTERS;
	const ST_Ps	init_tbs_jitter		= {.ps = ST_SYS_TIME_CREATE_INIT_TBS_JITTER};

	const u32	time_res_attempts	= ST_SYS_TIME_CREATE_TIME_RES_ATTEMPTS;
	const ST_Ps 	time_res_def		= {.ps = ST_SYS_TIME_CREATE_TIME_RES_DEF};
	const u32	time_res_force_def	= ST_SYS_TIME_CREATE_TIME_RES_FORCE_DEF;
	ST_STD_RES *	time_res_success	= NULL;

	const ST_Ps	min_nbs_first_ps	= {.ps = ST_SYS_TIME_CREATE_MIN_NBS_FIRST_PS};
	const f64	min_nbs_max_error	= ST_SYS_TIME_CREATE_MIN_NBS_MAX_ERROR;
	const u32	min_nbs_attempts	= ST_SYS_TIME_CREATE_MIN_NBS_ATTEMPTS;
	const u32	min_nbs_retries		= ST_SYS_TIME_CREATE_MIN_NBS_RETRIES;
	const u32	min_nbs_samples		= ST_SYS_TIME_CREATE_MIN_NBS_SAMPLES;
	const ST_Ps 	min_nbs_def		= {.ps = ST_SYS_TIME_CREATE_MIN_NBS_DEF};
	const u32	min_nbs_force_def	= ST_SYS_TIME_CREATE_MIN_NBS_FORCE_DEF;
	ST_STD_RES *	min_nbs_success		= NULL;

	const f64	min_tbs_ibs_factor	= ST_SYS_TIME_CREATE_MIN_TBS_IBS_FACTOR;
	const u32	min_tbs_attempts	= ST_SYS_TIME_CREATE_MIN_TBS_ATTEMPTS;
	const u32	min_tbs_samples		= ST_SYS_TIME_CREATE_MIN_TBS_SAMPLES;
	const ST_Ps	min_tbs_def		= {.ps = ST_SYS_TIME_CREATE_MIN_TBS_DEF};
	const u32	min_tbs_force_def	= ST_SYS_TIME_CREATE_MIN_TBS_FORCE_DEF;
	ST_STD_RES *	min_tbs_success		= NULL;

	const u32	arb_op_attempts		= ST_SYS_TIME_CREATE_ARB_OP_ATTEMPTS;
	const u32	arb_op_retries		= ST_SYS_TIME_CREATE_ARB_OP_RETRIES;
	const u32	arb_op_num_avg		= ST_SYS_TIME_CREATE_ARB_OP_NUM_AVG;
	const u32	arb_op_smp_per_avg	= ST_SYS_TIME_CREATE_ARB_OP_SMP_PER_AVG;
	const ST_Ps	arb_op_def		= {.ps = ST_SYS_TIME_CREATE_ARB_OP_DEF};
	const u32	arb_op_force_def	= ST_SYS_TIME_CREATE_ARB_OP_FORCE_DEF;
	ST_STD_RES *	arb_op_success		= NULL;


	return	ST_SysTime_create_opt
		(
			num_nbs_jitters,
			init_nbs_jitter,
			num_tbs_jitters,
			init_tbs_jitter,

			time_res_attempts,
			time_res_def,
			time_res_force_def,
			time_res_success,

			min_nbs_first_ps,
			min_nbs_max_error,
			min_nbs_attempts,
			min_nbs_retries,
			min_nbs_samples,
			min_nbs_def,
			min_nbs_force_def,
			min_nbs_success,

			min_tbs_ibs_factor,
			min_tbs_attempts,
			min_tbs_samples,
			min_tbs_def,
			min_tbs_force_def,
			min_tbs_success,

			arb_op_attempts,
			arb_op_retries,
			arb_op_num_avg,
			arb_op_smp_per_avg,
			arb_op_def,
			arb_op_force_def,
			arb_op_success
		);
}




/// Destroys a #ST_SysTime data structure that was previously created with
/// ST_SysTime_create_opt().
/*!
 *  @param stp			The #ST_SysTime to destroy
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_destroy (ST_SysTime * stp)
{
	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ARG);


	// First, deallocate any memory made for the #ST_PsRecord's

	free (stp->nbs_jitter.samples);
	free (stp->tbs_jitter.samples);


	// Second, destroy the data structure itself

	free (stp);


	return ST_SUCCESS;
}




/// Performs a unit test of the basic operations of the #ST_PsRecord data
/// #ST_PsRecord data structure
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_PsRecord_unit_test ()
{
	printf ("Test creating an #ST_PsRecord() and adding samples to it...\n\n");


	// Create an array to hold the #ST_Ps samples

	u32 num_samples = 10;

	ST_Ps * samples = malloc (sizeof (ST_Ps) * num_samples);

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, samples, ST_BAD_ALLOC);


	// Create a #ST_PsRecord

	ST_Ps init_val = {.ps = 100000};

	ST_PsRecord tpr;


	ST_PsRecord_set_all (&tpr, num_samples, samples, init_val);


	// Print the initial state of the #ST_PsRecord

	ST_PsRecord_print (stdout, &tpr);

	printf ("\n\n");


	// Add a few arbitrary samples to the #ST_PsRecord

	ST_Ps sample_val;


	sample_val.ps = -1;

	ST_PsRecord_add_sample (&tpr, sample_val);


	sample_val.ps = -3;

	ST_PsRecord_add_sample (&tpr, sample_val);


	sample_val.ps = -5;

	ST_PsRecord_add_sample (&tpr, sample_val);


	ST_PsRecord_print (stdout, &tpr);

	printf ("\n\n");


	// Enter new samples in a loop, guaranteed to cause wrap-around in the
	// #ST_PsRecord

	for (u32 sample_num = 0; sample_num < tpr.num_samples; sample_num += 1)
	{
		sample_val.ps = (ST_PsNum) sample_num;

		ST_PsRecord_add_sample (&tpr, sample_val);

		ST_PsRecord_print (stdout, &tpr);

		printf ("\n\n");
	}

	printf ("\n");


	//Free the memory allocated in this function

	free (samples);


	return ST_SUCCESS;
}




/// Runs a basic unit test on the #ST_SysTime data structure
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_unit_test ()
{

	printf ("Test the creation of a #ST_SysTime data structure...\n\n");


	// Test creating a #ST_SysTime

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, stp, ST_BAD_ALLOC);


	ST_SysTime_print (stdout, stp);


	//Record some arbitrary samples for the nonbusy and busy #ST_PsRecord's

	for (u32 sample_num = 0; sample_num < 10; sample_num += 1)
	{
		ST_Ps a_ps = {.ps = (ST_PsNum) (sample_num)};
		ST_Ps b_ps = {.ps = (ST_PsNum) (sample_num)};

		ST_SysTime_record_nbs_jitter (stp, a_ps);
		ST_SysTime_record_tbs_jitter (stp, b_ps);

		ST_SysTime_print (stdout, stp);

		printf ("\n\n\n");
	}


	// Test taking the average of the sleep jitters

	ST_Ps avg_jitter = {.ps = 0};


	ST_SysTime_avg_nbs_jitter (stp, &avg_jitter);

	printf ("The average nonbusy sleep jitter is %" ST_PRI_ST_PsNum " ps\n\n", avg_jitter.ps);


	ST_SysTime_avg_tbs_jitter (stp, &avg_jitter);

	printf ("The average busy sleep jitter is %" ST_PRI_ST_PsNum " ps\n\n\n", avg_jitter.ps);


	// Destroy the ST_SysTime structure

	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a basic unit test on ST_SysTime_create_copy ()
/*!
 *  @return				Standard status code
 */

ST_STD_RES ST_SysTime_create_copy_unit_test ()
{
	printf ("Testing creating copies of ST_SysTime's with ST_SysTime_create_copy ()....\n\n");


	//Create an original #ST_SysTime structure, and then perform a sleep
	//with it so that the sleep jitters stored in it get modified
	
	ST_SysTime * orig_stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, orig_stp, ST_BAD_ALLOC);


	printf ("Performing sleep with #orig_stp...\n\n");

	ST_Ps sleep_ps = {.ps = (ST_PsNum) 500 * 1000 * 1000 * 1000};

	ST_SysTime_nbs (orig_stp, sleep_ps, NULL);

	
	printf ("---orig_stp---\n\n");

	ST_SysTime_print (stdout, orig_stp);


	//Copy the previously created #ST_SysTime structure, and also perform a
	//sleep with it

	ST_STD_RES copy_res = ST_UNKNOWN_ERROR;

	ST_SysTime * copy_stp = ST_SysTime_create_copy (orig_stp, &copy_res);

	if (NULL == copy_stp)
	{
		ST_SysTime_destroy (orig_stp);

		ST_NULL_RETURN (ST_SYS_TIME_L_DEBUG, copy_stp, ST_BAD_ALLOC);
	}

	printf ("\n\nCopy attempt success : %s\n\n", ST_StdResStr_get (copy_res).str);

	printf ("---copy_stp---\n\n");

	ST_SysTime_print (stdout, copy_stp);


	printf ("\n\nPerforming sleep with #copy_stp...\n\n");

	ST_SysTime_nbs (copy_stp, sleep_ps, NULL);


	//Create and destroy several copies, so that any memory leaks can
	//easily be detected by external tools
	
	for (u32 copy_num = 0; copy_num < 1024; copy_num += 1)
	{
		ST_SysTime * temp_stp = ST_SysTime_create_copy (orig_stp, NULL);

		if (NULL != temp_stp)
		{
			ST_SysTime_destroy (temp_stp);
		}
	}


	//Destroy the remaining #ST_SysTime object

	ST_SysTime_destroy (orig_stp);
	ST_SysTime_destroy (copy_stp);


	return ST_SUCCESS;

}




/// Performs a unit test that measures the accuracy of ST_SysTime_tbs_opt ()
/*!
 *  @return				Standard status code
 */

//FIXME : There is a LOT of code duplication between
//ST_SysTime_tbs_opt_unit_test () and ST_SysTime_nbs_opt_unit_test ()

ST_STD_RES ST_SysTime_tbs_opt_unit_test ()
{
	printf
	(
		"Testing the performance of BUSY sleeps by "
		"ST_SysTime_tbs_opt ()...\n\n\n"
	);


	// Create a #ST_SysTime to use in this test

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);


	// Holds the amount of time that a sleep attempt took

	ST_Ps used_ps = {.ps = 0};


	// Perform several sleeps to get realistic values for the sleep jitter
	// in #stp.

	ST_Ps sleep_ps = {.ps = 1 * 1000 * 1000 * 1000};

	for (u32 sleep_num = 0; sleep_num < stp->tbs_jitter.num_samples; sleep_num += 1)
	{
		ST_SysTime_tbs_opt
		(
			stp,
			sleep_ps,
			ST_SYS_TIME_NBS_BUSY_RETRIES,
			&used_ps
		);
	}

	ST_SysTime_print (stdout, stp);
	printf ("\n\n");


	// Set intial parameters for the test loop regarding how many orders of
	// magnitude to test

	const u64 NUM_ORDERS_OF_MAG =	9;
	const u64 ORDER_OF_MAG_RATIO =	10;


	// Make the first sleep interval equal to 1 ns, and make sure that a
	// group of sleeps consumes some minimum amount of time so that it can
	// be measured with a stop-watch reliably

	sleep_ps.ps = 1000;

	ST_Ps min_req_total_ps = {.ps = (stp->min_nbs_ps.ps * 10)};


	// These variables are used to keep track of the total sleep time
	// requested, and the total sleep time as reported by the sleep
	// function itself

	ST_Ps req_total_ps = {.ps = 0};

	ST_Ps reported_total_ps = {.ps = 0};


	// Enter a test loop where sleeps of different sizes are tested

	for (u32 order = 0; order < NUM_ORDERS_OF_MAG; order += 1)
	{
		// Start a "stop-watch"

		const u32 STOP_WATCH_RETRIES = 1024;

		struct timespec start_ts = {.tv_sec = 0, .tv_nsec = 0};

		ST_STD_RES res = ST_get_monotonic_time_eager (&start_ts, STOP_WATCH_RETRIES);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not start stop-watch!"
			);

			continue;
		}


		// Perform the actual sleep

		req_total_ps.ps = 0;

		reported_total_ps.ps = 0;

		while (req_total_ps.ps < min_req_total_ps.ps)
		{
			res =	ST_SysTime_tbs_opt
				(
					stp,
					sleep_ps,
					ST_SYS_TIME_NBS_BUSY_RETRIES,
					&used_ps
				);


			if (ST_SUCCESS != res)
			{
				ST_DEBUG_PRINT
				(
					ST_SYS_TIME_H_DEBUG,
					"Could not get perform the sleep for "
					"this test iteration"
				);

				continue;

			}


			req_total_ps.ps += sleep_ps.ps;

			reported_total_ps.ps += used_ps.ps;
		}


		// Stop the "stop-watch"

		struct timespec stop_ts = {.tv_sec = 0, .tv_nsec = 0};

		res = ST_get_monotonic_time_eager (&stop_ts, STOP_WATCH_RETRIES);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not stop stop-watch!"
			);

			continue;
		}


		// Measure the actual amount of time used by stopping the
		// stop-watch

		struct timespec measured_total_ts = ST_timespec_abs_diff (stop_ts, start_ts);

		ST_Ps measured_total_ps = {.ps = 0};

		ST_timespec_to_ST_Ps (measured_total_ts, &measured_total_ps);


		// Calculate the accuracy of this sleep as reported by the
		// sleep function itself

		ST_Ps reported_jitter_ps = ST_Ps_diff (reported_total_ps, req_total_ps);

		f64 reported_jitter_rate = ((f64) reported_jitter_ps.ps / (f64) req_total_ps.ps);


		// Calculate the accuracy of this sleep as measured by the
		// stop-watch

		ST_Ps measured_jitter_ps = ST_Ps_diff (measured_total_ps, req_total_ps);

		f64 measured_jitter_rate = ((f64) measured_jitter_ps.ps / (f64) req_total_ps.ps);


		// Print out this test iteration's results

		printf ("Single Sleep          : %" ST_PRI_ST_PsNum " ps\n", sleep_ps.ps);

		printf ("Requested Total Sleep : %" ST_PRI_ST_PsNum " ps\n", req_total_ps.ps);

		printf ("Reported Total Time   : %" ST_PRI_ST_PsNum " ps\n", reported_total_ps.ps);

		printf ("Reported Sleep Jitter : %+" ST_PRI_ST_PsNum " ps\n", reported_jitter_ps.ps);

		printf ("Reported Error Rate   : %lf%%\n", 100 * reported_jitter_rate);

		printf ("Measured Total Time   : %" ST_PRI_ST_PsNum " ps\n", measured_total_ps.ps);

		printf ("Measured Sleep Jitter : %+" ST_PRI_ST_PsNum " ps\n", measured_jitter_ps.ps);

		printf ("Measured Error Rate   : %lf%%\n", 100 * measured_jitter_rate);

		printf ("\n");


		// Make the next sleep larger

		sleep_ps.ps = (ST_PsNum) (sleep_ps.ps * (ST_PsNum) ORDER_OF_MAG_RATIO);

	}

	printf ("\n\n");


	// Destroy all objects allocated in this function

	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a unit test that measures the accuracy of ST_SysTime_nbs_opt ()
/*!
 *  @return				Standard status code
 */

ST_STD_RES ST_SysTime_nbs_opt_unit_test ()
{
	printf
	(
		"Testing the performance of NON-busy sleeps by "
		"ST_SysTime_nbs_opt ()...\n\n\n"
	);


	// Create a #ST_SysTime to use in this test

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);


	// Holds the amount of time that a sleep attempt took

	ST_Ps used_ps = {.ps = 0};


	// Perform several sleeps to get realistic values for the sleep jitter
	// in #stp.
	//
	// Both non-busy and busy sleeps are performed so that jitter values
	// for both can be gathered.

	ST_Ps sleep_ps = {.ps = 1 * 1000 * 1000 * 1000};

	for (u32 sleep_num = 0; sleep_num < stp->nbs_jitter.num_samples; sleep_num += 1)
	{
		ST_SysTime_nbs_opt
		(
			stp,
			sleep_ps,
			ST_SYS_TIME_NBS_JITTER_RATIO,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			ST_SYS_TIME_NBS_BUSY_RETRIES,
			&used_ps
		);
	}

	for (u32 sleep_num = 0; sleep_num < stp->tbs_jitter.num_samples; sleep_num += 1)
	{
		ST_SysTime_tbs_opt
		(
			stp,
			sleep_ps,
			ST_SYS_TIME_NBS_BUSY_RETRIES,
			&used_ps
		);
	}

	ST_SysTime_print (stdout, stp);
	printf ("\n\n");


	// Set intial parameters for the test loop regarding how many orders of
	// magnitude to test

	const u64 NUM_ORDERS_OF_MAG =	9;
	const u64 ORDER_OF_MAG_RATIO =	10;


	// Make the first sleep interval equal to 1 ns, and make sure that a
	// group of sleeps consumes some minimum amount of time so that it can
	// be measured with a stop-watch reliably

	sleep_ps.ps = 1000;

	ST_Ps min_req_total_ps = {.ps = (stp->min_nbs_ps.ps * 10)};


	// These variables are used to keep track of the total sleep time
	// requested, and the total sleep time as reported by the sleep
	// function itself

	ST_Ps req_total_ps = {.ps = 0};

	ST_Ps reported_total_ps = {.ps = 0};


	// Enter a test loop where sleeps of different sizes are tested

	for (u32 order = 0; order < NUM_ORDERS_OF_MAG; order += 1)
	{
		// Start a "stop-watch"

		const u32 STOP_WATCH_RETRIES = 1024;

		struct timespec start_ts = {.tv_sec = 0, .tv_nsec = 0};

		ST_STD_RES res = ST_get_monotonic_time_eager (&start_ts, STOP_WATCH_RETRIES);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not start stop-watch!"
			);

			continue;
		}


		// Perform the actual sleep

		req_total_ps.ps = 0;

		reported_total_ps.ps = 0;

		while (req_total_ps.ps < min_req_total_ps.ps)
		{
			res =	ST_SysTime_nbs_opt
				(
					stp,
					sleep_ps,
					ST_SYS_TIME_NBS_JITTER_RATIO,
					ST_SYS_TIME_NBS_NONBUSY_RETRIES,
					ST_SYS_TIME_NBS_BUSY_RETRIES,
					&used_ps
				);


			if (ST_SUCCESS != res)
			{
				ST_DEBUG_PRINT
				(
					ST_SYS_TIME_H_DEBUG,
					"Could not get perform the sleep for "
					"this test iteration"
				);

				continue;

			}


			req_total_ps.ps += sleep_ps.ps;

			reported_total_ps.ps += used_ps.ps;
		}


		// Stop the "stop-watch"

		struct timespec stop_ts = {.tv_sec = 0, .tv_nsec = 0};

		res = ST_get_monotonic_time_eager (&stop_ts, STOP_WATCH_RETRIES);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_SYS_TIME_H_DEBUG,
				"Could not stop stop-watch!"
			);

			continue;
		}


		// Measure the actual amount of time used by stopping the
		// stop-watch

		struct timespec measured_total_ts = ST_timespec_abs_diff (stop_ts, start_ts);

		ST_Ps measured_total_ps = {.ps = 0};

		ST_timespec_to_ST_Ps (measured_total_ts, &measured_total_ps);


		// Calculate the accuracy of this sleep as reported by the
		// sleep function itself

		ST_Ps reported_jitter_ps = ST_Ps_diff (reported_total_ps, req_total_ps);

		f64 reported_jitter_rate = ((f64) reported_jitter_ps.ps / (f64) req_total_ps.ps);


		// Calculate the accuracy of this sleep as measured by the
		// stop-watch

		ST_Ps measured_jitter_ps = ST_Ps_diff (measured_total_ps, req_total_ps);

		f64 measured_jitter_rate = ((f64) measured_jitter_ps.ps / (f64) req_total_ps.ps);


		// Print out this test iteration's results

		printf ("Single Sleep          : %" ST_PRI_ST_PsNum " ps\n", sleep_ps.ps);

		printf ("Requested Total Sleep : %" ST_PRI_ST_PsNum " ps\n", req_total_ps.ps);

		printf ("Reported Total Time   : %" ST_PRI_ST_PsNum " ps\n", reported_total_ps.ps);

		printf ("Reported Sleep Jitter : %+" ST_PRI_ST_PsNum " ps\n", reported_jitter_ps.ps);

		printf ("Reported Error Rate   : %lf%%\n", 100 * reported_jitter_rate);

		printf ("Measured Total Time   : %" ST_PRI_ST_PsNum " ps\n", measured_total_ps.ps);

		printf ("Measured Sleep Jitter : %+" ST_PRI_ST_PsNum " ps\n", measured_jitter_ps.ps);

		printf ("Measured Error Rate   : %lf%%\n", 100 * measured_jitter_rate);

		printf ("\n");


		// Make the next sleep larger

		sleep_ps.ps = (ST_PsNum) (sleep_ps.ps * (ST_PsNum) ORDER_OF_MAG_RATIO);

	}

	printf ("\n\n");


	// Destroy all objects allocated in this function

	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a basic unit test on the ST_SysTime_nbs_opt_calc_error () function
/*!
 *  @return				Standard status code
 */

ST_STD_RES ST_SysTime_nbs_opt_calc_error_unit_test ()
{
	printf
	(
		"Testing the calculation of nbs sleep error with "
		"ST_SysTime_nbs_opt_calc_error ()...\n\n"
	);


	//Make a default #ST_SysTime

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);


	//Use ST_SysTime_nbs_opt_calc_error () to get the error associated with
	//different sleep intervals

	const u32 NUM_ORDERS_OF_MAG =	9;

	const u32 ORDER_OF_MAG_RATIO =	10;


	ST_Ps sleep_ps =		{.ps = 1 * 1000};

	ST_Ps min_total_sleep_ps =	{.ps = 1000 * 1000 * 1000};

	f64 error =			0;


	for (u32 order_num = 0; order_num < NUM_ORDERS_OF_MAG; order_num += 1)
	{
		u64 num_sleeps = (min_total_sleep_ps.ps / sleep_ps.ps) + 1;


		ST_STD_RES res =

		ST_SysTime_nbs_opt_calc_error
		(
			stp,
			sleep_ps,
			ST_SYS_TIME_NBS_JITTER_RATIO,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			ST_SYS_TIME_NBS_BUSY_RETRIES,
			num_sleeps,
			&error
		);


		if (ST_SUCCESS != res)
		{
			continue;
		}


		printf
		(
			"sleep_ps = %" ST_PRI_ST_PsNum " ps\n"
			"error    = %lf%%\n\n",
			sleep_ps.ps,
			100 * error
		);


		sleep_ps.ps = sleep_ps.ps * ORDER_OF_MAG_RATIO;
	}


	//Destroy the #ST_SysTime object used in this unit test
	
	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a basic unit test on the ST_SysTime_nbs_calc_error () function
/*!
 *  @return				Standard status code
 */

//FIXME : There is some code duplication between this function and
//ST_SysTime_nbs_opt_calc_error_unit_test ()

ST_STD_RES ST_SysTime_nbs_calc_error_unit_test ()
{
	printf
	(
		"Testing the calculation of nbs sleep error with "
		"ST_SysTime_nbs_calc_error ()...\n\n"
	);


	//Make a default #ST_SysTime

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);


	//Use ST_SysTime_nbs_calc_error () to get the error associated with
	//different sleep intervals

	const u32 NUM_ORDERS_OF_MAG =	9;

	const u32 ORDER_OF_MAG_RATIO =	10;


	ST_Ps sleep_ps =		{.ps = 1 * 1000};

	ST_Ps min_total_sleep_ps =	{.ps = 1000 * 1000 * 1000};

	f64 error =			0;


	for (u32 order_num = 0; order_num < NUM_ORDERS_OF_MAG; order_num += 1)
	{
		u64 num_sleeps = (min_total_sleep_ps.ps / sleep_ps.ps) + 1;


		ST_STD_RES res =

		ST_SysTime_nbs_calc_error
		(
			stp,
			sleep_ps,
			num_sleeps,
			&error
		);


		if (ST_SUCCESS != res)
		{
			continue;
		}


		printf
		(
			"sleep_ps = %" ST_PRI_ST_PsNum " ps\n"
			"error    = %lf%%\n\n",
			sleep_ps.ps,
			100 * error
		);


		sleep_ps.ps = sleep_ps.ps * ORDER_OF_MAG_RATIO;
	}


	//Destroy the #ST_SysTime object used in this unit test
	
	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a simple unit test on ST_SysTime_nbs_opt_calc_min_sleep ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_nbs_opt_calc_min_sleep_unit_test ()
{
	printf
	(
		"Testing the calculation of the minimum useful nbs sleep with "
		"ST_SysTime_nbs_opt_calc_min_sleep ()...\n\n"
	);


	//Make a default #ST_SysTime

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);


	//Use ST_SysTime_nbs_opt_calc_min_sleep () to determine the smallest
	//useful sleep with different maximum allowed errors

	const u32 NUM_ERROR_VALUES =    40;

	const f64 ERROR_VALUE_INCR =	0.005;

	const ST_Ps first_sleep_ps =	{.ps = 1000 * 1000 * 1000};

	const ST_Ps error_calc_min_ps =	{.ps = 1000 * 1000 * 1000};

	const ST_Ps min_bound_ps =	{.ps = 0};

	const ST_Ps def_min_nbs_ps =	{.ps = 777};


	f64 max_error =			0.0;

	ST_Ps min_nbs_ps =		{.ps = 0};


	for (u32 error_num = 0; error_num < NUM_ERROR_VALUES; error_num += 1)
	{
		ST_STD_RES res =

		ST_SysTime_nbs_opt_calc_min_sleep
		(
			stp,

			ST_SYS_TIME_NBS_JITTER_RATIO,
			ST_SYS_TIME_NBS_NONBUSY_RETRIES,
			ST_SYS_TIME_NBS_BUSY_RETRIES,

			first_sleep_ps,
			error_calc_min_ps,
			min_bound_ps,

			max_error,

			def_min_nbs_ps,
			&min_nbs_ps
		);


		printf
		(

		 	"res        = %s\n"
			"max_error  = %lf%%\n"
			"min_nbs_ps = %" ST_PRI_ST_PsNum " ps\n\n",
			ST_StdResStr_get (res).str,
			(100 * max_error),
			min_nbs_ps.ps
		);


		max_error = max_error + ERROR_VALUE_INCR;
	}


	//Destroy the #ST_SysTime object used in this unit test
	
	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a simple unit test on ST_SysTime_nbs_calc_min_sleep ()
/*!
 *  @return			Standard status code
 */

//FIXME : There is some code duplication between this function and
//ST_SysTime_nbs_opt_calc_min_sleep_unit_test ()

ST_STD_RES ST_SysTime_nbs_calc_min_sleep_unit_test ()
{
	printf
	(
		"Testing the calculation of the minimum useful nbs sleep with "
		"ST_SysTime_nbs_calc_min_sleep ()...\n\n"
	);


	//Make a default #ST_SysTime

	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);


	//Use ST_SysTime_nbs_calc_min_sleep () to determine the smallest
	//useful sleep with different maximum allowed errors

	const u32 NUM_ERROR_VALUES =    40;

	const f64 ERROR_VALUE_INCR =	0.005;

	const ST_Ps first_sleep_ps =	{.ps = 1000 * 1000 * 1000};

	const ST_Ps error_calc_min_ps =	{.ps = 1000 * 1000 * 1000};

	const ST_Ps min_bound_ps =	{.ps = 0};

	const ST_Ps def_min_nbs_ps =	{.ps = 777};


	f64 max_error =			0.0;

	ST_Ps min_nbs_ps =		{.ps = 0};


	for (u32 error_num = 0; error_num < NUM_ERROR_VALUES; error_num += 1)
	{
		ST_STD_RES res =

		ST_SysTime_nbs_calc_min_sleep
		(
			stp,

			first_sleep_ps,
			error_calc_min_ps,
			min_bound_ps,

			max_error,

			def_min_nbs_ps,
			&min_nbs_ps
		);


		printf
		(

		 	"res        = %s\n"
			"max_error  = %lf%%\n"
			"min_nbs_ps = %" ST_PRI_ST_PsNum " ps\n\n",
			ST_StdResStr_get (res).str,
			(100 * max_error),
			min_nbs_ps.ps
		);


		max_error = max_error + ERROR_VALUE_INCR;
	}


	//Destroy the #ST_SysTime object used in this unit test
	
	ST_SysTime_destroy (stp);


	return ST_SUCCESS;
}




/// Performs a very simple unit test on ST_SysTime_tbs ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_tbs_unit_test ()
{
	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);

	printf ("Performing smart busy sleep for 0.5 second...\n\n");

	ST_Ps sleep_ps = {.ps = (ST_PsNum) 500 * 1000 * 1000 * 1000};

	ST_SysTime_tbs (stp, sleep_ps, NULL);

	ST_SysTime_destroy (stp);

	return ST_SUCCESS;
}




/// Performs a very simple unit test on ST_SysTime_nbs ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_nbs_unit_test ()
{
	ST_SysTime * stp = ST_SysTime_create ();

	ST_NULL_RETURN (ST_SYS_TIME_H_DEBUG, stp, ST_BAD_ALLOC);

	printf ("Performing smart sleep for 0.5 second...\n\n");

	ST_Ps sleep_ps = {.ps = (ST_PsNum) 500 * 1000 * 1000 * 1000};

	ST_SysTime_nbs (stp, sleep_ps, NULL);

	ST_SysTime_destroy (stp);

	return ST_SUCCESS;
}




/// Performs all of the unit tests available in ST_SysTime.c
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_SysTime_all_unit_test ()
{
	ST_PsRecord_unit_test ();

	ST_SysTime_unit_test ();

	ST_SysTime_create_copy_unit_test ();

	ST_SysTime_tbs_opt_unit_test ();
	ST_SysTime_tbs_unit_test ();

	ST_SysTime_nbs_opt_unit_test ();
	ST_SysTime_nbs_unit_test ();

	ST_SysTime_nbs_opt_calc_error_unit_test ();
	ST_SysTime_nbs_calc_error_unit_test ();

	ST_SysTime_nbs_opt_calc_min_sleep_unit_test ();
	ST_SysTime_nbs_calc_min_sleep_unit_test ();

	return ST_SUCCESS;
}

