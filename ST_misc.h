/*!****************************************************************************
 *
 * @file
 * ST_misc.h
 * 
 * Functions, type definitions, and constants that do not fit in well anywhere
 * else in SlyTime
 *
 *****************************************************************************/




#ifndef ST_MISC_H
#define ST_MISC_H




#include <stdio.h>


#include "ST_misc_public.h"




/// Macro for an expression that returns the smallest of two numbers
/*!
 *  @param a	The first element to compare
 *
 *  @param b	The second element to compare
 */

#define ST_MIN(a,b)	\
			\
	(((a) < (b)) ? (a) : (b))




/// Macro for an expression that returns the largest of two numbers
/*!
 *  @param a	The first element to compare
 *
 *  @param b	The second element to compare
 */

#define ST_MAX(a,b)	\
			\
	(((a) > (b)) ? (a) : (b))




/// A type that represents whether or not something is ready

typedef enum ST_READY_FLAG
{
	ST_NOT_READY = 0,

	ST_READY

} ST_READY_FLAG;




/// A type that represents a basic true-or-false value

typedef enum ST_BOOL
{
	ST_FALSE = 0,

	ST_TRUE  = 1

} ST_BOOL;




/// Determines the length the string in the #ST_ErrnoStr struct

#define ST_ERRNO_STR_CHAR_LEN	(64)




/// A type that holds a string representing the current value of #errno (as
/// returned by the strerror_*() functions)

typedef struct ST_ErrnoStr 
{
	/// The actual string that represents value of #errno

	char str [ST_ERRNO_STR_CHAR_LEN];

} ST_ErrnoStr;




//*****************************************************************************
//
//	See the Doxygen Documentation for information on items below
//
//*****************************************************************************




ST_ErrnoStr ST_ErrnoStr_get (int errnum, ST_STD_RES * st_res);




ST_STD_RES ST_STD_RES_print (FILE * fp, ST_STD_RES st_res);




#endif //ST_MISC_H

