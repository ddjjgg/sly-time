/*!****************************************************************************
 *
 * @file
 * test_1.c
 *
 * Calls every single public SlyTime function in one program.
 *
 *****************************************************************************/


#include "SlyTime.h"


int main ()
{
	//Create an ST_SysTime structure to use

	ST_SysTime * stp = ST_SysTime_create ();

	if (NULL == stp)
	{
		printf ("Could not allocate #ST_SysTime structure\n");

		return -1;
	}


	//Print out the #ST_SysTime structure

	ST_STD_RES res = ST_SysTime_print (stdout, stp);

	if (ST_SUCCESS != res)
	{
		printf ("Could not use #ST_SysTime_print ()\n");

		return -1;
	}

	printf ("\n\n");


	//Perform a small sleep
	
	ST_Ps sleep_ps =	{.ps = (ST_PsNum) 100 * 1000 * 1000 * 1000};
	ST_Ps used_ps =		{.ps = 0};


	printf ("Sleeping for %" ST_PRI_ST_PsNum " ms....", sleep_ps.ps / (1000 * 1000 * 1000));
	
	
	res = ST_SysTime_nbs (stp, sleep_ps, &used_ps);

	if (ST_SUCCESS != res)
	{
		printf ("Could not use #ST_SysTime_nbs ()\n");

		return -1;
	}


	printf
	(
		"DONE. (sleep_ps = %" ST_PRI_ST_PsNum " ps, used_ps = %" ST_PRI_ST_PsNum " ps)\n\n",
		sleep_ps.ps,
		used_ps.ps
	);


	//Create a copy of #stp, and then perform a sleep with it, and then
	//destroy the copy

	ST_SysTime * copy_stp = ST_SysTime_create_copy (stp, NULL);

	if (NULL == copy_stp)
	{
		ST_SysTime_destroy (stp);

		printf ("Could not create copy of #stp\n");

		return -1;
	}

	printf ("\n\n----copy_stp----\n\n\n");

	ST_SysTime_print (stdout, copy_stp);


	printf ("\n\nSleeping with #copy_stp...\n");

	ST_SysTime_nbs (copy_stp, sleep_ps, NULL);

	printf ("DONE.\n\n");


	ST_SysTime_destroy (copy_stp);



	//Calculate the error typically associated with the above sleep interval
	
	double error = 0;

	res = ST_SysTime_nbs_calc_error (stp, sleep_ps, 1, &error);

	if (ST_SUCCESS != res)
	{
		printf ("Could not use #ST_SysTime_nbs_calc_error ()\n");

		return -1;
	}


	printf
	(
		"For sleep interval %" ST_PRI_ST_PsNum " ms, the error rate is %lf\n\n",
		sleep_ps.ps / (1000 * 1000 * 1000),
		error
	);


	//Calculate the smallest sleep that you can perform with a higher error
	//rate
	
	ST_Ps error_calc_ps = 	{.ps = 1000 * 1000 * 1000};

	ST_Ps min_bound_ps =	{.ps = 10 * 1000};

	ST_Ps min_nbs_ps =	{.ps = 0};

	error = 		20 * error;


	res =	ST_SysTime_nbs_calc_min_sleep
		(
			stp,

			sleep_ps,
			error_calc_ps,
			min_bound_ps,

			error,

			sleep_ps,
			&min_nbs_ps
		);


	printf
	(
		"The smallest sleep that can be performed at %lf error is %" ST_PRI_ST_PsNum " ps\n\n",
		error,
		min_nbs_ps.ps
	);


	//Test printing out an #ST_STD_RES
	
	printf ("The most recent value of #res is %s\n\n", ST_StdResStr_get (res).str);


	//Test setting the debug level to #ST_L_DEBUG, and then intentionally
	//perform a bad function call
	
	printf ("Testing setting the debug level to #ST_L_DEBUG...\n\n");

	ST_set_all_debug_levels (ST_H_DEBUG);

	res = ST_SysTime_nbs (NULL, min_nbs_ps, NULL);

	printf ("The most recent value of #res is %s\n\n", ST_StdResStr_get (res).str);



	//Set all the debugging levels to #ST_NO_DEBUG one-by-one, and then try
	//to induce another debugging message

	printf ("Testing setting all debug levels to #ST_NO_DEBUG one-by-one\nSTART...");

	ST_set_SysTime_debug_level (ST_NO_DEBUG);
	ST_set_os_time_debug_level (ST_NO_DEBUG);
	ST_set_misc_debug_level (ST_NO_DEBUG);
	
	res = ST_SysTime_nbs (NULL, min_nbs_ps, NULL);

	printf ("STOP\n\n");


	//Free any memory allocated in this function

	ST_SysTime_destroy (stp);


	return 0;
}
