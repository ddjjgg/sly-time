/*!****************************************************************************
 *
 * @file
 * ST_main.c
 *
 * Provides a main() function that will be compiled when the project is
 * configured to include it. This is where various unit tests can be executed
 * in the project.
 *
 *****************************************************************************/




#ifdef COMPILE_MAIN


#include "SlyTime.h"


#include "ST_debug.h"
#include "ST_misc.h"
#include "ST_os_time.h"
#include "ST_SysTime.h"




int main ()
{
	//ST_set_SysTime_debug_level (ST_H_DEBUG);

	//ST_SysTime * stp = ST_SysTime_create ();

	//ST_Ps ps = {.ps = 500 * 1000 * 1000};

	//ST_SysTime_nbs (stp, ps, NULL);

	//ST_SysTime_destroy (stp);



	ST_os_time_all_unit_test ();
	ST_SysTime_all_unit_test ();



	//ST_SysTime_print (stdout, NULL);

	//ST_set_SysTime_debug_level (ST_H_DEBUG);

	//ST_SysTime_print (stdout, NULL);

	//ST_set_all_debug_levels (ST_NO_DEBUG);

	//ST_SysTime_print (stdout, NULL);


	//for (int i = 0; i < 1000; i += 1)
	//{
	//	printf ("\n");
	//	ST_arb_op_print (stdout, i); 
	//	printf ("\n");
	//}


	return 0;
}



#endif //COMPILE_MAIN

