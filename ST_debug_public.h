/*!****************************************************************************
 *
 * @file
 * ST_debug_public.h
 *
 * The publicly accessible declarations associated with "ST_debug.c"
 *
 * Refer to the Doxygen generated documentation for information on the
 * following items.
 *
 *****************************************************************************/




#ifndef ST_DEBUG_PUBLIC_H
#define ST_DEBUG_PUBLIC_H




#include <inttypes.h>

#include "ST_misc_public.h"




/// @defgroup Debugging_Level_Values
///
/// Valid values that a debugging level can be set to.
///
/// EX:
///
///	//Put the program into a low-level debugging state
///
///	ST_set_all_debug_levels (ST_L_DEBUG);
///
/// @{


/// Represents a debug level where no debugging code should be executed

#define ST_NO_DEBUG		(0)


/// Represents a debug level where a low amount of debugging code should
/// execute

#define ST_L_DEBUG		(10000)


/// Represents a debug level where a medium amount of debugging code should
/// execute

#define ST_M_DEBUG		(20000)


/// Represents a debug level where a high amount of debugging code should
/// execute

#define ST_H_DEBUG		(30000)


/// @}




ST_STD_RES ST_set_all_debug_levels (uint32_t debug_level);




ST_STD_RES ST_set_os_time_debug_level (uint32_t debug_level);




ST_STD_RES ST_set_SysTime_debug_level (uint32_t debug_level);




ST_STD_RES ST_set_misc_debug_level (uint32_t debug_level);




#endif //ST_DEBUG_PUBLIC_H

