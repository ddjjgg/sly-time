/*!****************************************************************************
 *
 * @file
 * ST_SysTime.h
 *
 * Provides the #ST_SysTime data structure, which stores information to
 * characterize the current system's timing/sleep parameters.
 *
 *****************************************************************************/




#ifndef ST_SYS_TIME_H
#define ST_SYS_TIME_H




#include <stdio.h>
#include <time.h>


#include "SlyTime.h"
#include "ST_os_time.h"


#include "ST_SysTime_public.h"




/// @defgroup ST_SysTime_create_()_Default_Parameters
///
/// The default parameters that will be used when generating #ST_SysTime's
/// inside of ST_SysTime_create ().
///
/// See #ST_SysTime_create_opt () for details about what these values mean.
///
/// @{


#define ST_SYS_TIME_CREATE_NUM_NBS_JITTERS	(8)
#define ST_SYS_TIME_CREATE_INIT_NBS_JITTER	((ST_PsNum) 100 * 1000 * 1000)
#define ST_SYS_TIME_CREATE_NUM_TBS_JITTERS	(8)
#define ST_SYS_TIME_CREATE_INIT_TBS_JITTER	((ST_PsNum) 50 * 1000)

#define ST_SYS_TIME_CREATE_TIME_RES_ATTEMPTS	(4096)
#define ST_SYS_TIME_CREATE_TIME_RES_DEF		((ST_PsNum) 1000)
#define ST_SYS_TIME_CREATE_TIME_RES_FORCE_DEF	(0)

#define ST_SYS_TIME_CREATE_MIN_NBS_FIRST_PS	((ST_PsNum) 1000 * 1000 * 1000)
#define ST_SYS_TIME_CREATE_MIN_NBS_MAX_ERROR	(0.25)
#define ST_SYS_TIME_CREATE_MIN_NBS_ATTEMPTS	(128)
#define ST_SYS_TIME_CREATE_MIN_NBS_RETRIES	(16)
#define ST_SYS_TIME_CREATE_MIN_NBS_SAMPLES	(8)
#define ST_SYS_TIME_CREATE_MIN_NBS_DEF		((ST_PsNum) 1000 * 1000 * 1000)
#define ST_SYS_TIME_CREATE_MIN_NBS_FORCE_DEF	(0)

#define ST_SYS_TIME_CREATE_MIN_TBS_IBS_FACTOR	(32)
#define ST_SYS_TIME_CREATE_MIN_TBS_ATTEMPTS	(4096)
#define ST_SYS_TIME_CREATE_MIN_TBS_SAMPLES	(256)
#define ST_SYS_TIME_CREATE_MIN_TBS_DEF		((ST_PsNum) 70 * 1000)
#define ST_SYS_TIME_CREATE_MIN_TBS_FORCE_DEF	(0)

#define ST_SYS_TIME_CREATE_ARB_OP_ATTEMPTS	(4096)
#define ST_SYS_TIME_CREATE_ARB_OP_RETRIES	(4096)
#define ST_SYS_TIME_CREATE_ARB_OP_NUM_AVG	(256)
#define ST_SYS_TIME_CREATE_ARB_OP_SMP_PER_AVG	(25 * 1000)
#define ST_SYS_TIME_CREATE_ARB_OP_DEF		((ST_PsNum) 10500)
#define ST_SYS_TIME_CREATE_ARB_OP_FORCE_DEF	(0)


/// @}




/// @defgroup ST_SysTime_nbs_()_Default_Parameters
///
/// The default parameters that will be used when performing sleeps inside of
/// ST_SysTime_nbs ().
///
/// See #ST_SysTime_nbs_opt () for details about what these values mean.
///
/// @{


#define ST_SYS_TIME_NBS_JITTER_RATIO		(1.50)
#define ST_SYS_TIME_NBS_NONBUSY_RETRIES		(4096)
#define ST_SYS_TIME_NBS_BUSY_RETRIES		(4096)


/// @}




struct ST_PsRecord;
typedef struct ST_PsRecord ST_PsRecord;




struct ST_SysTime;
typedef struct ST_SysTime ST_SysTime;




ST_STD_RES ST_PsRecord_set_all
(
 	ST_PsRecord *	tpr,
	u32		num_samples,
	ST_Ps *		samples,
	ST_Ps		init_val
);




ST_STD_RES ST_PsRecord_init_samples (ST_PsRecord * tpr, ST_Ps init_val);




ST_STD_RES ST_PsRecord_refresh_avg (ST_PsRecord * tpr);




ST_STD_RES ST_PsRecord_add_sample (ST_PsRecord * tpr, ST_Ps new_tp_val);




ST_STD_RES ST_PsRecord_print (FILE * f_ptr, ST_PsRecord * tpr);




ST_STD_RES ST_SysTime_update_min_nbs_ps
(
	ST_SysTime *	stp,
	ST_Ps		monotonic_time_res,
	ST_Ps		min_nbs_first_ps,
	f64		max_sleep_error,
	u32		max_sleep_attempts,
	u32		num_sleep_retries,
	u32		num_sleep_samples,
	ST_Ps		def_small_sleep
);




ST_STD_RES ST_SysTime_update_min_tbs_ps
(
	ST_SysTime *		stp,
	u32			max_attempts,
	u32			num_samples,
	ST_Ps			def_min_tbs_ps
);




ST_STD_RES ST_SysTime_update_arb_op_ps
(
	ST_SysTime *	stp,
	u32		max_attempts,
	u32		max_attempts_per_avg,
	u32		num_averages,
	u32		num_samples_per_avg,
	ST_Ps		def_arb_op_ps
);




ST_STD_RES ST_SysTime_update_monotonic_time_res
(
	ST_SysTime *	stp,
	uint32_t	num_attempts,
	ST_Ps		def_time_res
);




ST_STD_RES ST_SysTime_record_nbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps		sleep_jitter_ps
);




ST_STD_RES ST_SysTime_record_tbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps		sleep_jitter_ps
);




ST_STD_RES ST_SysTime_avg_nbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps *		avg_jitter_ps
);




ST_STD_RES ST_SysTime_avg_tbs_jitter
(
	ST_SysTime *	stp,
	ST_Ps *		avg_jitter_ps
);





ST_STD_RES ST_SysTime_ibs_opt
(
	ST_SysTime *		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
);




ST_STD_RES ST_SysTime_tbs_opt
(
	ST_SysTime * 		stp,
	ST_Ps			sleep_ps,
	u32			num_retries,
	ST_Ps *			used_ps
);




ST_STD_RES ST_SysTime_tbs
(
	ST_SysTime *		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
);




ST_STD_RES ST_SysTime_nbs_opt
(
	ST_SysTime *		stp,
	ST_Ps			sleep_ps,
	f64			jitter_ratio,
	u32			nonbusy_retries,
	u32			busy_retries,
	ST_Ps *			used_ps
);




ST_STD_RES ST_SysTime_nbs
(
	ST_SysTime * 		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
);




ST_STD_RES ST_SysTime_nbs_opt_calc_error
(
	ST_SysTime *		stp,

	ST_Ps			single_nbs_ps,
	f64			jitter_ratio,
	u32			nonbusy_retries,
	u32			busy_retries,

	u64			num_sleeps,
	f64 *			error
);




ST_STD_RES ST_SysTime_nbs_opt_calc_min_sleep
(
	ST_SysTime *		stp,

	f64			jitter_ratio,
	u32			nonbusy_retries,
	u32			busy_retries,

	ST_Ps			first_sleep_ps,
	ST_Ps			error_calc_min_ps,
	ST_Ps			min_bound_ps,

	f64			max_error,

	ST_Ps			def_min_nbs_ps,
	ST_Ps *			min_nbs_ps
);




ST_SysTime * ST_SysTime_create_opt
(
	u32		num_nbs_jitters,
	ST_Ps		init_nbs_jitter,
	u32		num_tbs_jitters,
	ST_Ps		init_tbs_jitter,

	u32		time_res_attempts,
	ST_Ps 		time_res_def,
	u32		time_res_force_def,
	ST_STD_RES *	time_res_success,

	ST_Ps		min_nbs_first_ps,
	f64		min_nbs_max_error,
	u32		min_nbs_attempts,
	u32		min_nbs_retries,
	u32		min_nbs_samples,
	ST_Ps 		min_nbs_def,
	u32		min_nbs_force_def,
	ST_STD_RES *	min_nbs_success,

	f64		min_tbs_ibs_factor,
	u32		min_tbs_attempts,
	u32		min_tbs_samples,
	ST_Ps		min_tbs_def,
	u32		min_tbs_force_def,
	ST_STD_RES *	min_tbs_success,

	u32		arb_op_attempts,
	u32		arb_op_retries,
	u32		arb_op_num_avg,
	u32		arb_op_smp_per_avg,
	ST_Ps		arb_op_def,
	u32		arb_op_force_def,
	ST_STD_RES *	arb_op_success
);




ST_STD_RES ST_PsRecord_unit_test ();




ST_STD_RES ST_SysTime_unit_test ();




ST_STD_RES ST_SysTime_create_copy_unit_test ();




ST_STD_RES ST_SysTime_tbs_opt_unit_test ();




ST_STD_RES ST_SysTime_nbs_opt_unit_test ();




ST_STD_RES ST_SysTime_tbs_unit_test ();




ST_STD_RES ST_SysTime_nbs_unit_test ();




ST_STD_RES ST_SysTime_all_unit_test ();




#endif //ST_SYS_TIME_H

