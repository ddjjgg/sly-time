/*!****************************************************************************
 *
 * @file
 * ST_debug.c
 * 
 * Provides per-feature debug levels that can be checked throughout the rest of
 * the program.
 *
 * The general idea is that when a particular feature's debugging level is set
 * to #ST_NO_DEBUG, that feature should no longer have any debugging-related
 * code execute.
 *
 * Otherwise, the amount of debugging-related code to execute can be changed by
 * setting the debugging level to #ST_L_DEBUG, #ST_M_DEBUG, or #ST_H_DEBUG.
 *
 *****************************************************************************/




#include "ST_debug.h"




///Stores the debug level for functions that interact with the operating
///system's provided timing functions

u32 ST_os_time_debug_level =	ST_NO_DEBUG;


///Stores the debug level for the #SysTime data structure

u32 ST_SysTime_debug_level =	ST_NO_DEBUG;


///Stores the debug level for miscellaneous features in SlyTime

u32 ST_misc_debug_level = 	ST_NO_DEBUG;




/// Sets the debug level for all possible feature debug levels simultaneously
/*!
 *  @param debug_level			An integer representing the debug
 *  					level, which will usually be one of the
 *  					macro's ST_NO_DEBUG, ST_L_DEBUG,
 *  					ST_M_DEBUG, or ST_H_DEBUG
 *
 *  @return				Standard status code
 */

ST_STD_RES ST_set_all_debug_levels (u32 debug_level)
{
	ST_os_time_debug_level =	debug_level;
	ST_SysTime_debug_level =	debug_level;
	ST_misc_debug_level =		debug_level;

	return ST_SUCCESS;
}




/// Sets the debug level for the "os time" feature
/*!
 *  @param debug_level			An integer representing the debug
 *  					level, which will usually be one of the
 *  					macro's ST_NO_DEBUG, ST_L_DEBUG,
 *  					ST_M_DEBUG, or ST_H_DEBUG
 *
 *  @return				Standard status code
 */

ST_STD_RES ST_set_os_time_debug_level (u32 debug_level)
{
	ST_os_time_debug_level = debug_level;

	return ST_SUCCESS;
}




///Sets the debug level for the "SysTime" feature
/*!
 *  @param debug_level			An integer representing the debug
 *  					level, which will usually be one of the
 *  					macro's ST_NO_DEBUG, ST_L_DEBUG,
 *  					ST_M_DEBUG, or ST_H_DEBUG
 *
 *  @return				Standard status code
 */

ST_STD_RES ST_set_SysTime_debug_level (u32 debug_level)
{
	ST_SysTime_debug_level = debug_level;

	return ST_SUCCESS;
}




///Sets the debug level for the miscellaneous features of SlyTime
/*!
 *  @param debug_level			An integer representing the debug
 *  					level, which will usually be one of the
 *  					macro's ST_NO_DEBUG, ST_L_DEBUG,
 *  					ST_M_DEBUG, or ST_H_DEBUG
 *
 *  @return				Standard status code
 */

ST_STD_RES ST_set_misc_debug_level (u32 debug_level)
{
	ST_misc_debug_level = debug_level;

	return ST_SUCCESS;
}

