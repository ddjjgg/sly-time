/*!****************************************************************************
 *
 * @file
 * ST_os_time.c
 *
 * Functions that interact with the given operating system's timing / sleep
 * functions. This is where OS-dependent aspects of SlyTime should be
 * handled.
 *
 *
 * <b> KNOWN DESIGN QUIRKS </b>
 *
 * 1)
 * 	Currently, this source file is written only in reference to Linux.
 * 	Lots of #ifdef'ing here remains to be done to properly bring this to
 * 	other OS's.
 *
 *****************************************************************************/




#include <time.h>
#include <errno.h>


#include "ST_debug.h"
#include "ST_misc.h"
#include "ST_numeric_types.h"


#include "ST_os_time.h"




/// Prints out a #ST_Ps value
/*!
 *  @param f_ptr		The FILE * to print to
 *
 *  @param time_val		The #ST_Ps value to print out
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_Ps_print (FILE * f_ptr, ST_Ps t_ps)
{
	ST_NULL_RETURN (ST_OS_TIME_H_DEBUG, f_ptr, ST_BAD_ARG);

	fprintf (f_ptr, "%" ST_PRI_ST_PsNum " pico-seconds", t_ps.ps);

	return ST_SUCCESS;
}




/// Prints out a #timespec data structure
/*!
 *  @param f_ptr		The FILE * to print to
 *
 *  @param time_val		The #timespec to print out
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_timespec_print (FILE * f_ptr, struct timespec ts)
{
	ST_NULL_RETURN (ST_OS_TIME_H_DEBUG, f_ptr, ST_BAD_ARG);

	fprintf
	(
	 	f_ptr,
		"%" PRIi64 " seconds\t%ld nano-seconds",
		(i64) ts.tv_sec,
		ts.tv_nsec
	);

	return ST_SUCCESS;
}




/// Converts a given #timespec into an #ST_Ps representing a number of
/// picoseconds.
/*!
 *  @warning
 *  Since a #timespec can represent very large time intervals, it is easy to
 *  overflow the resulting #ST_Ps number. Ergo, you should almost always check
 *  the return value of this function for safety.
 *
 *  @param ts			The input timespec representing some time
 *				interval
 *
 *  @param ps			Where to place the resulting number of
 *				picoseconds
 *
 *  @return			Standard status code. If ps is overflowed, then
 *				this will _not_ be #ST_SUCCESS
 */

ST_STD_RES ST_timespec_to_ST_Ps (struct timespec ts, ST_Ps * t_ps)
{
	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, t_ps, ST_BAD_ARG);


	// Make sure the *tv_nsec field does not represent more than one
	// second. This is necessary for error checking at the end of the
	// function.

	struct timespec fixed_ts = ST_timespec_fix (ts);


	//Calculate the number of picoseconds equivalent to the given timespec

	t_ps->ps =	((ST_PsNum) fixed_ts.tv_sec * 1000 * 1000 * 1000 * 1000) +
			((ST_PsNum) fixed_ts.tv_nsec * 1000);


	// Check if the provided timespec has caused the ST_Ps to overlow
	//
	// (Note how there's two ways to return ST_SUCCESS here

	if (ST_Ps_MAX_SECS > fixed_ts.tv_sec)
	{
		return ST_SUCCESS;
	}

	else if
	(
	 	(ST_Ps_MAX_SECS == fixed_ts.tv_sec)

		&&

		(ST_Ps_MAX_NSECS_WHEN_MAX_SECS >= fixed_ts.tv_nsec)
	)
	{
		return ST_SUCCESS;
	}

	else
	{
		return ST_UNKNOWN_ERROR;
	}
}




/// Converts a #ST_Ps value into a #timespec
/*!
 *  @param t_ps			The #ST_Ps value to convert into a #timespec
 *
 *  @param ts			Where to place a #timespec that is _nearly_
 *  				equivalent to #t_ps. (Values less than 1000 in
 *  				#t_ps will be truncated to 0)
 *
 *  @return			Standard status code. If #t_ps could not be
 *				converted, then this return value will NOT be
 *				#ST_SUCCESS. The most likely reason this
 *				function fails is when #t_ps is negative on a
 *				platform where support for signed #timespec
 *				values was not detected. (Some systems
 *				techincally have signed #timespec values
 *				as a result of the #time_t type being signed).
 *
 *				Even when this function reports failure, it
 *				will still modify the value at #ts.
 */

ST_STD_RES ST_Ps_to_timespec (ST_Ps t_ps, struct timespec * ts)
{	
	//FIXME: Some system's actually signed #timespec's depending on how
	//#time_t is implemented. So, a completely proper conversion of an
	//#ST_Ps structure is system-dependent. Right now, this function
	//reports an error whenever #t_ps is negative.


	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, ts, ST_BAD_ARG);


	//Calculate the closest #timespec to #t_ps

	ts->tv_sec  = t_ps.ps / ((ST_PsNum) 1000 * 1000 * 1000 * 1000);
	ts->tv_nsec = ((t_ps.ps % ((ST_PsNum) 1000 * 1000 * 1000 * 1000)) / 1000);

	
	//Report an error in the case that #t_ps is negative

	if (t_ps.ps < 0)
	{
		return ST_UNKNOWN_ERROR;
	}


	return ST_SUCCESS;
}




/// Checks if a #timespec structure is malformed (i.e. *.tv_nsec is greater than
/// 1,000,000,000) and returns an equivalent #timespec except formatted
/// correctly.
///
/// This will also truncate negative #timespec values to 0. If the #tv_sec field
/// is negative, both fields get set to 0. If the #tv_nsec field is negative,
/// only that field gets set to 0.
/*!
 *  @param ts			The timespec to fix (if necessary)
 *
 *  @return			A timespec equivalent to ts (or as close as
 *				possible), except formatted correctly
 */

struct timespec ST_timespec_fix (struct timespec ts)
{
	struct timespec fixed_ts = ts;


	//Check if any of the fields of #ts are negative, and reset them to 0
	//in that case

	//FIXME: Is it true that no member of a timespec field should ever have
	//a negative value?

	if (fixed_ts.tv_sec < 0)
	{
		fixed_ts.tv_sec  = 0;
		fixed_ts.tv_nsec = 0;
	}

	if (fixed_ts.tv_nsec < 0)
	{
		fixed_ts.tv_nsec = 0;
	}


	//Make sure that the *.tv_nsec field is not representing more than 1
	//second

	//FIXME: Should wrap-around on *tv_sec be checked on this operation?

	if (fixed_ts.tv_nsec > ST_TIMESPEC_MAX_TV_NSEC)
	{
		fixed_ts.tv_sec = ts.tv_sec + (ts.tv_nsec / ST_NS_PER_SEC);

		fixed_ts.tv_nsec = ts.tv_nsec % ST_NS_PER_SEC;
	}


	return fixed_ts;
}




/// Returns 1 if the 1st timespec argument represents a larger time value
/// than the 2nd argument.
/*!
 *  @param ts_a			The first #timespec to compare
 *
 *  @param ts_b			The second #timespec to compare
 *
 *  @return			1 if #ts_a was larger time value than #ts_b
 *
 *  				0 if #ts_a and #ts_b were equal
 *
 *  				-1 if #ts_a was a smaller time value than #ts_b
 */

int ST_timespec_greater_than (struct timespec ts_a, struct timespec ts_b)
{
	//FIXME: Is this actually necessary to do in this function?

	// Fix the #timespec's just in case they are badly formated

	struct timespec fixed_ts_a = ST_timespec_fix (ts_a);
	struct timespec fixed_ts_b = ST_timespec_fix (ts_b);


	// Determine which #timespec represents a larger time value

	if (fixed_ts_a.tv_sec > fixed_ts_b.tv_sec)
	{
		return 1;
	}

	else if (fixed_ts_a.tv_sec < fixed_ts_b.tv_sec)
	{
		return -1;
	}

	else if (fixed_ts_a.tv_nsec > fixed_ts_b.tv_nsec)
	{
		return 1;
	}

	else if (fixed_ts_a.tv_nsec < fixed_ts_b.tv_nsec)
	{
		return -1;
	}

	else
	{
		return 0;
	}
}




/// Performs a comparison of 2 #ST_Ps values in a manner that can be used by
/// qsort ()
/*!
 *  @param ps_a_ptr		Pointer to the 1st #ST_Ps to compare
 *
 *  @param ps_b_ptr		Pointer to the 2nd #ST_Ps to compare
 *
 *  @return			-1 if the 1st #ST_Ps is lesser than the 2nd
 *
 *  				0 if the #ST_Ps values are equal
 *
 *  				1 if the 1st #ST_Ps is greater than the 2nd
 */

int ST_Ps_compare_qsort (const void * ps_a_ptr, const void * ps_b_ptr)
{
	//Sanity check on arguments

	ST_NULL_RETURN (ST_OS_TIME_L_DEBUG, ps_a_ptr, -1);
	ST_NULL_RETURN (ST_OS_TIME_L_DEBUG, ps_b_ptr, -1);


	ST_Ps ps_a = * ((ST_Ps *) ps_a_ptr);
	ST_Ps ps_b = * ((ST_Ps *) ps_b_ptr);


	//Perform the comparison needed for qsort ()

	if (ps_a.ps < ps_b.ps)
	{
		return -1;
	}

	else if (ps_a.ps > ps_b.ps)
	{
		return 1;
	}

	else
	{
		return 0;
	}

}




/// Returns the _absolute_ time difference between two #timespec's as a #timespec
/*!
 *  @param ts_a			The timespec representing the start or end time
 *
 *  @param ts_b			The timespec representing the start or end time
 *
 *  @return			A timespec representing the time difference
 *
 *  @warning
 *  This function avoids ever returning a negative time interval, so if #ts_b is
 *  eariler than #ts_a, the timespecs will be reordered internally in this
 *  function. The returned time interval is always in regards from earlier to
 *  later.
 */

struct timespec ST_timespec_abs_diff (struct timespec ts_a, struct timespec ts_b)
{
	//Determine which timespec is earlier, and which is later

	struct timespec early;
	struct timespec later;

	if (ts_a.tv_sec < ts_b.tv_sec)
	{
		early = ts_a;
		later = ts_b;
	}

	else if (ts_b.tv_sec < ts_a.tv_sec)
	{
		early = ts_b;
		later = ts_a;
	}

	//If execution reaches here, then the *.tv_sec fields were the same

	else if (ts_a.tv_nsec < ts_b.tv_nsec)
	{
		early = ts_a;
		later = ts_b;
	}

	else
	{
		early = ts_b;
		later = ts_a;
	}


	//Make sure that #early and #later are properly formatted timespec's

	early = ST_timespec_fix (early);
	later = ST_timespec_fix (later);


	//Calulate the time in between #early and #later. Take note that
	//because *.tv_sec represents the part to the left of the decimal
	//place, and *.tv_nsec represents the part to the right of the decimal
	//place, it is necessary to "carry the one" in some cases.

	struct timespec diff;

	diff.tv_sec	= 0;
	diff.tv_nsec	= 0;

	if (early.tv_nsec > later.tv_nsec)
	{
		//Carrying the one IS needed

		diff.tv_sec = (later.tv_sec - early.tv_sec) - 1;

		diff.tv_nsec = (ST_NS_PER_SEC + later.tv_nsec) - early.tv_nsec;
	}

	else
	{
		//Carrying the one is NOT needed

		diff.tv_sec = later.tv_sec - early.tv_sec;

		diff.tv_nsec = later.tv_nsec - early.tv_nsec;
	}


	return diff;
}




/// Returns the _absolute_ time difference between two #ST_Ps's as a #ST_Ps
/*!
 *  @param t_ps_a		The ST_Ps representing the start or end time
 *
 *  @param t_ps_b		The ST_Ps representing the start or end time
 *
 *  @return			A ST_Ps representing the time difference
 *
 *  @warning
 *  This function avoids ever returning a negative time interval, so if #t_ps_b
 *  is eariler than #t_ps_a, the ST_Ps's will be reordered internally in this
 *  function. The returned time interval is always in regards from earlier to
 *  later.
 */

ST_Ps ST_Ps_abs_diff (ST_Ps t_ps_a, ST_Ps t_ps_b)
{
	// Determine which ST_Ps is early, which is later

	ST_Ps early;
	ST_Ps later;

	if (t_ps_a.ps < t_ps_b.ps)
	{
		early = t_ps_a;
		later = t_ps_b;
	}

	else
	{
		early = t_ps_b;
		later = t_ps_a;
	}

	ST_Ps diff;

	diff.ps = later.ps - early.ps;

	return diff;
}




/// Returns the time difference between two #ST_Ps's as a #ST_Ps
/*!
 *  @param t_ps_a		The #ST_Ps value on the left (i.e. the
 *  				minuend)
 *
 *  @param t_ps_b		The #ST_Ps value on the right (i.e. the
 *  				subtrahend)
 *
 *  @return			A ST_Ps representing the time difference,
 *  				which is (#t_ps_a - #t_ps_b)
 *
 *  @warning
 *  This function CAN return negative #ST_Ps values if #t_ps_a has a smaller
 *  time value than #t_ps_b, unlike ST_Ps_abs_diff()
 */

ST_Ps ST_Ps_diff (ST_Ps t_ps_a, ST_Ps t_ps_b)
{
	ST_Ps res = {.ps = (ST_PsNum) 0};

	res.ps = ((ST_PsNum) t_ps_a.ps - (ST_PsNum) t_ps_b.ps);

	return res;
}




/// Gets the resolution of time measurements performed with ST_get_monotonic_time().
/*!
 *  @param time_res_ptr		Where to place the smallest amount of time that
 *				can be measured by ST_get_monotonic_time()
 *
 *  @return			ST_SUCCESS if the resolution was measured properly,
 *				ST_BAD_LIB_CALL if some system calls failed,
 *				and ST_BAD_ARG if time_res_ptr is malformed
 *				(namely, if it is a NULL value)
 */

ST_STD_RES ST_get_monotonic_res_ts (struct timespec * time_res_ptr)
{
	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, time_res_ptr, ST_BAD_ARG);

	int res = clock_getres (CLOCK_MONOTONIC, time_res_ptr);

	if (0 != res)
	{
		int errnum = errno;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Could not place monotonic timer "
			"resolution into %p. "
			"errno : %s",
			time_res_ptr,
			ST_ErrnoStr_get (errnum, NULL).str
		);

		return ST_BAD_LIB_CALL;
	}

	return ST_SUCCESS;
}




/// Gets the resolution of time measurements performed with ST_get_monotonic_time(),
/// and does so in an "eager" sense, meaning multiple attempts are used if
/// necessary.
/*!
 *  @param time_res_ptr		Where to place the smallest amount of time that
 *				can be measured by ST_get_monotonic_time()
 *
 *  @param max_attempts		The maximum number of attempts to use in trying
 *  				to get the monotonic resolution. If this
 *  				argument is 0, it will be treated as if it were
 *  				1.
 *
 *  @return			ST_SUCCESS if the resolution was measured properly,
 *				ST_BAD_LIB_CALL if some system calls failed,
 *				and ST_BAD_ARG if time_res_ptr is malformed
 *				(namely, if it is a NULL value)
 */

ST_STD_RES ST_get_monotonic_res_ts_eager
(
	struct timespec *	time_res_ptr,
	u32			max_attempts
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, time_res_ptr, ST_BAD_ARG);


	int internal_max_attempts = max_attempts;

	if (internal_max_attempts <= 0)
	{
		internal_max_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// Enter attempt loop to determine monotonic timer resolution

	u32 attempt_num	= 0;

	ST_STD_RES res	= ST_SUCCESS;

	for (attempt_num = 0; attempt_num < internal_max_attempts; attempt_num += 1)
	{
		res = ST_get_monotonic_res_ts (time_res_ptr);

		if (ST_SUCCESS == res)
		{
			break;
		}
	}


	// Check if attempt loop succeeded

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Failed to place monotonic time resolution in %p",
			time_res_ptr
		);

		return res;
	}


	return ST_SUCCESS;
}




/// Same as ST_get_monotonic_res_ts (), except that the output value is formatted
/// as a #ST_Ps
/*!
 *  @param time_res_ptr		Where to place the smallest amount of time that
 *  				can be measured by ST_get_monotonic_time()
 *
 *  @return			Standard status code. ST_SUCCESS if the
 *  				resolution was measured properly, ST_BAD_LIB_CALL
 *  				if some system calls failed, and ST_BAD_ARG if
 *  				time_res_ptr is malformed (namely, if it is a
 *  				NULL value)
 */

ST_STD_RES ST_get_monotonic_res_ps (ST_Ps * time_res_ptr)
{
	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, time_res_ptr, ST_BAD_ARG);


	// Get the monotonic timer resolution

	struct timespec time_res_ts;

	ST_STD_RES res = ST_get_monotonic_res_ts (&time_res_ts);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Could not get system timer resolution."
		);

		return res;
	}


	// Convert the time resolution to a ST_Ps structure

	ST_Ps time_res_ps;

	res = ST_timespec_to_ST_Ps (time_res_ts, &time_res_ps);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Could not convert system timer resolution to "
			"a #ST_Ps value."
		);

		return res;
	}


	//Save the result

	*time_res_ptr = time_res_ps;


	return ST_SUCCESS;
}




/// Same as ST_get_monotonic_res_ts (), except that the output value is formatted
/// as a #ST_Ps
/*!
 *  @param time_res_ptr		Where to place the smallest amount of time that
 *  				can be measured by ST_get_monotonic_time()
 *
 *  @param max_attempts		The maximum number of attempts to use in trying
 *  				to get the monotonic resolution. If this
 *  				argument is 0, it will be treated as if it were
 *  				1.
 *
 *  @return			Standard status code. ST_SUCCESS if the
 *  				resolution was measured properly, ST_BAD_LIB_CALL
 *  				if some system calls failed, and ST_BAD_ARG if
 *  				time_res_ptr is malformed (namely, if it is a
 *  				NULL value)
 */

ST_STD_RES ST_get_monotonic_res_ps_eager
(
	ST_Ps *		time_res_ptr,
	u32		max_attempts
)
{
	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, time_res_ptr, ST_BAD_ARG);


	int internal_max_attempts = max_attempts;

	if (internal_max_attempts <= 0)
	{
		internal_max_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// Get the monotonic timer resolution

	struct timespec time_res_ts;

	ST_STD_RES res = ST_get_monotonic_res_ts_eager (&time_res_ts, internal_max_attempts);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Could not get system timer resolution."
		);

		return res;
	}


	// Conver the time resolution to a ST_Ps structure

	ST_Ps time_res_ps;

	res = ST_timespec_to_ST_Ps (time_res_ts, &time_res_ps);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Could not convert system timer resolution to "
			"a #ST_Ps value."
		);

		return res;
	}


	//Save the result

	*time_res_ptr = time_res_ps;


	return ST_SUCCESS;
}




/// Gets the current MONOTONIC time.
/*!
 *  @warning
 *  This gets the system MONOTONIC time, not the wall-clock time. What this
 *  means is that sequential calls to this function are guaranteed to produce
 *  increasing time values (barring timer overflow). However, that doesn't mean
 *  the produced time value corresponds to any particular calendar date.
 *
 *  Note that the resolution of the current time measurement can be determined
 *  by ST_get_monotonic_res_ts()
 *
 *  @param cur_time_ptr		Where to place the current monotonic time value
 *
 *  @return			ST_SUCCESS if the time was measured properly,
 *				ST_BAD_LIB_CALL if some system calls failed,
 *				and ST_BAD_ARG if cur_time_ptr is malformed
 *				(namely, if it is a NULL value)
 */

ST_STD_RES ST_get_monotonic_time (struct timespec * cur_time_ptr)
{
	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, cur_time_ptr, ST_BAD_ARG);

	int res = clock_gettime (CLOCK_MONOTONIC, cur_time_ptr);

	if (0 != res)
	{
		int errnum = errno;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Could not place monotonic time into %p. "
			"errno : %s",
			cur_time_ptr,
			ST_ErrnoStr_get (errnum, NULL).str
		);

		return ST_BAD_LIB_CALL;
	}

	return ST_SUCCESS;
}




/// Gets the current MONOTONIC time, but in an "eager" sense, meaning that
/// multiple attempts are used if necessary
/*!
 *  This function is necessary because technically the system function
 *  clock_gettime() can probabilistically fail.
 *
 *  @warning
 *  See warning in ST_get_monotonic_time()
 *
 *  @param cur_time_ptr		Where to place the current monotonic time value
 *
 *  @param max_attempts		The maximum number of attempts to use in trying
 *  				to get the current time. If this argument is 0,
 *  				it will be treated as if it were 1.
 *
 *  @param			Standard status code. This will be ST_SUCCESS
 *				unless all attempts to obtain the monotonic
 *				time have failed.
 */

ST_STD_RES ST_get_monotonic_time_eager
(
	struct timespec *	cur_time_ptr,
	u32			max_attempts
)
{
	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, cur_time_ptr, ST_BAD_ARG);


	u32 internal_max_attempts = max_attempts;

	if (internal_max_attempts <= 0)
	{
		internal_max_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// Enter attempt loop to get monotonic time

	u32 attempt_num	=  0;

	ST_STD_RES res = ST_SUCCESS;

	for (attempt_num = 0; attempt_num < internal_max_attempts; attempt_num += 1)
	{
		res = ST_get_monotonic_time (cur_time_ptr);

		if (ST_SUCCESS == res)
		{
			break;
		}
	}


	// Check if attempt loop succeeded

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"Failed to place monotonic time in %p",
			cur_time_ptr
		);

		return res;
	}


	return ST_SUCCESS;
}




/// Performs a sleep, but if the sleep is interrupted, this function retries
/// the sleep with the remaining amount of time (repeatedly, if necessary).
/// Hence, this sleep is "eager".
/*!
 *  Note that this function can still fail if the internal system sleep
 *  functions are interrupted more than #num_attempts number of times
 *
 *  @param sleep_ts		The amount of time to sleep
 *
 *  @param num_attempts		The number of times to attempt to sleep. If
 *				this function should fail if the first sleep
 *				attempt fails, leave this argument as 1.
 *
 *				If this argument is 0, it will be treated as if
 *				it were 1.
 *
 *  @return			Standard status code. This should be checked
 *				for ST_SUCCESS, because this can fail if the
 *				inernal system sleep function are interrupted
 *				for a fatal reason.
 */

ST_STD_RES ST_sleep_eager (struct timespec sleep_ts, u32 num_attempts)
{

	//Ensure that the number of attempts is 1 is greater

	u32 internal_num_attempts = num_attempts;

	if (internal_num_attempts <= 0)
	{
		internal_num_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// Enter a loop where attempts will be made to sleep

	struct timespec req_sleep_ts = sleep_ts;

	int res = 1;

	for (u32 attempt_num = 0; attempt_num < internal_num_attempts; attempt_num += 1)
	{
		// Note how the remaining time is placed in the original
		// #req_sleep_ts structure

		// FIXME: Should clock_nanosleep be used here instead with
		// CLOCK_MONOTONIC?

		res = nanosleep (&req_sleep_ts, &req_sleep_ts);


		if (0 == res)
		{
			// Sleep was successful, so leave the attempt loop

			break;
		}

		else
		{
			// If reached here, then nanosleep () failed.
			//
			// So print debug message, and let loop make another
			// attempt

			int errnum = errno;

			ST_DEBUG_PRINT
			(
				ST_OS_TIME_H_DEBUG,
				"Use of nanosleep () failed. \n"
				"rqtp = %" PRIi64 " sec and %" PRIi64 " ns. \n"
				"errno : %s \n"
				"Retrying sleep...",
				(i64) req_sleep_ts.tv_sec,
				(i64) req_sleep_ts.tv_nsec,
				ST_ErrnoStr_get (errnum, NULL).str
			);
		}
	}


	// Check if #res indicates a failure (which means that all attempts were
	// consumed and the sleep was not successful)

	if (0 != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_M_DEBUG,
			"Used all attempts to perform system sleep and "
			"still failed!"
		);


		return ST_UNKNOWN_ERROR;
	}


	return ST_SUCCESS;
}




/// Performs a basic system sleep where the starting and stopping times of the
/// sleep are saved. This sleep is "eager" in the same way that ST_sleep_eager()
/// is eager.
/*!
 *  @param sleep_ts		A #timespec representing the amount of time
 *  				to sleep.
 *
 *  @param num_attempts		The number of times to retry each system
 *				sleep/timing function in the event it fails.
 *				This argument is necessary because system timer
 *				functions can probabilistically fail.
 *
 *				The higher this value is, the more resistant
 *				this function is to failure, but also the
 *				longer it will take to fail. The recommended
 *				value for this argument is 128.
 *
 *				If this argument is 0, it will be treated as if
 *				it were 1.
 *
 *  @param start_ts		Where to place the start time of the sleep.
 *  				This is a MONOTONIC time value.
 *  				(See clock_gettime () on Linux).
 *  				This can be NULL if this value is not desired.
 *
 *  @param stop_ts		Where to place the end time of the sleep.
 *  				This is a MONOTONIC time value.
 *  				This can be NULL if this value is not desired.
 *
 *  @return			Standard status code. If this is not ST_SUCCESS,
 *  				then all attempts have been consumed, and
 *  				function has failed.
 */

ST_STD_RES ST_sleep_marked_monotonic
(
	struct timespec		sleep_ts,
	u32			num_attempts,
	struct timespec *	start_ts,
	struct timespec *	stop_ts
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, start_ts, ST_BAD_ARG);
	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, stop_ts, ST_BAD_ARG);


	u32 internal_num_attempts = num_attempts;

	if (internal_num_attempts <= 0)
	{
		internal_num_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// Measure the time before attempting the program sleep
	// (AKA "start the stop-watch")

	ST_STD_RES std_res = ST_SUCCESS;

	if (NULL != start_ts)
	{
		std_res = ST_get_monotonic_time_eager (start_ts, internal_num_attempts);

		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_H_DEBUG,
				"Failed to START the stop-watch!"
			);

			return ST_UNKNOWN_ERROR;
		}
	}


	// Attempt the program sleep

	ST_sleep_eager (sleep_ts, internal_num_attempts);


	// Measure the time after attempting the program sleep

	if (NULL != stop_ts)
	{
		std_res = ST_get_monotonic_time_eager (stop_ts, internal_num_attempts);

		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_H_DEBUG,
				"Failed to STOP the stop-watch!"
			);

			return ST_UNKNOWN_ERROR;
		}
	}


	return ST_SUCCESS;
}




/// Similar to ST_sleep_marked_monotonic(), except the sleep is implemented as a
/// busy sleep, and so therefore the accuracy of the sleep is extremely high.
/// This form of a sleep does NOT reduce a program's CPU usage, however.
/*!
 *  @param sleep_ts		Same usage as in ST_sleep_marked_monotonic()
 *
 *  @param num_attempts		Same usage as in ST_sleep_marked_monotonic()
 *
 *  @param start_ts		Same usage as in ST_sleep_marked_monotonic()
 *
 *  @param stop_ts		Same usage as in ST_sleep_marked_monotonic()
 *
 *  @return			Standard status code. If this is not ST_SUCCESS,
 *  				then all attempts have been consumed, and
 *  				the function has failed.
 */

// FIXME: Make the function busy_sleep (), and THEN make
// ST_busy_ST_sleep_marked_monotonic () use it.

ST_STD_RES ST_busy_sleep_marked_monotonic
(
	struct timespec		sleep_ts,
	u32			num_attempts,
	struct timespec *	start_ts,
	struct timespec *	stop_ts
)
{
	//Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, start_ts, ST_BAD_ARG);
	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, stop_ts, ST_BAD_ARG);


	u32 internal_num_attempts = num_attempts;

	if (internal_num_attempts <= 0)
	{
		internal_num_attempts = 1;

		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_attempts argument was floored to 1."
		);
	}


	// These variables are used to record the start and stop times of this
	// function

	ST_STD_RES std_res = ST_SUCCESS;

	struct timespec internal_start_ts;
	struct timespec internal_stop_ts;


	// Obtain the start time

	std_res = ST_get_monotonic_time_eager (&internal_start_ts, internal_num_attempts);

	if (ST_SUCCESS != std_res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"Failed to START the stop-watch!"
		);

		return ST_UNKNOWN_ERROR;
	}


	// Save the start time if necessary

	if (NULL != start_ts)
	{
		*start_ts = internal_start_ts;
	}


	// Constantly check the time until desired amount of sleep has elapsed

	while (1)
	{
		// Obtain the current time

		std_res = ST_get_monotonic_time_eager (&internal_stop_ts, internal_num_attempts);

		if (ST_SUCCESS != std_res)
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_H_DEBUG,
				"Failed to STOP the stop-watch!"
			);

			return ST_UNKNOWN_ERROR;
		}


		// Check the amount of time that has elapsed so far, and if
		// so, leave the busy-wait loop

		struct timespec used_ts =

		ST_timespec_abs_diff (internal_stop_ts, internal_start_ts);


		if (1 == ST_timespec_greater_than (used_ts, sleep_ts))
		{
			break;
		}

	}


	// Save the stop time if necessary

	if (NULL != stop_ts)
	{
		*stop_ts = internal_stop_ts;
	}


	return ST_SUCCESS;
}




/// Attempts to find the smallest non-busy sleep interval possible on the
/// running system through experimental means. In other words, this function
/// performs many system sleeps in order to determine the smallest useful sleep
/// interval.
///
/// Internally, this function uses a binary search. As a result, the maximum
/// sleep time is about equal to (#first_sleep * (number of bits used to store
/// #ST_Ps.ps value)) in the worst-case. If #first_sleep is well-chosen, the
/// usual run-time for this function is around 1 to 2 times as long as
/// #first_sleep.
///
/// @warning
/// It is almost always true that on a given system, system sleep accuracy is
/// dependent on the current system load. As a result, the TRUE minimal useful
/// sleep time is a moving target, because system load is not a constant. So,
/// to get a realistic value for the minimal system sleep time, this function
/// should be used several times, and the results should be averaged.
///
/// @warning
/// If #first_sleep is small enough to put the 1st attempted sleep outside the
/// error range specified by #max_sleep_error, then this function will fail and
/// return ST_BAD_ARG. In this case, #min_sleep will not be modified.
/*!
 *  @param min_sleep		Where the result for the smallest nonbusy sleep
 *				interval will be placed.
 *
 *  @param first_sleep		The amount of time to sleep in the first
 *  				test sleep interval. If this sleep interval has
 *  				an error associated with it higher than
 *  				#max_sleep_error, than the calculation of
 *  				#min_sleep will fail. A recommended value for
 *  				this argument would be (5 * 1000 * 1000 *
 *  				1000), which is 5 ms.
 *
 *  @param max_sleep_error	Percentage by which the requested sleep time
 *				and the actual sleep time can differ from each
 *				other, and still be considered "successful".
 *				Essentially, the larger this value is, the
 *				smaller the minimum sleep interval that will be
 *				found, but also less accurate. Note, this error
 *				is "absolute"; if the requested sleep interval
 *				is 100 us and the actual sleep is 95 us or 105
 *				us, both cases are considered 5% error.
 *
 *  @param montonic_time_res	The system monotonic timer resolution. This
 *  				can be obtained with ST_get_monotonic_res_ps ().
 *
 *  @param num_sleep_retries	System sleeps can probabilistically fail, so
 *  				this argument sets how many times a particular
 *  				system sleep can be retried before giving up.
 *
 *  				Take note that this argument is for the number
 *  				of attempts for a single sleep interval, it is
 *  				not the number of sleep attempts in total.
 *  				The recommended value for this argument is 8.
 *
 *  @return			ST_SUCCESS in the event that #min_sleep was
 *				successfully calculated; any other value means
 *				that #min_sleep was not calculated correctly.
 */

ST_STD_RES ST_get_min_nb_sleep_ps
(
	ST_Ps *		min_sleep,
	ST_Ps		first_sleep,
	f64		max_sleep_error,
	ST_Ps		monotonic_time_res,
	u32		num_sleep_retries
)
{
	// Sanity check on arguments

	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, min_sleep, ST_BAD_ARG);


	if (monotonic_time_res.ps > first_sleep.ps)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_M_DEBUG,
			"#monotonic_time_res is greater than #first_sleep"
		);
	}


	if (0 >= first_sleep.ps)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_L_DEBUG,
			"#first_sleep argument was 0, which is invalid"
		);

		return ST_BAD_ARG;
	}


	// These variables hold the upper/lower bounds on the binary search of
	// what the smallest useful nonbusy sleep could be, as well the value
	// of the next sleep to attempt.

	ST_Ps hi_sleep =	first_sleep;
	ST_Ps lo_sleep =	{.ps = 0};

	ST_Ps cur_sleep =	first_sleep;


	// This constant is derived from the fact that a binary search is used,
	// and so the search space goes down by at least a factor of two each
	// time

	const u32 MAX_SEARCHES = (sizeof (cur_sleep.ps) * 8);


	// Enter a loop where a binary search will be performed to search for
	// the smallest sleep interval

	for (u32 search_num = 0; search_num < MAX_SEARCHES; search_num += 1)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_M_DEBUG,
			"hi_sleep  = %" ST_PRI_ST_PsNum " ps\n"
			"cur_sleep = %" ST_PRI_ST_PsNum " ps\n"
			"lo_sleep  = %" ST_PRI_ST_PsNum " ps\n",
			hi_sleep.ps,
			cur_sleep.ps,
			lo_sleep.ps
		);



		// Convert the current sleep interval into a #timespec
		
		struct timespec cur_sleep_ts;
		
		ST_STD_RES res = ST_Ps_to_timespec (cur_sleep, &cur_sleep_ts);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_L_DEBUG,
				"Could not convert #cur_sleep into #cur_sleep_ts"
			);

			return res;
		}


		// Perform the sleep, and keep track of when the sleep started
		// and ended

		struct timespec start_ts =	{.tv_sec = 0, .tv_nsec = 0};
		struct timespec stop_ts =	{.tv_sec = 0, .tv_nsec = 0};

		res =	ST_sleep_marked_monotonic
			(
				cur_sleep_ts,
				num_sleep_retries,
				&start_ts,
				&stop_ts
			);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_L_DEBUG,
				"Stopped due to failed sleep attempt!"
			);

			return res;
		}


		// Determine the difference between requested sleep time and
		// the actual sleep time

		struct timespec used_ts =

		ST_timespec_abs_diff (stop_ts, start_ts);


		ST_Ps used_ps = {.ps = 0};

		res = ST_timespec_to_ST_Ps (used_ts, &used_ps);

		if (ST_SUCCESS != res)
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_L_DEBUG,
				"Could not convert measured sleep time "
				"from a #timespec to a #ST_Ps"
			);

			return res;
		}


		ST_Ps error_ps = ST_Ps_abs_diff (used_ps, cur_sleep);


		// Calculate the error associated with this current sleep,
		//
		// Keep in mind, cur_sleep.ps is guaranteed to never be 0.

		f64 cur_sleep_error = (f64) error_ps.ps / cur_sleep.ps;


		//Check if the first sleep error was above the max error

		if ((0 == search_num) && (cur_sleep_error > max_sleep_error))
		{
			ST_DEBUG_PRINT
			(
				ST_OS_TIME_L_DEBUG,
				"Argument #first_sleep = %" ST_PRI_ST_PsNum " "
				"was too small, calculating #min_sleep_ps "
				"failed.",
				first_sleep.ps
			);

			return ST_BAD_ARG;
		}


		//Update the high/low bounds of the binary search space
		//depending on the current error

		if (cur_sleep_error < max_sleep_error)
		{
			//The sleep can become smaller, because the error is
			//allowed to become larger.

			hi_sleep = cur_sleep;


			cur_sleep.ps =

			(ST_PsNum) ((lo_sleep.ps + cur_sleep.ps) / (ST_PsNum) 2);
		}

		else
		{
			//The sleep must become larger, because the error is
			//too high

			lo_sleep = cur_sleep;


			cur_sleep.ps =

			(ST_PsNum) ((hi_sleep.ps + cur_sleep.ps) / (ST_PsNum) 2);
		}


		//Check if the sleep interval has become so low that it is not
		//representable by an #ST_Ps any more, in which case, the sleep
		//can not progress any farther

		if (0 >= cur_sleep.ps)
		{
			cur_sleep.ps = 1;

			break;
		}


		//Check if the search space has become exhausted, and if so,
		//terminate the binary search
		
		if (lo_sleep.ps >= hi_sleep.ps)
		{
			break;
		}


		// Check if the estimation for #min_sleep can not become any
		// more accurate due to the system timer resolution

		if (ST_Ps_abs_diff (hi_sleep, lo_sleep).ps <= monotonic_time_res.ps)
		{
			break;
		}
	}


	// Return the smallest sleep calculated

	*min_sleep = cur_sleep;


	return ST_SUCCESS;
}




/// This function performs an "arbitrary operation", which is a
/// platform-dependet set of instructions that are thought to satisfy these
/// properties
///
///	1) Takes almost the same amount of time to execute every time
///
///	2) Impossible / difficult for the compiler to optimize out
///
///	3) Consumes a small number of clock cycles so that time delays created
///	   by executing it are "granular"
///
///	4) Consumes enough clock cycles so that CPU caching and pipeline status
///	   don't wildly throw off estimates of how long it takes to execute.
///	   Additionally, the longer the arbitrary operation takes, the more
///	   "forgiving" it is, because other functions can execute instructions
///	   before calling it without significantly contributing to the time
///	   delay created by it.
///
/// The purpose of such an arbitrary operation is that it can be used to 
/// approximate sleeps that are too small to even call the operating system
/// functions to get the current system time. Depending on the speed at which
/// the OS can return timer values, such a sleep may be totally unnecessary.
///
/// @warning
/// Take note that an implementation of this function using...
///
/// 	@code
///
/// 	    __asm__ __volatile__ ("nop");
///
/// 	@endcode
///
/// ...was actually tested, but this was experimentally determined to have a
/// greater variability on its execution time than the current method.
///
/*!
 *  @param offset		A value that will be used in the arbitrary
 *				operation.
 *
 *  @return			The current value of the arbitrary operation
 */

u32 ST_arb_op (u32 offset)
{
	//Why is this variable a static volatile variable? It is to make it
	//necessary to _actually_ execute every call to this function, because
	//every call has the side-effect of updating this value. Ergo, it
	//hopefully makes this function impossible to remove by optimization.
	
	static volatile u32 arb_val;

	arb_val = ~(arb_val + offset);
	arb_val = ~(arb_val + offset);
	arb_val = ~(arb_val + offset);
	arb_val = ~(arb_val + offset);

	return arb_val;
}




/// Attempts to get the average amount of time used to execute #ST_arb_op ().
///
/// @warning
/// Since the operating system's functions to get the current time take a
/// non-zero amount of time to execute, the argument #num_samples should be
/// large enough such that the total amount of time spent executing
/// #ST_arb_op () is comfortably larger than the time spent executing the timer
/// functions.
/*!
 *  @param arb_op_ps		Where to place the calculated value for the
 *				average amount of time that #ST_arb_op () takes
 *				to execute
 *
 *  @param max_attempts		The max number of attempts to use in getting
 *				the start time and stop time in this function.
 *				This argument should be larger than 0.
 *
 *  @param num_samples		The number of samples to use in getting the
 *				average amount of time that #ST_arb_op () takes
 *				to execute. I.E., the number of times to call
 *				call #ST_arb_op () in between the start and
 *				stop time of this function.
 *				It is recommended that this argument be at
 *				least (50 * 1000), and it should be larger than
 *				0.
 * 
 *  @return			Standard status code
 */

ST_STD_RES ST_get_arb_op_ps_avg
(
	ST_Ps *	arb_op_ps,
	u32	max_attempts,
	u32	num_samples
)
{
	//Sanity check on arguments
	
	ST_DEBUG_NULL_RETURN (ST_OS_TIME_H_DEBUG, arb_op_ps, ST_BAD_ARG);


	if (0 >= max_attempts)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#max_attempts floored to 1."
		);

		max_attempts = 1;
	}


	if (0 >= num_samples)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_H_DEBUG,
			"#num_samples floored to 1."
		);

		num_samples = 1;
	}


	//Get the start time

	struct timespec start_ts;

	ST_STD_RES res = ST_get_monotonic_time_eager (&start_ts, max_attempts);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_M_DEBUG,
			"Could not START the stop-watch."
		);

		return res;
	}


	//Run a large number of iterations of #ST_SysTime_arb_op () to induce a
	//noticeable delay (as far as ST_get_monotonic_time_eager () is
	//concerned)

	for (u32 sample_num = 0; sample_num < num_samples; sample_num += 1)
	{
		ST_arb_op (sample_num);
	}


	//Get the stop time
	
	struct timespec stop_ts;

	res = ST_get_monotonic_time_eager (&stop_ts, max_attempts);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_OS_TIME_M_DEBUG,
			"Could not STOP the stop-watch."
		);

		return res;
	}


	//Determine the total amount of time used executing ST_arb_op (), then
	//get the average amount of time used by each function call

	struct timespec diff_ts = ST_timespec_abs_diff (stop_ts, start_ts);


	ST_Ps diff_ps = {.ps = 0};

	res = ST_timespec_to_ST_Ps (diff_ts, &diff_ps);

	if (ST_SUCCESS != res)
	{
		ST_DEBUG_PRINT
		(
			ST_SYS_TIME_L_DEBUG,
			"Could not convert #diff_ts to #diff_ps."
		);

		return res;
	}


	arb_op_ps->ps = (ST_PsNum) ((f64) diff_ps.ps / num_samples);


	return ST_SUCCESS;
}




/// Prints out the current value of ST_arb_op (). This function exists to make
/// a pathway between the static variable in ST_arb_op () to an IO operation,
/// to make it even more difficult for the compiler to optimize ST_arb_op ()
/// out.
/*!
 *  @param f_ptr		The #FILE to print the value of ST_arb_op () to
 *
 *  @param offset		The #offset argument to pass to ST_arb_op ()
 *
 *  @return			Standard status code
 */

ST_STD_RES ST_arb_op_print (FILE * f_ptr, u32 offset)
{
	ST_NULL_RETURN (ST_OS_TIME_H_DEBUG, f_ptr, ST_BAD_ARG);

	fprintf (f_ptr, "%" PRIu32, ST_arb_op (offset));

	return ST_SUCCESS;
}




/// Tests if the #ST_get_min_nb_sleep_ps() function works
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_get_min_nb_sleep_ps_unit_test ()
{
	//Test calculating the smallest sleep interval on the system

	printf ("\nTest calculating the smallest sleep interval possible...\n\n");


	// Make the variables to hold the arguments for ST_get_min_nb_sleep_ps ()

	ST_STD_RES res		= ST_SUCCESS;

	ST_Ps min_sleep		= {0};

	ST_Ps first_sleep	= {0};

	f64 max_error		= 0;


	// Find the system monotonic timer resolution, which is necessary to
	// call ST_get_min_nb_sleep_ps ()

	ST_Ps monotonic_time_res;

	res = ST_get_monotonic_res_ps (&monotonic_time_res);

	if (ST_SUCCESS != res)
	{
		SLY_PRINT_DEBUG ("Unable to get #monotonic_time_res");

		return res;
	}


	// Iterate through an arbitrary set of test parameters for
	// #ST_get_min_nb_sleep_ps ()

	const int num_tests = 7;

	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			first_sleep.ps =	(ST_PsNum) 1;
			max_error =		0;
		}

		else if (1 == ii)
		{
			first_sleep.ps =	(ST_PsNum) 1000 * 1000 * 1000;
			max_error =		0.1;
		}

		else if (2 == ii)
		{
			first_sleep.ps =	(ST_PsNum) 1000 * 1000 * 1000;
			max_error =		0.2;
		}

		else if (3 == ii)
		{
			first_sleep.ps =	(ST_PsNum) 1000 * 1000 * 1000;
			max_error =		0.5;
		}

		else if (4 == ii)
		{
			first_sleep.ps =	(ST_PsNum) 1000 * 1000 * 1000;
			max_error =		1.0;
		}

		else if (5 == ii)
		{
			first_sleep.ps =	(ST_PsNum) 1;	//Intentionally too-small sleep interval
			max_error =		0.01;
		}

		else
		{
			first_sleep.ps =	0;
			max_error =		0;
		}


		min_sleep.ps = 0;

		res = ST_get_min_nb_sleep_ps (&min_sleep, first_sleep, max_error, monotonic_time_res, 8);

		printf ("error margin     : %lf\n", max_error);
		
		printf ("first sleep      : ");

		ST_Ps_print (stdout, first_sleep);
		
		printf ("\n");
		
		printf ("actual min sleep : ");

		ST_Ps_print (stdout, min_sleep);
		
		printf ("\n");
	
		printf ("result           : ");

		ST_STD_RES_print (stdout, res);
		
		printf ("\n\n");
	}

	printf ("\n");


	return ST_SUCCESS;
}




/// Performs a small unit test on the ST_sleep_eager() function
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_sleep_eager_unit_test ()
{
	//Test sleeping with ST_sleep_eager ()

	struct timespec sleep_ts;

	sleep_ts.tv_sec		= 0;
	sleep_ts.tv_nsec	= 500 * 1000 * 1000;


	printf ("Using ST_sleep_eager ()\n\n");

	ST_sleep_eager (sleep_ts, 128);

	printf ("sleep : ");
	ST_timespec_print (stdout, sleep_ts);
	printf ("\n\n\n");


	return ST_SUCCESS;
}




/// Performs a small unit test on the ST_sleep_marked_monotonic() function
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_sleep_marked_monotonic_unit_test ()
{
	//Test sleeping with ST_sleep_marked_monotonic ()

	struct timespec sleep_ts;

	sleep_ts.tv_sec		= 0;
	sleep_ts.tv_nsec	= 500 * 1000 * 1000;

	struct timespec start_ts;
	struct timespec stop_ts;


	printf ("Using ST_sleep_marked_monotonic ()\n\n");


	ST_sleep_marked_monotonic (sleep_ts, 128, &start_ts, &stop_ts);

	printf ("sleep : ");
	ST_timespec_print (stdout, sleep_ts);
	printf ("\n");

	printf ("start : ");
	ST_timespec_print (stdout, start_ts);
	printf ("\n");

	printf ("stop  : ");
	ST_timespec_print (stdout, stop_ts);
	printf ("\n\n\n");

	return ST_SUCCESS;
}




/// Performs a small unit test on the ST_busy_sleep_marked_monotonic() function
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_busy_sleep_marked_monotonic_unit_test ()
{
	//Test sleeping with ST_busy_sleep_marked_monotonic ()

	struct timespec sleep_ts;

	sleep_ts.tv_sec		= 0;
	sleep_ts.tv_nsec	= 500 * 1000 * 1000;

	struct timespec start_ts;
	struct timespec stop_ts;


	printf ("Using ST_busy_sleep_marked_monotonic ()\n\n");

	ST_busy_sleep_marked_monotonic (sleep_ts, 128, &start_ts, &stop_ts);

	printf ("sleep : ");
	ST_timespec_print (stdout, sleep_ts);
	printf ("\n");

	printf ("start : ");
	ST_timespec_print (stdout, start_ts);
	printf ("\n");

	printf ("stop  : ");
	ST_timespec_print (stdout, stop_ts);
	printf ("\n\n\n");

	return ST_SUCCESS;
}




/// Performs a unit test on the various sleep functions available in
/// ST_os_time.c
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_sleep_unit_test ()
{
	ST_get_min_nb_sleep_ps_unit_test ();
	ST_sleep_eager_unit_test ();
	ST_sleep_marked_monotonic_unit_test ();
	ST_busy_sleep_marked_monotonic_unit_test ();

	return ST_SUCCESS;
}




/// Performs a very basic test on ST_Ps_print ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_Ps_print_unit_test ()
{

	printf ("\n\nTesting printing out arbitrary #ST_Ps values...\n\n");


	ST_Ps t_ps;

	const int num_tests = 8;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			t_ps.ps = 0;
		}

		else if (1 == ii)
		{
			t_ps.ps = 42;
		}

		else if (2 == ii)
		{
			t_ps.ps = (ST_PsNum) ST_Ps_MAX_SECS * ST_PS_PER_SEC;
		}

		else if (3 == ii)
		{
			t_ps.ps =
				
			(ST_PsNum)
			(ST_Ps_MAX_SECS * ST_PS_PER_SEC) +
			(ST_Ps_MAX_NSECS_WHEN_MAX_SECS * ST_PS_PER_NS);
		}

		else if (4 == ii)
		{
			t_ps.ps = (ST_PsNum) 0x7FFFFFFFFFFFFFFF;
		}

		else if (5 == ii)
		{
			t_ps.ps = (ST_PsNum) 0x8000000000000000;
		}

		else if (6 == ii)
		{
			t_ps.ps = (ST_PsNum) 0xffffffffffffffff;
		}

		else
		{
			t_ps.ps = 0;
		}



		ST_Ps_print (stdout, t_ps);

		printf ("\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on #ST_timespec_print ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_timespec_print_unit_test ()
{

	printf ("\n\nTesting printing out arbitrary #timespec values...\n\n");


	struct timespec ts;

	const int num_tests = 6;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			ts.tv_sec  =	1;
			ts.tv_nsec =	1;
		}

		else if (1 == ii)
		{
			ts.tv_sec  =	0;
			ts.tv_nsec =	50;
		}

		else if (2 == ii)
		{
			ts.tv_sec  =	50;
			ts.tv_nsec =	0;
		}

		else if (3 == ii)
		{
			ts.tv_sec  =	1111;
			ts.tv_nsec =	2222;
		}

		else if (4 == ii)
		{
			ts.tv_sec  =	0xFFFFFFFF;
			ts.tv_nsec =	0xFFFFFFFF;
		}

		else
		{
			ts.tv_sec  =	0;
			ts.tv_nsec =	0;
		}


		ST_timespec_print (stdout, ts);

		printf ("\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on #ST_Ps_to_timespec () to make sure it can
/// convert from an #ST_Ps to a #timespec properly
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_Ps_to_timespec_unit_test ()
{
	printf ("\n\nTesting conversion of #ST_Ps to #timespec...\n\n");

	ST_Ps t_ps;

	const int num_tests = 10;

	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			t_ps.ps = 1;
		}

		else if (1 == ii)
		{
			t_ps.ps = 999;
		}

		else if (2 == ii)
		{
			t_ps.ps = 1000;
		}

		else if (3 == ii)
		{
			t_ps.ps = 1001;
		}

		else if (4 == ii)
		{
			t_ps.ps = (ST_PsNum) 1000 * 1000 * 1000 * 1000 - 1;
		}

		else if (5 == ii)
		{
			t_ps.ps = (ST_PsNum) 1000 * 1000 * 1000 * 1000;
		}

		else if (6 == ii)
		{
			t_ps.ps = (ST_PsNum) 1000 * 1000 * 1000 * 1000 + 1;
		}

		else if (7 == ii)
		{
			t_ps.ps = (ST_PsNum) -1;
		}

		else if (8 == ii)
		{
			t_ps.ps = (ST_PsNum) -1 * 1000 * 1000 * 1000 * 1000 - 5000;
		}

		else
		{
			t_ps.ps = 0;
		}


		struct timespec ts;

		ST_STD_RES res = ST_Ps_to_timespec (t_ps, &ts);

		
		ST_Ps_print (stdout, t_ps);

		printf ("\n");

		ST_timespec_print (stdout, ts);

		printf ("\n");

		ST_STD_RES_print (stdout, res);

		printf ("\n\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on #ST_timespec_to_ST_Ps () to make sure
/// convert from a #timespec to an #ST_Ps properly
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_timespec_to_ST_Ps_unit_test ()
{
	printf ("\n\nTesting conversion of #timespec to #ST_Ps...\n\n");

	ST_Ps t_ps;

	struct timespec ts;

	ST_STD_RES res;


	const int num_tests = 9;

	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			ts.tv_sec =	0;
			ts.tv_nsec =	ST_TIMESPEC_MAX_TV_NSEC;
		}

		else if (1 == ii)
		{
			ts.tv_sec =	0;
			ts.tv_nsec =	ST_TIMESPEC_MAX_TV_NSEC + 1;
		}

		else if (2 == ii)
		{
			ts.tv_sec =	ST_Ps_MAX_SECS;
			ts.tv_nsec =	0;
		}

		else if (3 == ii)
		{
			ts.tv_sec =	ST_Ps_MAX_SECS;
			ts.tv_nsec =	ST_Ps_MAX_NSECS_WHEN_MAX_SECS;
		}

		else if (4 == ii)
		{
			ts.tv_sec =	ST_Ps_MAX_SECS;
			ts.tv_nsec =	ST_Ps_MAX_NSECS_WHEN_MAX_SECS + 1;
		}

		else if (5 == ii)
		{
			ts.tv_sec =	ST_Ps_MAX_SECS + 1;
			ts.tv_nsec =	0;
		}


		else if (6 == ii)
		{
			ts.tv_sec =	ST_Ps_MAX_SECS;
			ts.tv_nsec =	ST_TIMESPEC_MAX_TV_NSEC;
		}

		else if (7 == ii)
		{
			ts.tv_sec =	ST_Ps_MAX_SECS + 1;
			ts.tv_nsec =	ST_TIMESPEC_MAX_TV_NSEC + 1;
		}

		else
		{
			ts.tv_sec =	0;
			ts.tv_nsec =	0;
		}


		//Convert the selected #timespec to an #ST_Ps

		res = ST_timespec_to_ST_Ps (ts, &t_ps);


		ST_timespec_print (stdout, ts);

		printf ("\n");

		ST_Ps_print (stdout, t_ps);

		printf ("\n%s\n\n", ST_StdResStr_get (res).str);
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on #ST_timespec_fix ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_timespec_fix_unit_test ()
{
	printf ("\nTesting detecting / fixing badly formatted #timespec's...\n\n");


	struct timespec ts;

	const int num_tests = 8;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			ts.tv_sec  = 0;
			ts.tv_nsec = -1;
		}

		else if (1 == ii)
		{
			ts.tv_sec  = -1;
			ts.tv_nsec = 0;
		}

		else if (2 == ii)
		{
			ts.tv_sec  = -1;
			ts.tv_nsec = -1;
		}

		else if (3 == ii)
		{
			ts.tv_sec  = 45;
			ts.tv_nsec = -45;
		}

		else if (4 == ii)
		{
			ts.tv_sec  = -45;
			ts.tv_nsec = 45;
		}

		else if (5 == ii)
		{
			ts.tv_sec  = 10;
			ts.tv_nsec = 1000 * 1000 * 1000 + 1;
		}

		else if (6 == ii)
		{
			ts.tv_sec  = 10;
			ts.tv_nsec = 1000 * 1000 * 1000 - 1;
		}

		else
		{
			ts.tv_sec  = 0;
			ts.tv_nsec = 0;
		}


		ST_timespec_print (stdout, ts);

		printf ("\n");

		ts = ST_timespec_fix (ts);

		ST_timespec_print (stdout, ts);

		printf ("\n\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on #ST_timespec_abs_diff ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_timespec_abs_diff_unit_test ()
{
	printf ("\nTest taking the absolute difference between #timespec's...\n\n");


	struct timespec ts_0;
	struct timespec ts_1;
	struct timespec ts_diff;

	const int num_tests = 5;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{

			ts_0.tv_sec  = 1;
			ts_0.tv_nsec = 50 * 1000 * 1000 * 10;

			ts_1.tv_sec  = 1;
			ts_1.tv_nsec = 75 * 1000 * 1000 * 10;

		}

		else if (1 == ii)
		{

			ts_0.tv_sec  = 3;
			ts_0.tv_nsec = 60 * 1000 * 1000 * 10;

			ts_1.tv_sec  = 2;
			ts_1.tv_nsec = 50 * 1000 * 1000 * 10;
		}

		else if (2 == ii)
		{
			ts_0.tv_sec  = 2;
			ts_0.tv_nsec = 60 * 1000 * 1000 * 10;

			ts_1.tv_sec  = 3;
			ts_1.tv_nsec = 50 * 1000 * 1000 * 10;
		}

		else if (3 == ii)
		{
			ts_0.tv_sec  = 5;
			ts_0.tv_nsec = 1000 * 1000 * 1000 - 1;

			ts_1.tv_sec  = 5;
			ts_1.tv_nsec = 1000 * 1000 * 1000 + 1;

		}

		else
		{
			ts_0.tv_sec  = 0;
			ts_0.tv_nsec = 0;

			ts_1.tv_sec  = 0;
			ts_1.tv_nsec = 0;

		}


		ts_diff = ST_timespec_abs_diff (ts_0, ts_1);


		ST_timespec_print (stdout, ts_0);

		printf ("\n");

		ST_timespec_print (stdout, ts_1);

		printf ("\n");

		ST_timespec_print (stdout, ts_diff);

		printf ("\n\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on #ST_Ps_abs_diff ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_Ps_abs_diff_unit_test ()
{
	printf ("\nTest taking the absolute difference between ST_Ps's...\n\n");


	struct ST_Ps t_ps_0;
	struct ST_Ps t_ps_1;
	struct ST_Ps t_ps_diff;

	const int num_tests = 5;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			t_ps_0.ps = 0;
			t_ps_1.ps = 1;
		}

		else if (1 == ii)
		{
			t_ps_0.ps = 100;
			t_ps_1.ps = 95;
		}

		else if (2 == ii)
		{
			t_ps_0.ps = 95;
			t_ps_1.ps = 100;
		}

		else if (3 == ii)
		{
			t_ps_0.ps = 101;
			t_ps_1.ps = 303;
		}

		else
		{
			t_ps_0.ps = 0;
			t_ps_1.ps = 0;
		}


		t_ps_diff = ST_Ps_abs_diff (t_ps_0, t_ps_1);

		ST_Ps_print (stdout, t_ps_0);

		printf ("\n");

		ST_Ps_print (stdout, t_ps_1);

		printf ("\n");

		ST_Ps_print (stdout, t_ps_diff);

		printf ("\n\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on ST_Ps_diff ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_Ps_diff_unit_test ()
{

	printf ("\nTest taking the NON-absolute difference between ST_Ps's...\n\n");

	struct ST_Ps t_ps_0;
	struct ST_Ps t_ps_1;
	struct ST_Ps t_ps_diff;

	const int num_tests = 7;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			t_ps_0.ps = 100;
			t_ps_1.ps = 50;
		}

		else if (1 == ii)
		{
			t_ps_0.ps = 50;
			t_ps_1.ps = 100;
		}

		else if (2 == ii)
		{
			t_ps_0.ps = -100;
			t_ps_1.ps = 50;
		}

		else if (3 == ii)
		{
			t_ps_0.ps = 100;
			t_ps_1.ps = -50;
		}

		else if (4 == ii)
		{
			t_ps_0.ps = 100;
			t_ps_1.ps = -50;
		}

		else if (5 == ii)
		{
			t_ps_0.ps = 0;
			t_ps_1.ps = -150;
		}

		else
		{
			t_ps_0.ps = 0;
			t_ps_1.ps = 0;
		}


		t_ps_diff = ST_Ps_diff (t_ps_0, t_ps_1);

		ST_Ps_print (stdout, t_ps_0);

		printf ("\n");

		ST_Ps_print (stdout, t_ps_1);

		printf ("\n");

		ST_Ps_print (stdout, t_ps_diff);

		printf ("\n\n");
	}


	return ST_SUCCESS;
}




/// Performs a basic unit test on ST_timespec_greater_than ()
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_timespec_greater_than_unit_test ()
{
	printf ("\nTest comparing #timespec's with ST_timespec_greater_than()...\n\n");


	struct timespec ts_0;
	struct timespec ts_1;

	const int num_tests = 10;


	for (int ii = 0; ii < num_tests; ii += 1)
	{
		if (0 == ii)
		{
			ts_0.tv_sec  = 1;
			ts_0.tv_nsec = 0;

			ts_1.tv_sec  = 0;
			ts_1.tv_nsec = 0;
		}

		else if (1 == ii)
		{
			ts_0.tv_sec  = 0;
			ts_0.tv_nsec = 0;

			ts_1.tv_sec  = 1;
			ts_1.tv_nsec = 0;
		}

		else if (2 == ii)
		{
			ts_0.tv_sec  = 1;
			ts_0.tv_nsec = 1;

			ts_1.tv_sec  = 1;
			ts_1.tv_nsec = 0;
		}

		else if (3 == ii)
		{
			ts_0.tv_sec  = 1;
			ts_0.tv_nsec = 0;

			ts_1.tv_sec  = 1;
			ts_1.tv_nsec = 1;
		}

		else if (4 == ii)
		{
			ts_0.tv_sec  = 1;
			ts_0.tv_nsec = 1;

			ts_1.tv_sec  = 1;
			ts_1.tv_nsec = 1;
		}

		else if (4 == ii)
		{
			ts_0.tv_sec  = 0;
			ts_0.tv_nsec = 2 * 1000 * 1000 * 1000 + 1;

			ts_1.tv_sec  = 0;
			ts_1.tv_nsec = 2 * 1000 * 1000 * 1000;
		}

		else if (5 == ii)
		{
			ts_0.tv_sec  = 0;
			ts_0.tv_nsec = 2 * 1000 * 1000 * 1000;

			ts_1.tv_sec  = 0;
			ts_1.tv_nsec = 2 * 1000 * 1000 * 1000 + 1;
		}

		else
		{
			ts_0.tv_sec  = 0;
			ts_0.tv_nsec = 0;

			ts_1.tv_sec  = 0;
			ts_1.tv_nsec = 0;
		}


		ST_timespec_print (stdout, ts_0);

		printf ("\n");

		ST_timespec_print (stdout, ts_1);

		printf ("\n");

		printf
		(
			"The result of ST_timespec_greater_than (ts_0, ts_1) "
			"is %d\n\n",
			ST_timespec_greater_than (ts_0, ts_1)
		);

	}


	return ST_SUCCESS;
}




/// Performs all of the unit tests available in ST_os_time.c
/*!
 *  @return			Standard status code
 */

ST_STD_RES ST_os_time_all_unit_test ()
{

	ST_Ps_print_unit_test ();
	ST_timespec_print_unit_test ();
	ST_Ps_to_timespec_unit_test ();
	ST_timespec_to_ST_Ps_unit_test ();
	ST_timespec_fix_unit_test ();
	ST_timespec_abs_diff_unit_test ();
	ST_Ps_abs_diff_unit_test ();
	ST_Ps_diff_unit_test ();
	ST_timespec_greater_than_unit_test ();
	ST_sleep_unit_test ();


	return ST_SUCCESS;
}

