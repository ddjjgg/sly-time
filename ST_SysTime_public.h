/*!****************************************************************************
 *
 * @file
 * ST_SysTime_public.h
 *
 * The publicly accessible declarations associated with "ST_SysTime.c".
 *
 * Refer to the Doxygen generated documentation for information on the
 * following items.
 *
 *****************************************************************************/




#include <stdio.h>
#include <inttypes.h>


#include "ST_os_time_public.h"




#ifndef ST_SYS_TIME_PUBLIC_H
#define ST_SYS_TIME_PUBLIC_H




struct ST_SysTime;
typedef struct ST_SysTime ST_SysTime;




ST_STD_RES ST_SysTime_print (FILE * f_ptr, ST_SysTime * stp);




ST_SysTime * ST_SysTime_create ();




ST_SysTime * ST_SysTime_create_copy
(
	ST_SysTime *	src_stp,
	ST_STD_RES *	res_ptr
);




ST_STD_RES ST_SysTime_destroy (ST_SysTime * stp);




ST_STD_RES ST_SysTime_nbs
(
	ST_SysTime * 		stp,
	ST_Ps			sleep_ps,
	ST_Ps *			used_ps
);




ST_STD_RES ST_SysTime_nbs_calc_error
(
	ST_SysTime *		stp,
	ST_Ps			single_nbs_ps,
	uint64_t		num_sleeps,
	double *		error
);




ST_STD_RES ST_SysTime_nbs_calc_min_sleep
(
	ST_SysTime *		stp,

	ST_Ps			first_sleep_ps,
	ST_Ps			error_calc_min_ps,
	ST_Ps			min_bound_ps,

	double			max_error,

	ST_Ps			def_min_nbs_ps,
	ST_Ps *			min_nbs_ps
);




#endif //ST_SYS_TIME_PUBLIC_H

