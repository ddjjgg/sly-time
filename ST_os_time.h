/*!****************************************************************************
*
* @file
* ST_os_time.h
*
* Functions that interact with the given operating system's timing/sleep
* functions. This is where OS-dependent aspects of SlyTime should be
* handled.
*
******************************************************************************/




#ifndef ST_OS_TIME_H
#define ST_OS_TIME_H




#include <time.h>


#include "ST_numeric_types.h"


#include "ST_os_time_public.h"




ST_STD_RES ST_Ps_print (FILE * f_ptr, ST_Ps time_val);




ST_STD_RES ST_timespec_print (FILE * f_ptr, struct timespec ts_ptr);




ST_STD_RES ST_timespec_to_ST_Ps (struct timespec ts, ST_Ps * t_ps);




ST_STD_RES ST_Ps_to_timespec (ST_Ps t_ps, struct timespec * ts);




struct timespec ST_timespec_fix (struct timespec ts);




int ST_timespec_greater_than (struct timespec ts_a, struct timespec ts_b);




int ST_Ps_compare_qsort (const void * ps_a_ptr, const void * ps_b_ptr);




struct timespec ST_timespec_abs_diff (struct timespec ts_a, struct timespec ts_b);




ST_Ps ST_Ps_abs_diff (ST_Ps t_ps_a, ST_Ps t_ps_b);




ST_Ps ST_Ps_diff (ST_Ps t_ps_a, ST_Ps t_ps_b);




ST_STD_RES ST_get_monotonic_res_ts (struct timespec * time_res_ptr);




ST_STD_RES ST_get_monotonic_res_ts_eager
(
	struct timespec *	time_res_ptr,
	u32			max_attempts
);




ST_STD_RES ST_get_monotonic_res_ps (ST_Ps * time_res_ptr);




ST_STD_RES ST_get_monotonic_res_ps_eager (ST_Ps * time_res_ptr, u32 max_attempts);




ST_STD_RES ST_get_monotonic_time (struct timespec * cur_time_ptr);




ST_STD_RES ST_get_min_nb_sleep_ps
(
	ST_Ps *		min_sleep,
	ST_Ps		first_sleep,
	f64		max_sleep_error,
	ST_Ps		monotonic_time_res,
	u32		num_sleep_retries
);




ST_STD_RES ST_sleep_eager
(
	struct timespec		sleep_ts,
	u32 			num_attempts
);




ST_STD_RES ST_get_monotonic_time_eager
(
	struct timespec *	cur_time_ptr,
	u32			max_attempts
);




ST_STD_RES ST_sleep_marked_monotonic
(
	struct timespec		sleep_ts,
	u32			num_attempts,
	struct timespec *	start_ts,
	struct timespec *	stop_ts
);




ST_STD_RES ST_busy_sleep_marked_monotonic
(
	struct timespec		sleep_ts,
	u32			num_attempts,
	struct timespec *	start_ts,
	struct timespec *	stop_ts
);




ST_STD_RES ST_get_monotonic_time_eager
(
	struct timespec *	cur_time_ptr,
	u32			max_attempts
);




u32 ST_arb_op (u32 offset);




ST_STD_RES ST_get_arb_op_ps_avg
(
	ST_Ps *	arb_op_ps,
	u32	max_attempts,
	u32	num_samples
);




ST_STD_RES ST_arb_op_print (FILE * f_ptr, u32 offset);




ST_STD_RES ST_get_min_nb_sleep_ps_unit_test ();




ST_STD_RES ST_sleep_unit_test ();




ST_STD_RES ST_os_time_all_unit_test ();




#endif //ST_OS_TIME_H

