# SlyTime

SlyTime is a library that allows a program to sleep for very small time
intervals in a highly accurate manner.

The premier function of SlyTime is ST\_SysTime\_nbs (), which performs a
so-called "smart nonbusy sleep". By calling this function, the sleep is
automatically partitioned into a "nonbusy portion" and a "busy portion". This
function takes into account the sleep jitter associated with previous sleep
attempts, as well as general timing characteristics of the running system. By
doing so, ST\_SysTime\_nbs () is able to achieve sleeps with tiny error rates,
whilst giving as large of a time-slice as possible back to the operating
system.

By using SlyTime, a calling program is absolved from having to manually switch
sleep techniques for different sleep intervals. SlyTime will always attempt to
perform the best kind of sleep for a given time interval, whether that means
requesting the operating system to suspend execution of the current thread, or
running instructions in a loop to achieve the desired time delay. Notably, the
same sleep interval may require the use of different techniques at different
times depending on the current system load (which SlyTime accounts for by
monitoring sleep jitter).

To find the error rate associated with ST\_SysTime\_nbs () for a particular
sleep interval, the function ST\_SysTime\_nbs\_calc\_error () can be used. On
the other hand, to find the smallest sleep interval possible at a given error
rate, the function ST\_SysTime\_nbs\_calc\_min\_sleep () can be used.

The question of what is the smallest *useful* sleep interval is highly
dependent on what the acceptable error rate is. For example, on my development
system with an i7-4510U CPU running Linux 4.19.8, the smallest sleep interval
at 5% error is about 4 micro-seconds. At 20% error, the smallest sleep interval
goes to 25 nano-seconds.


## Dependencies

The only notable dependency right now is
[SlyDebug](https://gitlab.com/ddjjgg/sly-debug), other than standard headers
like time.h.

For now, the only supported platform is Linux.


## Known Issues

* The Doxygen documentation for the project does not clearly denote which items
  are publicly accessible and which are not. Although this can be easily
  determined by simply looking at which declarations are inside of the
  \*\_public.h headers, this should be visible within the Doxygen documentation
  itself.

* The 'makefile' used in this project is based off of the one used in SlyDebug,
  which works but is fairly ad-hoc. More work needs to be done so this project
  can properly build/install itself on different systems.

* While none of the algorithms used in this project are inherently
  Linux-dependent, this project makes use of Linux syscalls without being
  properly #ifdef'd. To bring SlyTime to other platforms, some work in
  ST\_os\_time.c will need to be completed.

* The number of functions made publicly accessible in this library is quite
  small. It is better to be on the conservative-side in this regard, but as the
  library stabilizes, it may be desirable to develop more public functions or
  expose already existing ones.


## Usage Tips

* When performing program sleeps (with or without SlyTime), it is advisable to
  bundle several small sleeps together as one larger sleep. Larger sleeps allow
  for a greater ratio of the sleep interval to be "nonbusy" than a smaller
  sleep, thus reducing the program's CPU utilization. Additionally, larger
  sleeps are more accurate than smaller sleeps, and will lead to a lower error
  rate.

  Of course, it is not always possible to group every small sleep together. The
  difference between SlyTime and many OS's sleep functions in this regard is
  that SlyTime will perform a best-effort busy sleep in user-space when given a
  tiny sleep interval. Many OS sleep functions will just perform a completely
  innaccurate sleep in that event. (This is why many people advise avoiding
  sleeps shorter than a millisecond; not necessarily true with SlyTime!)

* There is such a thing as a minimum useful sleep interval, even with SlyTime.
  However, what determines that value is

  	1) The current platform you are executing on

	2) The current system load on said platform

	3) The amount of acceptable sleep error in your application

  This is why the function ST\_SysTime\_nbs\_calc\_error () is provided. There
  is simply no one value that can be considered the smallest useful sleep
  interval on all systems. It is something that must be experimentally
  determined.

  Larger sleep intervals are less susceptible to error rates shifting with the
  system load than smaller sleep intervals are. Generally speaking, the error
  on tiny sleeps change so fast that by the time ST\_SysTime\_nbs\_calc\_error
  () completes, the error rate may have already changed. Ergo, point 2) does
  not normally imply periodically re-running ST\_SysTime\_nbs\_calc\_error ().


* The best way to perform a busy sleep is like so...

		while (flag_that_is_true_while_sleep_continues)
		{
			; //Do nothing
		}

   However, performing a busy sleep like this is only possible when you have a
   flag to compare against. If your intention is to *always* perform a busy
   sleep in some scenario, and you have such a flag available, then it is
   better to do that than it is to use ST\_SysTime\_nbs (). (Keep in mind that
   such a sleep will never save CPU resources).

   When ST\_SysTime\_nbs () performs a busy sleep, it might be doing so by
   constantly checking an OS provided timer value (if performing a "timed busy
   sleep" and not an "instruction busy sleep"). Ultimately, it is up to the OS
   how long this timer request will take, and so such a busy sleep has a jitter
   associated with it. While this jitter is typically very small (about ~60 ns
   on my system), it is still a non-zero value.

   The while-loop above at least has a chance of responding to the flag
   becoming false in the next few CPU clock cycles. It is unlikely such a low
   response latency can occur with ST_SysTime_nbs (). Again, the above
   while-loop relies on the existence of a flag that can be checked against


## Public Interface

Only the declarations found in the \*\_public.h headers form the public
interface of SlyTime. All other declarations are considered internal, and are
subject to change without the major version number being updated. The
documentation associated with all of the functions in the project (both public
and private) can be built with:

	doxygen ./doxygen.cfg

This will place the documentation into './docs/'. The HTML manual can be loaded
at './docs/html/index.html'.

## Building and Installation

To build and install SlyTime, run these commands in the root of the source
directory:

	make;
	sudo make install;

This will install the library to '/usr/lib/SlyTime-A-B-C.so', where A, B, and C
represent a version number. Publicly accessible headers will be placed in
'/usr/include/SlyTime-A-B-C/'.

Additionally, symbolic links will be created like so

	/usr/include/SlyTime-A-B-x   -> /usr/include/SlyTime-A-B-C/
	/usr/include/SlyTime-A-x-x   -> /usr/include/SlyTime-A-B-C/

	/usr/lib/libSlyTime-A-B-x.so -> /usr/lib/libSlyTime-A-B-C.so
	/usr/lib/libSlyTime-A-x-x.so -> /usr/lib/libSlyTime-A-B-C.so

By doing this, a calling programs can build against generic versions,
increasing the chances that a user has a compatible version installed.


## Debug Levels

It is possible to set debug levels of different components of SlyTime to
ST\_NO\_DEBUG, ST\_L\_DEBUG, ST\_M\_DEBUG, or ST\_H\_DEBUG. By default, every
debug level is set to ST\_NO\_DEBUG so that no debugging code executes. In some
circumstances, it may be useful to use...

	ST_set_all_debug_levels (ST_L_DEBUG);

...which can help catch badly formatted arguments. The higher debug levels are
mainly intended for internal development purposes only, and they will seriously
decrease the performance of sleeps with numerous debug messages.

