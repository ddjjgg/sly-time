/*!****************************************************************************
 *
 * @file
 * ST_misc_public.h
 * 
 * The publicly accessible declarations associated with "ST_misc.c".
 *
 * Refer to the Doxygen generated documentation for information on the
 * following items.
 *
 *****************************************************************************/




#ifndef ST_MISC_PUBLIC_H
#define ST_MISC_PUBLIC_H




/// Determines the length of the string in the #SlyResult struct

#define ST_STD_RES_STR_LEN	(64)




/// Represents common reasons for returning from a function.
///
/// There should ONLY be one "ST_SUCCESS" value, and NO generic "ST_FAILURE"
/// value. That way other functions can test "if (ST_SUCCESS != result)"
/// correctly, while the other return values can explain why the function
/// failed.
///
/// The "ST_" prefix simply prevents these values from clobbering names that
/// are likely to be used by the linking program.

typedef enum ST_STD_RES
{

	///Used to indicate a function successfully executed

	ST_SUCCESS = 0,	


	///Used to indicate a function was given bad arguments

	ST_BAD_ARG,	
	

	/// Used to indicate a memory allocation failed, stopping the function
	/// prematurely

	ST_BAD_ALLOC,	
	

	/// Denotes that calling a function internal to this library failed

	ST_BAD_CALL,


	/// Denotes that calling an external library function failed

	ST_BAD_LIB_CALL,	
	

	/// Denotes that the function failed due to being called at an
	/// unfortunate point in time
	
	ST_BAD_TIMING,


	/// Used to indicate that there is no work to be done. Similar to a
	/// #ST_NO_OP value, but meant to imply that the reason is specfically
	/// the set of work becoming empty.
	
	ST_NO_WORK,	

	
	/// Used to indicate that the function had no side-effects for whatever
	/// reason
	
	ST_NO_OP,


	/// Used to indicate a failure that occurred for an unknown reason
	
	ST_UNKNOWN_ERROR


} ST_STD_RES;




/// A type that holds a string representing a #ST_STD_RES value

typedef struct ST_StdResStr
{

	/// The actual string that represents a value of #ST_STD_RES

	char str [ST_STD_RES_STR_LEN];


} ST_StdResStr;




ST_StdResStr ST_StdResStr_get (ST_STD_RES str);




#endif //ST_MISC_PUBLIC_H

