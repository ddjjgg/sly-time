/*!****************************************************************************
 *
 * @file
 * test_0.c
 *
 * Performs a very simple test with SlyTime where a sleep is performed for 0.5
 * seconds.
 *
 *****************************************************************************/


#include "SlyTime.h"


int main ()
{
	//Create an ST_SysTime structure to use

	ST_SysTime * stp = ST_SysTime_create ();

	if (NULL == stp)
	{
		printf ("Could not allocate #ST_SysTime structure\n");

		return -1;
	}


	printf ("Performing multiple sleep operations\n\n");

	printf ("START\n");


	for (size_t sleep_num = 0; sleep_num < 100; sleep_num += 1)
	{
		ST_Ps sleep_ps =	{.ps = (ST_PsNum) 200 * 1000 * 1000 * 1000};

		ST_Ps used_ps =		{.ps = 0};


		ST_SysTime_nbs (stp, sleep_ps, &used_ps);

		printf
		(
			"sleep_ps = %" ST_PRI_ST_PsNum "\n"
			"used_ps  = %" ST_PRI_ST_PsNum "\n\n",
			sleep_ps.ps,
			used_ps.ps
		);
	}

	printf ("STOP\n\n");



	//Free any memory allocated in this function

	ST_SysTime_destroy (stp);


	return 0;
}
