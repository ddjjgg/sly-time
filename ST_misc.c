/*!****************************************************************************
 *
 * @file
 * ST_misc.c
 * 
 * Functions, type definitions, and constants that do not fit in well anywhere
 * else in SlyTime
 *
 *****************************************************************************/




#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "ST_debug.h"


#include "ST_misc.h"




/// Prints out the value contained by a ST_STD_RES enumerated value.
/*!
 *  @param s			The #ST_STD_RES to convert to a #ST_StdResStr
 *  				containing its representation as a string
 *
 *  @return			A #ST_StdResStr containing the string
 *				representation of #s
 *
 *
 *  EXAMPLE
 *
 *  @code
 *
 *	ST_STD_RES st_res = ST_SUCCESS;
 *
 *	printf
 *	(
 *		"Here is a string representing a ST_STD_RES value : %s\n",
 *		ST_StdResStr_get (st_res).str
 *	);
 *
 *  @endcode
 */

ST_StdResStr ST_StdResStr_get (ST_STD_RES s)
{
	ST_StdResStr strs;

	if (ST_SUCCESS == s)
	{
		strncpy (strs.str, "ST_SUCCESS", ST_STD_RES_STR_LEN);
	}

	else if (ST_BAD_ARG == s)
	{
		strncpy (strs.str, "ST_BAD_ARG", ST_STD_RES_STR_LEN);
	}

	else if (ST_BAD_ALLOC == s)
	{
		strncpy (strs.str, "ST_BAD_ALLOC", ST_STD_RES_STR_LEN);
	}

	else if (ST_BAD_CALL == s)
	{
		strncpy (strs.str, "ST_BAD_CALL", ST_STD_RES_STR_LEN);
	}

	else if (ST_BAD_LIB_CALL == s)
	{
		strncpy (strs.str, "ST_BAD_LIB_CALL", ST_STD_RES_STR_LEN);
	}

	else if (ST_BAD_TIMING == s)
	{
		strncpy (strs.str, "ST_BAD_TIMING", ST_STD_RES_STR_LEN);
	}

	else if (ST_NO_WORK == s)
	{
		strncpy (strs.str, "ST_NO_WORK", ST_STD_RES_STR_LEN);
	}

	else if (ST_NO_OP == s)
	{
		strncpy (strs.str, "ST_NO_OP", ST_STD_RES_STR_LEN);
	}

	else if (ST_UNKNOWN_ERROR == s)
	{
		strncpy (strs.str, "ST_UNKNOWN_ERROR", ST_STD_RES_STR_LEN);
	}

	else
	{
		strncpy (strs.str, "(??? ST_STD_RES)", ST_STD_RES_STR_LEN);
	}


	//Ensure that the last character is always NULL

	strs.str [ST_STD_RES_STR_LEN - 1] = '\0';


	return strs;
}




/// Prints out a given #ST_STD_RES to the provided FILE *
/*!
 *  @param fp			The FILE * to print to
 *
 *  @param st_res		The #ST_STD_RES to convert to a string and
 *				print out
 *
 *  @return			A standard status code
 */

ST_STD_RES ST_STD_RES_print (FILE * fp, ST_STD_RES st_res)
{
	ST_DEBUG_NULL_RETURN (ST_MISC_H_DEBUG, fp, ST_BAD_ARG);

	ST_StdResStr strs = ST_StdResStr_get (st_res);

	fprintf (fp, strs.str);

	return ST_SUCCESS;
}




/// Returns an #ST_ErrnoStr that represents the current value of #errno
/*!
 *  EXAMPLE USAGE
 *
 *  @code
 *
 *  printf ("The current value of errno is : %s", ST_ErrnoStr_get (errno, NULL).str);
 *
 *  @endcode
 *
 *
 *  @param errnum		The value of #errno (either the current value
 *				or a saved value)
 *
 *  @param success		A pointer to a STD_RESULT value that will hold
 *				the result of this function. If this is not
 *				SUCCESS, then the value of #errno was not
 *				successfully retrieved.
 *
 *				This argument CAN be NULL if no error-checking
 *				is desired.
 *
 *  @return			An #ST_ErrnoStr structure representing the value
 *				of #errno
 */

ST_ErrnoStr ST_ErrnoStr_get (int errnum, ST_STD_RES * st_res)
{
	ST_ErrnoStr errno_str;


	#if ((_POSIX_C_SOURCE >= 200112L) && !  _GNU_SOURCE)

		int res = strerror_r (errnum, errno_str.str, ST_ERRNO_STR_CHAR_LEN);


		if (NULL != st_res)
		{
			if (0 != res)
			{
				*st_res = ST_BAD_LIB_CALL;
			}

			else
			{
				*st_res = ST_SUCCESS;
			}
		}


	#else
		strerror_r (errnum, errno_str.str, ST_ERRNO_STR_CHAR_LEN);

		if (NULL != st_res)
		{
			*st_res = ST_SUCCESS;
		}

	#endif


	return errno_str;
}

