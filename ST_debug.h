/*!****************************************************************************
 *
 * @file
 * ST_debug.h
 *
 * Provides per-feature debug levels that can be checked throughout the rest of
 * the program.
 *
 * The general idea is that when a particular feature's debugging level is set
 * to #ST_NO_DEBUG, that feature should no longer have any debugging-related
 * code execute.
 *
 * Otherwise, the amount of debugging-related code to execute can be changed by
 * setting the debugging level to #ST_L_DEBUG, #ST_M_DEBUG, or #ST_H_DEBUG.
 *
 *****************************************************************************/




#ifndef ST_DEBUG_H
#define ST_DEBUG_H




#include <SlyDebug-0-x-x.h>


#include "ST_numeric_types.h"


#include "ST_debug_public.h"




/// @defgroup Debugging_Levels
///
/// Macros that define the debugging level for different program features.
///
/// Debug levels should be set PER feature, there should be NO "ALL FEATURES"
/// DEBUG LEVEL! Otherwise, it be very difficult to allow for granular
/// selection of debug levels later on.
///
/// The debugging levels of the program can be checked like so
///
/// @code
///
///	//Only do something if this feature's debugging is set to high
///
///	if (THIS_FEATURE_H_DEBUG)
///	{
///		//Stuff to do in high-debugging mode
///	}
///
/// @endcode
///
/// If the THIS_FEATURE_DEBUG_LEVEL is set to a lower level than ST_H_DEBUG,
/// than the statement should be removed from the program entirely. Removing
/// if-blocks that always evaluate to zero are one of the easiest optimizations
/// for compilers to make.
///
/// @{




///Sets default debug level for functions that directly interact with the
///operating system's provided timer functions


extern u32 ST_os_time_debug_level;


//#ifndef ST_OS_TIME_DEBUG_LEVEL
//
//	#define ST_OS_TIME_DEBUG_LEVEL		(ST_H_DEBUG)
//
//#endif //ST_OS_TIME_DEBUG_LEVEL


///Indicates high ST_OS_TIME debugging is enabled

#define ST_OS_TIME_H_DEBUG			(ST_os_time_debug_level >= ST_H_DEBUG)


///Indicates medium ST_OS_TIME debugging is enabled

#define ST_OS_TIME_M_DEBUG			(ST_os_time_debug_level >= ST_M_DEBUG)


///Indicates low ST_OS_TIME debugging is enabled

#define ST_OS_TIME_L_DEBUG			(ST_os_time_debug_level >= ST_L_DEBUG)




///Sets default debug level for functions that implement the #SysTimeParams
///data structure


extern u32 ST_SysTime_debug_level;


//#ifndef ST_SYS_TIME_DEBUG_LEVEL
//
//	#define ST_SYS_TIME_DEBUG_LEVEL		(ST_H_DEBUG)
//
//#endif //ST_SYS_TIME_DEBUG_LEVEL


///Indicates high ST_SYS_TIME debugging is enabled

#define ST_SYS_TIME_H_DEBUG			(ST_SysTime_debug_level >= ST_H_DEBUG)


///Indicates medium ST_SYS_TIME debugging is enabled

#define ST_SYS_TIME_M_DEBUG			(ST_SysTime_debug_level >= ST_M_DEBUG)


///Indicates low ST_SYS_TIME debugging is enabled

#define ST_SYS_TIME_L_DEBUG			(ST_SysTime_debug_level >= ST_L_DEBUG)




///Sets default debug level for functions that do not fit well into any other
///feature set of this library


extern u32 ST_misc_debug_level;


//#ifndef ST_MISC_DEBUG_LEVEL
//
//	#define ST_MISC_DEBUG_LEVEL		(ST_H_DEBUG)
//
//#endif //ST_MISC_DEBUG_LEVEL


///Indicates high MISC debugging is enabled

#define ST_MISC_H_DEBUG				(ST_misc_debug_level >= ST_H_DEBUG)


///Indicates medium MISC debugging is enabled

#define ST_MISC_M_DEBUG				(ST_misc_debug_level >= ST_M_DEBUG)


///Indicates low MISC debugging is enabled

#define ST_MISC_L_DEBUG				(ST_misc_debug_level >= ST_L_DEBUG)




/// @}




/// Causes a function to return if a pointer evaluates to NULL. If the
/// #debug_flag evaluates to true, then a debug message will also be printed
/// out.
///
/// Unlike #DEBUG_NULL_RETURN (), the NULL check is always performed, and only
/// the message printing is dependent on the debug flag.
/*!
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param ptr			The pointer to check for a NULL value
 *
 *  @param return_val		The value to return in the event the pointer is
 *  				NULL
 *
 *
 *  EXAMPLE
 *
 *  @code
 *
 *	//Returns an error if cool_ptr fails to be initialized
 *
 *  	int * cool_ptr = malloc (sizeof (int));
 *
 *  	ST_NULL_RETURN (cool_ptr, -1);
 *
 *  @endcode
 */

#define ST_NULL_RETURN(debug_flag, ptr, return_val)		\
								\
	do							\
	{							\
		if (debug_flag)					\
		{						\
			SLY_NULL_CHECK (ptr);			\
		}						\
								\
		if (NULL == (ptr))				\
		{						\
			return (return_val);			\
		}						\
	}							\
	while (0)




/// Causes a function to return if the pointer is NULL _only_ when the program
/// is in a debug mode
/*!
 *  This macro is very similar to #ST_NULL_RETURN (), except the NULL check is
 *  only performed when the debug flag is true.
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param ptr			The pointer to check for a NULL value
 *
 *  @param return_val		The value to return in the event the pointer is
 *				NULL
 *
 *
 *  EXAMPLE
 *
 *  @code
 *
 *  	//Returns an error if the function is passed a NULL pointer and the
 *  	//program is in a high debugging mode
 *
 *	int some_func (int * some_num_ptr)
 *	{
 *		ST_DEBUG_NULL_RETURN (DEBUG_IS_HIGH, some_num_ptr, -1);
 *
 *		//...
 *
 *		return 0;
 *	}
 *
 *  @endcode
 */

#define ST_DEBUG_NULL_RETURN(debug_flag, ptr, return_val)	\
								\
	do							\
	{							\
		if (debug_flag)					\
		{						\
			SLY_NULL_CHECK (ptr);			\
								\
			if (NULL == (ptr))			\
			{					\
				return (return_val);		\
			}					\
		}						\
	}							\
	while (0)




/// Prints a message if a given debug flag is asserted
/*!
 *  @param debug_flag		A flag indicating the program is in a given
 *  				debug mode, such as MATRIX_H_DEBUG or
 *  				TIMER_L_DEBUG
 *
 *  @param ...			The format string to print out, along with its
 *  				associated arguments. The format string follows
 *  				the same conventions as printf()
 */

#define ST_DEBUG_PRINT(debug_flag, ...)	\
					\
	{\
		if (debug_flag)\
		{\
			SLY_PRINT_DEBUG (__VA_ARGS__);\
		}\
	}




#endif //ST_DEBUG_H

