/*!****************************************************************************
 *
 * @file
 * SlyTime.h
 *
 *
 * This is the top-level header of the SlyTime library. This library provides
 * functions that can be used to perform "smart" busy / nonbusy sleeps, as well
 * as other functions to help characterize the timing behavior of the system.
 *
 *****************************************************************************/




#ifndef SLY_TIME_H
#define SLY_TIME_H




/// @defgroup		Sly_Time_Version_Constants
///
/// @{


//FIXME: Currently, the "makefile" references these values in this header
//by way of grep'ing for it. So, by messing up these values, it is too easy
//to break the build system. Find a better way to store the version numbers,
//such that these #define's are still available for reading, and so that the
//makefile properly labels things the same version _always_.


/// The major version of the current SlyTime header

#define SLY_TIME_MAJOR_VERSION			(0)


/// The minor version of the current SlyTime header

#define SLY_TIME_MINOR_VERSION			(3)


/// The patch version of the current SlyTime header

#define SLY_TIME_PATCH_VERSION			(1)


/// The semantic version of the current SlyTime header

#define SLY_TIME_SEM_VERSION			"0.3.1"


/// @}




//This is where all the publicly accessible functions in SlyTime bundled
//together and included

#include "ST_debug_public.h"
#include "ST_misc_public.h"
#include "ST_os_time_public.h"
#include "ST_SysTime_public.h"




#endif //SLY_TIME_H

